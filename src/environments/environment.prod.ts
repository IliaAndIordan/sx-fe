export const environment = {
  production: true,
  hmr: false,
  api_url: {
    auth: 'https://sx.ws.iordanov.info/',
    company: 'https://sx.ws.iordanov.info/company/',
    pricefile:'https://sx.ws.iordanov.info/pricefile/',
    project:'https://sx.ws.iordanov.info/project/',
    commodity:'https://sx.ws.iordanov.info/mfr/',
  },
  isDevelopment: false,
  isStaging: false,
  isProduction: true
};

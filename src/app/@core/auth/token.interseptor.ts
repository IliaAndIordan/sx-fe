import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Observable, BehaviorSubject, throwError } from 'rxjs';
import { catchError, switchMap, filter, take } from 'rxjs/operators';
// Services
import { TokenService } from './token.service';
import { SxApiAuthClient } from './api/api-auth.client';
import { JwtInterceptor } from '@auth0/angular-jwt';
import { AppStore } from '../const/app-local-storage.const';


@Injectable()
export class TokenInterceptor implements HttpInterceptor {

    private isRefreshing = false;
    private refreshTokenSubject: BehaviorSubject<any> = new BehaviorSubject<any>(null);

    constructor(
        public authClient: SxApiAuthClient,
        private tokenService: TokenService,
        private jwtInterceptor: JwtInterceptor) {
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // console.log('intercept ->');

        if (this.jwtInterceptor.isWhitelistedDomain(request) &&
            !this.jwtInterceptor.isBlacklistedRoute(request)) {

            return next.handle(request).pipe(
                catchError((err) => {
                    console.log('intercept -> catchError err', err);
                    if (err instanceof HttpErrorResponse) {
                        const errorResponse = err as HttpErrorResponse;
                        // && errorResponse.error.message === 'Expired JWT Token') {
                        if (errorResponse.status === 401 &&
                            errorResponse.error &&
                            errorResponse.error.message) {
                            console.log('intercept -> message', errorResponse.error.message);
                            localStorage.setItem(AppStore.RefreshToken, errorResponse.error.message);
                            console.log('intercept -> refreshToken', this.tokenService.decode(this.tokenService.refreshToken));
                            return this.handle401Error(request, next);
                        } else {
                            return throwError(err);
                        }


                    } else {
                        return throwError(err);
                    }
                }));

        } else {
            return next.handle(request);
        }
    }



    private handle401Error(request: HttpRequest<any>, next: HttpHandler) {
        console.log('handle401Error -> request', request);
        console.log('handle401Error -> isRefreshing', this.isRefreshing);
        if (!this.isRefreshing) {
            this.isRefreshing = true;
            this.refreshTokenSubject.next(null);

            return this.authClient.refreshToken().pipe(
                switchMap((token: any) => {
                    console.log('handle401Error -> token', token);
                    if (!this.tokenService.isTokenExpired()) {
                        this.isRefreshing = false;
                        this.refreshTokenSubject.next(this.tokenService.barerToken);
                        return this.jwtInterceptor.intercept(request, next);
                        // return next.handle(request);
                    } else {
                        this.authClient.logout();
                    }

                }),
                catchError(err => {
                    this.isRefreshing = false;
                    return throwError(err);
                }));

        } else {
            return this.refreshTokenSubject.pipe(
                filter(token => token != null),
                take(1),
                switchMap(jwt => {
                    return this.jwtInterceptor.intercept(request, next);
                    // return next.handle(request);
                }));
        }
    }

}

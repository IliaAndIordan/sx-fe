import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
// Services
import { CurrentUserService } from './current-user.service';
import { TokenService } from './token.service';
import { SxApiAuthClient } from './api/api-auth.client';
import { ApiHelper } from './api.helper';
import { ApiBaseClient } from './api-base.client';
import { JwtModule, JWT_OPTIONS, JwtInterceptor } from '@auth0/angular-jwt';
import { TokenFactory } from './token-factory';
import { TokenInterceptor } from './token.interseptor';


@NgModule({

  imports: [
    CommonModule,
    HttpClientModule,
    JwtModule.forRoot({
      jwtOptionsProvider: {
        provide: JWT_OPTIONS,
        useClass: TokenFactory
      }
    }),
  ],
  declarations: [],
  exports: [],
  providers: [
    ApiHelper,
    ApiBaseClient,
    SxApiAuthClient,
    JwtInterceptor, // Providing JwtInterceptor allow to inject JwtInterceptor manually into RefreshTokenInterceptor
    {
      provide: HTTP_INTERCEPTORS,
      useExisting: JwtInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    TokenService,
    CurrentUserService,
  ]
})
export class SxAuthModule { }

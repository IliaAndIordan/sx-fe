import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
// Constants
// Service
import { TokenService } from './token.service';
import { SxApiAuthClient } from './api/api-auth.client';
import { ToastrService } from 'ngx-toastr';
import { Observable, BehaviorSubject } from 'rxjs';
import { ServiceNames, ServiceUrl } from '../const/app-local-storage.const';




@Injectable()
export class ApiHelper {

    public ServiceNames = ServiceNames;
    public ServiceUrl = ServiceUrl;
    protected observable: Observable<any>;
    tokenAboutToExpire = new BehaviorSubject<string>(undefined);

    constructor(private tokenService: TokenService) { }

    public getServiceUrl(serviceName: string): string {
        let retValue = null;
        const sn = ServiceNames[serviceName];
        const urlFilter = ServiceUrl.filter(x => x.name === serviceName);
        // console.log('urlFilter.length:' + urlFilter.length);
        if (urlFilter && urlFilter.length > 0) {
            retValue = urlFilter[0].url;
        }

        // console.log('sn: ' + sn + ' for serviceUrl:', retValue);
        return retValue;
    }

    checkTocken(): void {
        const interalMs = this.tokenService.getTokenExpirationInterval();
        if (!interalMs && interalMs < 120000) {
            this.tokenAboutToExpire.next(this.tokenService.refreshToken);
        }
    }


}

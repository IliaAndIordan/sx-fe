import { Injectable, OnInit, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs';
import { of } from 'rxjs/observable/of';
import { Subject } from 'rxjs/Subject';
// --- Services
import { GravatarService } from '@infinitycube/gravatar';
import { SxApiAuthClient } from './api/api-auth.client';

// Models
import { UserModel, LatLng } from './api/dto';
import { AppStore, URL_NO_IMG, URL_NO_IMG_SQ } from '../const/app-local-storage.const';
import { TokenService } from './token.service';
import { LocalUserSettings, AppSettings } from '../models/app/settings';
import { UserRole } from './api/enums';
import { SettingsService } from '../services/settings.service';
import { CompanyModel, CompanyProductModel } from '../api/company/dto';
import { SxProjectModel } from '../api/project/dto';
import { CompanyType } from '../api/company/enums';

@Injectable()
export class CurrentUserService {

    currHubAirportSubject = new BehaviorSubject<any>(null);

    constructor(
        private tockenService: TokenService,
        private gravatarService: GravatarService,
        private settings: SettingsService) {

        this.settings.notice.subscribe(options => {
            // console.log('options changed: ', options);
            this.optionsChanged.next(options);
        });
    }


    //#region Location Geo

    getLocation(): Observable<LatLng> {
        return new Observable(obs => {
            const str = localStorage.getItem(AppStore.UserLocation);
            if (!str) {
                navigator.geolocation.getCurrentPosition(
                    pos => {
                        const rv = new LatLng(pos.coords.latitude, pos.coords.longitude);
                        localStorage.setItem(AppStore.UserLocation, JSON.stringify(rv));
                        obs.next(rv);
                        obs.complete();
                    },
                    error => {
                        console.log('error', error);
                        const rv = new LatLng(42.704198, 23.2840454);
                        obs.next(rv);
                        obs.complete();
                    }
                );
            } else {
                const rv = JSON.parse(str) as LatLng;
                obs.next(rv);
                obs.complete();
            }

        });
    }

    clearLocation() {

        localStorage.removeItem(AppStore.UserLocation);
    }

    //#endregion

    //#region User

    // tslint:disable-next-line: member-ordering
    userChanged = new Subject<UserModel>();

    get user(): UserModel {
        const userStr = localStorage.getItem(AppStore.CurrentUser);
        let userObj: UserModel;
        // console.log('userStr: ', userStr);
        if (userStr) {
            userObj = Object.assign(new UserModel(), JSON.parse(userStr));
        }
        // console.log('userObj: ', userObj);
        return userObj;
    }

    set user(userObj: UserModel) {
        if (userObj) {
            localStorage.setItem(AppStore.CurrentUser, JSON.stringify(userObj));
        } else {
            localStorage.removeItem(AppStore.CurrentUser);
            this.clearLocalStorage();
            this.clearLocation();
        }
        this.userChanged.next(userObj);
    }

    get isLogged(): boolean {
        let retValue = false;
        const userObj: UserModel = this.user;
        retValue = (userObj && !this.tockenService.isTokenExpired()) ? true : false;
        if (retValue) {
            // console.log('isLogged user: ', userObj);
        }
        // console.log('isLogged: ' + retValue);
        return retValue;
    }

    get isAdmin(): boolean {
        let retValue = false;
        const userObj: UserModel = this.user;
        // retValue = (userObj && !this.tockenService.isTokenExpired()) ? true : false;
        retValue = (userObj && userObj.user_role === UserRole.Admin);
        // console.log('isAdmin: ' + retValue);
        return retValue;
    }

    get isCompanyAdmin(): boolean {
        let retValue = false;
        const userObj: UserModel = this.user;
        if (userObj && userObj.company_id) {
            retValue = (userObj.user_role === UserRole.CompanyAdmin);
        }
        // console.log('isCompanyAdmin: ' + retValue);
        return retValue;
    }

    get companyType(): CompanyType {
        return this.company ? this.company.company_type_id : undefined;
    }

   
    get isAuthenticated(): boolean {
        // console.log('isAuthenticated: ');
        return this.isLogged;
    }

    get avatarUrl(): string {
        let retValue = URL_NO_IMG_SQ;
        const userObj: UserModel = this.user;
        // console.log('avatarUrl: userObj', userObj);
        // console.log('avatarUrl: email',  this.user.e_mail);
        if (this.user && this.user.e_mail) {

            const gravatarImgUrl = this.gravatarService.url(userObj.e_mail, 128, 'identicon');
            // console.log('gravatarImgUrl', gravatarImgUrl);
            if (gravatarImgUrl) {
                retValue = gravatarImgUrl;
            }
        }

        return retValue;
    }

    getAvatarUrl(e_mail: string): string {
        let retValue = URL_NO_IMG_SQ;
        const userObj: UserModel = this.user;
        // console.log('avatarUrl: userObj', userObj);
        // console.log('avatarUrl: email',  this.user.e_mail);
        if (e_mail) {

            const gravatarImgUrl = this.gravatarService.url(e_mail, 128, 'identicon');
            // console.log('gravatarImgUrl', gravatarImgUrl);
            if (gravatarImgUrl) {
                retValue = gravatarImgUrl;
            }
        }

        return retValue;
    }

    public clearLocalStorage() {
        // console.log('clearLocalStorage ->');
        Object.keys(AppStore).forEach((key: string) => {
            if (AppStore.hasOwnProperty(key)) {
                const val = (AppStore as any)[key];
                console.log('clearLocalStorage key= ' + key + ', val= ' + val);
                localStorage.removeItem(val);
            }
        });
        // console.log('clearLocalStorage <-');
    }


    //#endregion

    //#region CompanyModel

    // tslint:disable-next-line: member-ordering
    companyChanged = new Subject<CompanyModel>();

    get company(): CompanyModel {
        const objStr = localStorage.getItem(AppStore.CurrentUserCompany);
        let obj: CompanyModel;
        if (objStr) {
            obj = Object.assign(new CompanyModel(), JSON.parse(objStr));
        }
        // console.log('company-> obj', obj);
        return obj;
    }

    set company(value: CompanyModel) {
        if (value) {
            localStorage.setItem(AppStore.CurrentUserCompany, JSON.stringify(value));
        } else {
            localStorage.removeItem(AppStore.CurrentUserCompany);
        }
        this.companyChanged.next(value);
    }

    //#endregion

    //#region Company Products

    productsChanged = new Subject<Array<CompanyProductModel>>();

    get products(): Array<CompanyProductModel> {
        const objStr = localStorage.getItem(AppStore.CurrentUserProducts);
        let obj: Array<CompanyProductModel>;
        if (objStr) {
            obj = Object.assign(new Array<CompanyProductModel>(), JSON.parse(objStr));
        }
        return obj;
    }

    set products(value: Array<CompanyProductModel>) {
        const oldValue = this.products;
        if (value) {
            localStorage.setItem(AppStore.CurrentUserProducts, JSON.stringify(value));
        } else {
            localStorage.removeItem(AppStore.CurrentUserProducts);
        }

        this.productsChanged.next(value);
    }

    //#endregion

    //#region Project

    // tslint:disable-next-line: member-ordering
    selProjectChanged = new Subject<SxProjectModel>();

    get selProject(): SxProjectModel {
        const onjStr = localStorage.getItem(AppStore.SelProject);
        let obj: SxProjectModel;
        if (onjStr) {
            obj = Object.assign(new SxProjectModel(), JSON.parse(onjStr));
        }
        return obj;
    }

    set selProject(value: SxProjectModel) {
        const oldValue = this.selProject;
        if (value) {
            localStorage.setItem(AppStore.SelProject, JSON.stringify(value));
        } else {
            localStorage.removeItem(AppStore.SelProject);
        }

        this.selProjectChanged.next(value);
    }

    //#endregion

    //#region Local User Settings
    // tslint:disable-next-line: member-ordering
    optionsChanged = new Subject<AppSettings>();

    get options(): AppSettings {
        return this.settings.getOptions();
    }

    set options(value: AppSettings) {
        this.settings.setLayout(value);
    }

    setNavState(type: string, value: boolean) {
        this.settings.setNavState(type, value);
    }

    // tslint:disable-next-line:member-ordering
    public LocalSettingsChanged = new Subject<LocalUserSettings>();

    public get localSettings(): LocalUserSettings {
        const userStr = localStorage.getItem(AppStore.UserSettings);
        let userObj = null;
        if (userStr) {
            userObj = Object.assign(new LocalUserSettings(), JSON.parse(userStr));
        } else {
            userObj = new LocalUserSettings();
            userObj.snavLeftWidth = 4;
            localStorage.setItem(AppStore.UserSettings, JSON.stringify(userObj));
        }
        return userObj;
    }

    public set localSettings(userObj: LocalUserSettings) {
        if (userObj) {
            localStorage.setItem(AppStore.UserSettings, JSON.stringify(userObj));
        } else {
            localStorage.removeItem(AppStore.UserSettings);
        }
        this.LocalSettingsChanged.next(userObj);
    }

    //#endregion

    //#region Airports



    //#endregion
}

import { Injectable, Inject } from '@angular/core';
import { AppStore } from '../const/app-local-storage.const';

@Injectable()
export class TokenFactory {

    // public headerName: string =  'Authorization';
    // public authScheme: string = 'Bearer ';
    // tslint:disable-next-line:no-inferrable-types
    public skipWhenExpired: boolean = false;
    public  throwNoTokenError = false;
    public whitelistedDomains: string[] = [
        'sx.ws.iordanov.info',
        'sx.iordanov.info',
        'common.ams.iord',
        'localhost:4306',
        'maps.googleapis.com',
    ];

    // constructor(@Inject(localStorage) protected localStorage: any) { }
    constructor() { }
    public tokenGetter = () => {
        const token = localStorage.getItem(AppStore.BarerToken);
        // console.log('TokenFactory.tokenGetter token:' + token);
        return token;
    }
}

import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpParams, HttpResponse, HttpHeaders, HttpHeaderResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { catchError, map, tap } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';


// Services
import { ToastrService } from 'ngx-toastr';
import { CurrentUserService } from '../current-user.service';
import { UserModel, ResponseAuthenticate } from './dto';
import { TokenService } from '../token.service';
import { LocalUserSettings } from '../../models/app/settings';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { ApiHelper } from '../api.helper';
import { ServiceNames } from '../../const/app-local-storage.const';
import { AppRoutes } from '../../const/app-routes.const';
import { ApiBaseClient } from '../api-base.client';
import { CompanyModel, CompanyProductModel } from '../../api/company/dto';
import { resolve } from 'dns';
import { SxProduct } from '../../models/pipes/product.pipe';

// Models

// import { JwtHelperService } from '@auth0/angular-jwt';


@Injectable()
export class SxApiAuthClient extends ApiBaseClient {

  refreshTockenSubscriver: Subscription;

  constructor(
    private router: Router,
    private client: HttpClient,
    private apiHelper: ApiHelper,
    private tokenService: TokenService,
    private cus: CurrentUserService) {
    super();
    this.refreshTockenSubscriver = this.apiHelper.tokenAboutToExpire.subscribe(refreshTocken => {
      // console.log('refreshTockenSubscriver -> refreshTocken:', refreshTocken);
      if (refreshTocken) {
        this.refreshToken().subscribe(resp => {
          console.log('refreshToken-> resp: ', resp);
        },
          err => {
            console.log('refreshToken-> err: ', err);
          });
      }

    });
  }

  private get baseUrl(): string {
    return this.apiHelper.getServiceUrl(ServiceNames.auth);
  }

  //#region JWT Tocken

  autenticate(name: string, pwd: string): Observable<any> {
    // console.log('user: ' + name + ', ped: ' + pwd);
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.client.post<HttpResponse<any>>(
      this.baseUrl + '/login',
      { username: name, password: pwd },
      { headers: hdrs, observe: 'response' })
      .pipe(
        map((responce: HttpResponse<any>) => this.convertAutenticateToUser(responce)),
        /*
        tap((res: HttpResponse<any>) => {
          console.log(res.headers.keys);
        }),*/
        catchError(this.handleError)
      );
    /*
      .map(user => {
        // login successful if there's a jwt token in the response
        if (user && user.token) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('currentUser', JSON.stringify(user));
        }

        return user;
      });
      */
  }

  refreshToken(): Observable<any> {

    console.log('refreshToken -> ');
    console.log('refreshToken -> barerToken:', this.tokenService.barerToken);
    console.log('refreshToken -> refreshToken:', this.tokenService.refreshToken);
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.client.post<HttpResponse<any>>(
      this.baseUrl + 'jwtrefresh',
      { refresh: this.tokenService.refreshToken },
      { headers: hdrs, observe: 'response' })
      .pipe(
        map((response: HttpResponse<any>) => {
          const ra: ResponseAuthenticate = this.convertAutenticateToUser(response);
          console.log('refreshToken -> ResponseAuthenticate:', ra);
          console.log('refreshToken -> barerToken:', this.tokenService.barerToken);
          return this.tokenService.barerToken;
        }),
        catchError(this.handleError)
      );
  }

  convertAutenticateToUser(response: HttpResponse<any>): ResponseAuthenticate {
    let userObj;
    let userSettings: LocalUserSettings;
    let company: CompanyModel;
    let token;
    let rv: ResponseAuthenticate;
    let products = new Array<CompanyProductModel>();
    console.log('response:', response);
    if (response) {
      if (response.headers) {
        token = response.headers.get('X-Authorization');
      }

      if (response.body) {
        rv = Object.assign(new ResponseAuthenticate(), response.body);
        console.log('convertAutenticateToUser ResponseAuthenticate:', rv);
        if (rv && rv.success) {

          if (rv.data && rv.data.current_user) {
            userObj = UserModel.fromJSON(rv.data.current_user);
            console.log('convertAutenticateToUser -> userObj:', userObj);
          }
          
          if (rv.data && rv.data.company) {
            company = CompanyModel.fromJSON(rv.data.company);
            console.log('convertAutenticateToUser -> company:', company);
          }

          console.log('convertAutenticateToUser -> rv.data.products:', rv.data.products);
          if (userObj && rv.data.products && rv.data.products.length > 0) {
            rv.data.products.forEach(el => {
              const obj = CompanyProductModel.fromJSON(el);
              console.log('products-> obj=', obj);
              products.push(obj);
            });

            console.log('convertAutenticateToUser -> products:', products);
          }


          if (rv.data.settings) {
            userSettings = LocalUserSettings.fromJSON(rv.data.settings);
            console.log(' User Settings: ', userSettings);
          }

        }
      }


      if (token) {
        this.tokenService.barerToken = token;
        /*
        if (!this.refreshTokenTimerId) {
          this.refreshTokenTimerId = setInterval(function () {
            if (!this.tockenService.barerToken) {
              clearInterval(this.refreshTokenTimerId);
              this.refreshTokenTimerId = null;
              return;
            }
            console.log(' ---- begin setInterval ----');
            this.refreshToken()
            .subscribe(user => {
              if (user) {
                this.toastr.success('Welcome back ' + user.name + '!', 'Login success');
              } else {
                this.toastr.successr('Login Failed', 'Login Failed');
              }
            });
            console.log(' ---- end setInterval ----');
          }.bind(this), 5000);

        }
        */

        console.log('decodeToken:', this.tokenService.decodeToken());
        this.cus.user = userObj;
        this.cus.localSettings = userSettings;
        this.cus.company = company;
        this.cus.products = products;

      }

    }

    // console.dir(retvalue);
    return rv;
  }



  logout(): Observable<any> {
    console.log('logout -> ');
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.client.post<HttpResponse<any>>(
      this.baseUrl + '/authenticate',
      { provider: 'logout' },
      { headers: hdrs, observe: 'response' })
      .pipe(
        map((response: HttpResponse<any>) => {
          console.log('logout -> responce=', response);
          this.tokenService.clearToken();
          this.cus.user = undefined;
          this.cus.clearLocalStorage();
          this.router.navigate(['/', AppRoutes.guest]);
          return of(response);
        }),
        /*
        tap((res: HttpResponse<any>) => {
          console.log(res.headers.keys);
        }),*/
        catchError(this.handleError)
      );
  }

  register(mail: string, pwd: string): Observable<any> {
    console.log('e-mail: ' + mail + ', psw: ' + pwd);
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.client.post<HttpResponse<any>>(
      this.baseUrl + '/register',
      { email: mail, password: pwd },
      { headers: hdrs, observe: 'response' })
      .pipe(
        map((responce: HttpResponse<any>) => this.convertAutenticateToUser(responce)),
        /*
        tap((res: HttpResponse<any>) => {
          console.log(res.headers.keys);
        }),*/
        catchError(this.handleError)
      );
    /*
      .map(user => {
        // login successful if there's a jwt token in the response
        if (user && user.token) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('currentUser', JSON.stringify(user));
        }

        return user;
      });
      */
  }

  //#endregion


}

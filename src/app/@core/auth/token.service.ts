import { Injectable, Inject } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { TokenFactory } from './token-factory';
import { AppStore } from '../const/app-local-storage.const';

@Injectable()
export class TokenService extends TokenFactory {


  constructor(private jwtHelper: JwtHelperService) {
    super();
  }

  //#region JwtHelperService methods

  public isTokenExpired(): boolean {
    const retVal = this.jwtHelper.isTokenExpired();
    if (retVal) {
       console.log('isTokenExpired: ' + retVal);
    }
    return retVal;
  }

  public getTokenExpirationDate(): Date {
    return this.jwtHelper.getTokenExpirationDate();
  }

  public getTokenExpirationInterval(): number {
    let timeMs = -1;
    const expDate = this.getTokenExpirationDate();
    if (expDate) {
      timeMs = expDate.getTime() - new Date().getTime();
    }
    console.log('getTokenExpirationInterval -> ', timeMs);
    return timeMs;
  }


  public decodeToken(): string {
    return this.jwtHelper.decodeToken();
  }

  public decode(token: string): string {
    return this.jwtHelper.decodeToken(token);
  }
  

  
  public get expIntervalMs(): number {
    let rv = 0;
    const curDate: Date = new Date();
    const jwtExp: Date = this.getTokenExpirationDate();
    rv = jwtExp.getTime() - curDate.getTime();
    // console.log('ExpIntervalMs-> rv=', rv);
    return rv;
  }
  //#endregion

  //#region Token

  public set barerToken(value: string) {
    if (!value) {
      return;
    }

    localStorage.setItem(AppStore.BarerToken, value);
  }

  public get barerToken(): string {
    return localStorage.getItem(AppStore.BarerToken);
  }

  public get refreshToken(): string {
    return localStorage.getItem(AppStore.RefreshToken);
  }

  public clearToken() {
    localStorage.removeItem(AppStore.RefreshToken);
    localStorage.removeItem(AppStore.BarerToken);
  }


  //#endregion
}

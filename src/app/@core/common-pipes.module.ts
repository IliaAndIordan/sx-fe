import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserRoleDisplayPipe } from './auth/api/enums';
import { CompanyStatusDisplayPipe, CompanyTypeDisplayPipe } from './api/company/enums';
import { FileSizePipe } from './models/pipes/file-size.pipe';
import { SxProjectStatusDisplayPipe } from './api/project/enums';
import { SxCurrencyDisplayPipe } from './models/pipes/sx-currency.pipe';
import { TruncatePipe } from './models/pipes/truncate.pipe';
import { CommodityDisplayDisplayPipe } from './api/mfr/commodity/enums';
import { ItemProcessingStatusDisplayPipe } from './api/mfr/item/enums';


@NgModule({
    imports: [
        CommonModule,
    ],
    declarations: [
        CompanyTypeDisplayPipe,
        CompanyStatusDisplayPipe,
        UserRoleDisplayPipe,
        FileSizePipe,
        SxProjectStatusDisplayPipe,
        SxCurrencyDisplayPipe,
        TruncatePipe,
        CommodityDisplayDisplayPipe,
        ItemProcessingStatusDisplayPipe,
    ],
    exports:[
        CompanyTypeDisplayPipe,
        CompanyStatusDisplayPipe,
        UserRoleDisplayPipe,
        FileSizePipe,
        SxProjectStatusDisplayPipe,
        SxCurrencyDisplayPipe,
        TruncatePipe,
        CommodityDisplayDisplayPipe,
        ItemProcessingStatusDisplayPipe,
    ],
    providers: [
        CompanyTypeDisplayPipe,
        CompanyStatusDisplayPipe,
        UserRoleDisplayPipe,
        FileSizePipe,
        SxProjectStatusDisplayPipe,
        TruncatePipe,
        CommodityDisplayDisplayPipe,
        ItemProcessingStatusDisplayPipe,
    ]
})
export class SxCommonPipesModule { }

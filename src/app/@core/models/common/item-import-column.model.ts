import { ReadVarExpr } from '@angular/compiler';

export class DataField {
    idx: number;
    name: string;
    value: any;
}

export class ItemImportColumnModel {
    dbColumn: string;
    name: string;
    description?: string;

    dataField?: DataField;
    columnIndex?: number = -1;
    example?: any;
    required?: boolean = false;
}




export const MfrItemColumnsOpt: ItemImportColumnModel[] = [
    {
        dbColumn: 'item_id',
        name: 'SX PIK',
        description: 'SX permanent and unique key to identify the product.  This is the column by which matching is performed firstM',
    },
    {
        dbColumn: 'mfr_item_id',
        name: 'SX MFR ITEM PIK',
        description: 'SX permanent and unique key to identify the manufacturer product.  This is the column by which matching is performed firstM',
    },
    {
        dbColumn: 'item_status_id',
        name: 'ITEM STATUS',
        description: 'Status of item 2:User Added, 3:Inactive, 4:Deleted, 6:Active',
    },
    {
        dbColumn: 'upc',
        name: 'UPC',
        description: 'Standard 11-digit Universal Product Code (UPC) of the item.  If SX PIK is not present, then matching is performed by UPC.',
    },
    {
        dbColumn: 'ean',
        name: 'EAN',
        description: 'Thirteen-digit EAN-13, a superset of the original 11-digit Universal Product Code (UPC) of the item.  If SX PIK is not present, then matching is performed by EAN.',
    },
    {
        dbColumn: 'gtin',
        name: 'GTIN',
        description: '13-digit number called a Global Trade Item Number (GTIN).  If SX PIK is not present, then matching is performed by GTIN.',
    },
    {
        dbColumn: 'mfr_item_pik',
        name: 'Manufacturer PIK',
        description: 'Manufacturer permanent and unique key to identify the product.',
    },
    {
        dbColumn: 'mfr_catalog_code',
        name: 'Manufacturer Catalog Code',
        description: 'The catalog code of the item.  If UPC is not present, then matching is performed by ManufacturerName and CatalogCode.  If UPC and ManufacturerName are not present, then matching is performed by CatalogCode only.',
        required: true,
    },
    {
        dbColumn: 'mfr_item_name',
        name: 'Item Name',
        description: 'The name of the item.',
        required: true,
    },
    {
        dbColumn: 'mfr_ucc',
        name: 'UCC',
        description: 'First 6 numbers from UPC.',
    },
    {
        dbColumn: 'mfr_country_of_origin_code',
        name: 'Country of Origin',
        description: '3 letter country of origin code (MEX, USA, BGN).',
    },
    {
        dbColumn: 'industry_id',
        name: 'Industry',
        description: 'One digit industry code (1:Plumbing, 2:Industrial, 3:HVACR, 4:Electricall).',
    },
    {
        dbColumn: 'image_url',
        name: 'Image URL',
        description: 'Url to the product image.',
    },
    {
        dbColumn: 'thumb_url',
        name: 'Tumbnail URL',
        description: 'Url to the product image.',
    },
    {
        dbColumn: 'web_thumb_ur',
        name: 'Web Tumbnail',
        description: 'Url to the product image.',
    },
    {
        dbColumn: 'brand_name',
        name: 'Brand',
        description: 'Product brand name if exist.',
    },
    {
        dbColumn: 'item_product_id',
        name: 'Product ID',
        description: 'Reference Global SX Product.',
    },
    {
        dbColumn: 'edms_pik',
        name: 'EDMS ID',
        description: 'TS EDMS PIK.',
    },
    {
        dbColumn: 'mfr_item_description',
        name: 'Description',
    },
    {
        dbColumn: 'invoice_description',
        name: 'Short Description',
        description: 'Short Description Invoice/PFMS.',
    },
    {
        dbColumn: 'lead_free',
        name: 'Lead Free',
    },
    {
        dbColumn: 'buy_amerika_complain',
        name: 'Buy Amerika Complain',
    },
];

import { Pipe, PipeTransform } from '@angular/core';
import { CurrencyPipe } from '@angular/common';

export enum CurrencyType {
    Usd,
    Cad,
}

/**
 * Get Curreny code based on Trade Service Currency Id
 */
@Pipe({ name: 'sxcurrency' })
export class SxCurrencyDisplayPipe implements PipeTransform {

    constructor(private currencyPipe: CurrencyPipe) { }

    transform(value: any, args?: any[], symbolDisplay?: boolean, digits?: string): string {
        let currencyCode = CurrencyType[value];
        if (!currencyCode) {
            console.error('no currencyCode provided to tscurrencydisplay pipe, defaulting to USD');
            currencyCode = CurrencyType[0]; //USD
        }

        return currencyCode.toUpperCase();
    }
}


export interface AppSettings {
  navPos?: 'side' | 'top';
  dir?: 'ltr' | 'rtl';
  theme?: 'light' | 'dark';
  showHeader?: boolean;
  headerPos?: 'fixed' | 'static' | 'above';
  showUserPanel?: boolean;
  sidenavOpened?: boolean;
  sidenavCollapsed?: boolean;
}

export const defaults: AppSettings = {
  navPos: 'side',
  dir: 'ltr',
  theme: 'light',
  showHeader: true,
  headerPos: 'fixed',
  showUserPanel: true,
  sidenavOpened: true,
  sidenavCollapsed: true,
};

export class LocalUserSettings {
  public userId: number;
  public snavLeftWidth = 4;
  public displayDistanceMl: boolean;
  public displayWeightLb: boolean;

  public static fromJSON(json: ILocalUserSettings): LocalUserSettings {
      const vs = Object.create(LocalUserSettings.prototype);
      return Object.assign(vs, json, {
          snavLeftWidth: json.snavLeftWidth ? json.snavLeftWidth : 4,
          displayDistanceMl: json.displayDistanceMl !== 0 ? true : false,
          displayWeightLb: json.displayWeightLb !== 0 ? true : false,
      });
  }

  // reviver can be passed as the second parameter to JSON.parse
  // to automatically call User.fromJSON on the resulting value.
  public static reviver(key: string, value: any): any {

      return key === '' ? LocalUserSettings.fromJSON(value) : value;
  }

  public toJSON(): ILocalUserSettings {
      const vs = Object.create(LocalUserSettings.prototype);
      return Object.assign(vs, this);
  }
}

export interface ILocalUserSettings {
  userId: number;
  snavLeftWidth: number;
  displayDistanceMl: number;
  displayWeightLb: number;
}


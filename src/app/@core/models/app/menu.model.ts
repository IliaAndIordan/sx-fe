import { CompanyType } from '../../api/company/enums';

export interface Tag {
    color: string; // Background Color
    value: string;
}

export interface ChildrenItem {
    state: string;
    name: string;
    type: 'link' | 'sub' | 'extLink' | 'extTabLink';
    children?: ChildrenItem[];
}

export interface Menu {
    state: string;
    name: string;
    type: 'link' | 'sub' | 'extLink' | 'extTabLink';
    icon: string;
    label?: Tag;
    badge?: Tag;
    children?: ChildrenItem[];
}

export const MENU_TYPE_OPT: string[] = ['link', 'sub', 'extLink', 'extTabLink'];

export enum MenuItemType {
    Link = 1,
    Sub = 2,
    ExtLink = 3,
    ExtTabLink = 4,
}

export class MenuTag {
    color: string; // Background Color
    value: string;
}

export class MenuItem {
    state: string;
    name: string;
    icon: string;
    type: string;
    menuType: MenuItemType;
    label?: MenuTag;
    badge?: MenuTag;
    children?: ChildrenItem[];

    hasCompanyId = true;
    isAdmin = false;
    maxUserRole = 4;
    company_type?: CompanyType;
}

export class Breadcrumb {
    name: string;
    path: string[];

    constructor(name: string, path: string[]) {
        this.name = name;
        this.path = path;
    }
}


export const MenuOpt: MenuItem[] = [
    {
        state: 'companycreate',
        name: 'Create Company',
        icon: 'business',
        menuType: MenuItemType.Link,
        type: 'link',
        label: {
            color: 'indigo-500',
            value: 'new'
        },
        hasCompanyId: false,
        isAdmin: false,
        maxUserRole: 4,
    },
    {
        state: 'company',
        name: 'My Company',
        icon: 'business',
        menuType: MenuItemType.Link,
        type: 'link',
        hasCompanyId: true,
        isAdmin: false,
        maxUserRole: 4,
    },
    {
        state: 'admin',
        name: 'Admin',
        icon: 'admin_panel_settings',
        menuType: MenuItemType.Link,
        type: 'link',
        hasCompanyId: true,
        isAdmin: true,
        maxUserRole: 1,
        children: [
            {
                state: 'dashboard',
                name: 'Dashboard',
                type: 'link',
            },
            {
                state: 'company-list',
                name: 'Company List',
                type: 'link',
            },
            {
                state: 'user-list',
                name: 'User List',
                type: 'link',
            },
            {
                state: 'company',
                name: 'Company',
                type: 'link',
            },
        ]
    },
    {
        state: 'projects',
        name: 'Projects',
        icon: 'bento',
        menuType: MenuItemType.Link,
        type: 'link',
        hasCompanyId: true,
        isAdmin: false,
        maxUserRole: 4,
        children: [
            {
                state: 'dashboard',
                name: 'Dashboard',
                type: 'link',
            },
        ]
    },
    {
        state: 'manufacturer',
        name: 'Manufacturer',
        icon: 'engineering',
        menuType: MenuItemType.Link,
        type: 'link',
        hasCompanyId: true,
        company_type: CompanyType.Manufacturer,
        isAdmin: false,
        maxUserRole: 4,
        children: [
            {
                state: 'dashboard',
                name: 'Dashboard',
                type: 'link',
            },
        ]
    },
    {
        state: 'distributor',
        name: 'Distributor',
        icon: 'store_mall_directory',
        menuType: MenuItemType.Link,
        type: 'link',
        hasCompanyId: true,
        company_type: CompanyType.Distributor,
        isAdmin: false,
        maxUserRole: 4,
    },
];

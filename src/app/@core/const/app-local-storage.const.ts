import { environment } from 'src/environments/environment';

export const AppStore = {
    BarerToken: 'sx_barer_token_key',
    RefreshToken: 'sx_refresh_token_key',
    CurrentUser: 'sx_current_user_key',
    CurrentUserCompany: 'sx_current_user_company_key',
    CurrentUserProducts: 'sx_current_user_company_products_key',
    UserLocation: 'sx_local_user_location',
    SelCompany: 'sx_sel_company_key',
    SelCompanyProducts: 'sx_sel_company_products_key',
    UserSettings: 'sx_user_settings_key',
    SelProject: 'sx_sel_project_key',
    AdminSelCompany: 'admin_sel_company',
    AdminSelProject: 'admin_sel_project',
};

export const ServiceUrl = [
    {
        name: 'auth',
        url: environment.api_url.auth,
    },
    {
        name: 'company',
        url: environment.api_url.company,
    },
    {
        name: 'pricefile',
        url: environment.api_url.pricefile,
    },
    {
        name: 'project',
        url: environment.api_url.project,
    },
    {
        name: 'commodity',
        url: environment.api_url.commodity,
    },
];

export const ServiceNames = {
    auth: 'auth',
    company: 'company',
    pricefile: 'pricefile',
    project: 'project',
    commodity: 'commodity',
};

export const AVATAR_IMG_URL = '/assets/images/common/noimage.png';
export const NO_IMG_URL = '/assets/images/common/noimage.png';
export const MOBILE_MEDIAQUERY = 'screen and (max-width: 599px)';
export const TABLET_MEDIAQUERY = 'screen and (min-width: 600px) and (max-width: 959px)';
export const MONITOR_MEDIAQUERY = 'screen and (min-width: 960px)';

export const URL_COMMON = 'https://common.iordanov.info/';
export const URL_COMMON_IMAGES = URL_COMMON + 'images/';
export const URL_COMMON_IMAGES_COMPANY = URL_COMMON_IMAGES + 'company/';
export const URL_COMMON_IMAGE_SX = URL_COMMON_IMAGES + 'sx/';
export const URL_COMMON_IMAGE_FLAGS = URL_COMMON_IMAGES + 'flags/';
export const URL_COMMON_IMAGE_COMMODITY = URL_COMMON_IMAGES + 'commodity/';

export const COMMON_IMG_LOGO_RED = URL_COMMON_IMAGE_SX + 'sx-logo-red-3d.png';
export const COMMON_IMG_LOGO_CERCLE = URL_COMMON_IMAGE_SX + 'sx-logo-red-3d.png';
export const COMMON_IMG_AVATAR = URL_COMMON_IMAGES + 'iziordanov_sd_128_bw.png';
export const GMAP_API_KEY = 'AIzaSyDFFY0r9_fLApc2awmt-O1Q2URpkSFbQVc';

export const URL_NO_IMG = 'assets/images/common/noimage.png';
export const URL_NO_IMG_SQ = 'assets/images/common/noimage-sq.jpg';

export const HOUR_MS = (1 * 60 * 60 * 1000);
export const DAY_MS = (24 * HOUR_MS);

export const PAGE_SIZE_OPTIONS = [12, 25, 50, 100, 200];

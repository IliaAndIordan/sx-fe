
export const PageViewType = {
    Card: 'card',
    List: 'list',
    Table: 'table'
};

export const AppRoutes = {
    Root: '',
    guest: 'guest',
    Login: 'login',
    // -- COMMON
    itemas:'items',
    UsersList: 'user-list',
    Users: 'users',
    Create: 'create',
    MyCompany: 'my-company',
    Dashboard: 'dashboard',
    ItemsImport: 'items-import',
    ItemsImportResult: 'items-import-result',
    ItemImportFiles: 'item-import-files',
    ItemImportFile: 'item-import-file',
    // ---Company
    Company: 'company',
    CompanyList: 'company-list',
    // -- Projects
    Projects: 'projects',
    ProjectList: 'projects-list',
    Project: 'project',
    // ---Commodity
    Commodity: 'commodity',
    // -- Manufacturer
    Manufacturer: 'manufacturer',
    // -- Admin
    Admin: 'admin',
    // --Distributor
    Distributor: 'distributor',
    // --
    NotAuthorized: 'not-authorized',
    NoImageAvailable: '//images.tradeservice.com/ProductImages/DIR_TSL/TSL_PRODUCT_NOT_AVAILABLE.png',
    Maintenance: 'under-maintenance',
    SessionSuspended: 'session-suspended'
};

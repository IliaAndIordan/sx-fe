import { Injectable } from '@angular/core';
import {
  CanActivate, CanActivateChild, CanLoad,
  Route, Router, ActivatedRouteSnapshot, RouterStateSnapshot
} from '@angular/router';
import { CurrentUserService } from '../auth/current-user.service';
import { AppRoutes } from '../const/app-routes.const';
import { CompanyType } from '../api/company/enums';

@Injectable()
export class DistributorGuard implements CanActivate, CanActivateChild, CanLoad {

  constructor(
    private cus: CurrentUserService,
    private router: Router) { }

  /**
   * used to prevent the application to load entire
   * modules lazily if the user is not authorize to do so.
   */
  canLoad(route: Route) {
    return true;
  }

  /**
   * used to prevent unauthorized users to access certain routes
   */
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot) {

    if (this.cus.isAuthenticated) {
      const company = this.cus.company;
      if (this.cus.isAdmin ||  (company && company.company_type_id === CompanyType.Distributor))  {
        return true;
      } else {
        this.router.navigate(['/', AppRoutes.Company, AppRoutes.MyCompany]);
        return false;
      }
      /*
        if(this.cus.licenseAgreement){
          return true;
        }else{
          this.router.navigate(['/', AppRoutes.LicenseAgreement]);
          return
        }
        */
    }
    this.router.navigate(['/', AppRoutes.guest], { queryParams: { redirectTo: state.url } });
    return false;
  }

  canActivateChild(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ) {
    return this.canActivate(route, state);
  }
}

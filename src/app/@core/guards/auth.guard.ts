import { Injectable } from '@angular/core';
import {
  CanActivate, CanActivateChild, CanLoad,
  Route, Router, ActivatedRouteSnapshot, RouterStateSnapshot
} from '@angular/router';
import { CurrentUserService } from '../auth/current-user.service';
import { AppRoutes } from '../const/app-routes.const';

@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild, CanLoad {

  constructor(
    private cus: CurrentUserService,
    private router: Router) { }

  /**
   * used to prevent the application to load entire
   * modules lazily if the user is not authorize to do so.
   */
  canLoad(route: Route) {
    return true;
  }

  /**
   * used to prevent unauthorized users to access certain routes
   */
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot) {

    if (this.cus.isAuthenticated) {
      const user = this.cus.user;
      if (user && user.company_id) {
        return true;
      } else {
        this.router.navigate(['/', AppRoutes.Company, 'create']);
        return;
      }
      /*
        if(this.cus.licenseAgreement){
          return true;
        }else{
          this.router.navigate(['/', AppRoutes.LicenseAgreement]);
          return
        }
        */
    }
    this.router.navigate(['/', AppRoutes.guest], { queryParams: { redirectTo: state.url } });
    return false;
  }

  canActivateChild(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ) {
    return this.canActivate(route, state);
  }
}

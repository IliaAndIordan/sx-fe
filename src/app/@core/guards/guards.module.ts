import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthGuard } from './auth.guard';
import { AdminGuard } from './admin.guard';
import { DistributorGuard } from './distributor.guard';
import { MfrGuard } from './mfr.guard';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
  ],
  providers: [
    AuthGuard,
    AdminGuard,
    DistributorGuard,
    MfrGuard,
  ]
})
export class GuardsModule { }

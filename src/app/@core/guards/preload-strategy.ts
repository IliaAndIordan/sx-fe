import { Route, PreloadingStrategy } from '@angular/router';
import { Observable ,  of } from 'rxjs';
import { Injectable } from '@angular/core';

// add  data: {preload: true} to lazy loaded module routes that you would like preloaded
@Injectable()
export class PreloadSelectedModulesList implements PreloadingStrategy {
    preload(route: Route, load: Function): Observable<any> {
        return route.data && route.data.preload ? load() : of(null);
    }
}

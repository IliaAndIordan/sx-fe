import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { Subject } from 'rxjs/Subject';
import { Menu, MenuOpt, MenuItem, Breadcrumb } from '../models/app/menu.model';
import { CurrentUserService } from '../auth/current-user.service';
import { UserRole } from '../auth/api/enums';


@Injectable({ providedIn: 'root', })
export class MenuService {
  /**
   * Fields
   */
  private menu: MenuItem[] = [];

  get hasCompanyId(): boolean {
    return this.cus.user && this.cus.user.company_id ? true : false;
  }
  get isAdmin(): boolean {
    return this.cus.isAdmin;
  }

  get isCompanyAdmin(): boolean {
    return this.cus.isCompanyAdmin;
  }

  get userRole(): UserRole {
    return this.cus.user ? this.cus.user.user_role : undefined;
  }

  constructor(
    private cus: CurrentUserService) {

  }

  enable(mi: MenuItem): boolean {
    let rv = true;
    
    rv = ((mi.hasCompanyId === (this.cus.company ? true : false)) &&
      (mi.maxUserRole >= this.userRole) &&
      (mi.company_type ? mi.company_type === this.cus.companyType : true));

    if (!rv) {
      rv = this.isAdmin;
    }
    return rv;
  }

  getAll(): MenuItem[] {
    return this.menu;
  }

  set(menu: MenuItem[]): MenuItem[] {
    // console.log('set-> menu:', menu);
    this.menu = this.menu.concat(menu);
    // console.log('set-> this.menu:', this.menu);
    return this.menu;
  }

  add(menu: MenuItem) {
    this.menu.push(menu);
  }

  getMenuItemName(stateArr: string[]): string {
    // console.log('getMenuItemName-> stateArr:', stateArr);
    const bc = this.getMenuLevel(stateArr)[stateArr.length - 1];
    return bc ? bc.name : '';
  }

  // TODO:
  getMenuLevel(stateArr: string[]): Breadcrumb[] {
    // console.log('getMenuLevel-> stateArr:', stateArr);
    const tempArr = new Array<Breadcrumb>();;
    let stateLv0: string;
    this.menu.map(item => {
      if (item.state === stateArr[0]) {
        // console.log('getMenuLevel-> stateArr[0] item:', item);
        stateLv0 = item.state;
        const bc = new Breadcrumb(item.name, [stateLv0]);

        tempArr.push(bc);
        // Level1
        if (item.children && item.children.length) {
          item.children.forEach(itemlvl1 => {
            if (stateArr[1] && itemlvl1.state === stateArr[1]) {
              // console.log('getMenuLevel-> Level1 item:', itemlvl1);
              // tempArr.push(itemlvl1.name);
              const bc1 = new Breadcrumb(itemlvl1.name, [stateLv0, itemlvl1.state]);
              tempArr.push(bc1);
              // Level2
              if (itemlvl1.children && itemlvl1.children.length) {
                itemlvl1.children.forEach(itemlvl2 => {
                  if (stateArr[2] && itemlvl2.state === stateArr[2]) {
                    const bc2 = new Breadcrumb(itemlvl2.name, [stateLv0, itemlvl2.state]);
                    tempArr.push(bc2);
                    // tempArr.push(itemlvl2.name);
                  }
                });
              }
            } else if (stateArr[1]) {
              // Level2
              if (itemlvl1.children && itemlvl1.children.length) {
                itemlvl1.children.forEach(itemlvl2 => {
                  if (itemlvl2.state === stateArr[1]) {
                    const bc2 = new Breadcrumb(itemlvl2.name, [stateLv0, itemlvl1.state, itemlvl2.state]);
                    tempArr.push(bc2);
                    // console.log('getMenuLevel-> Level2 item:', itemlvl2);
                    // tempArr.push(itemlvl1.name, itemlvl2.name);
                  }
                });
              }
            }
          });
        }
      }
    });
    // console.log('getMenuLevel-> tempArr:', tempArr);

    return tempArr;
  }
}

import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
// Services
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
import { MfrService } from '../../manufacturer/mfr.service';
// Models
import { CompanyModel } from 'src/app/@core/api/company/dto';
import { AppStore } from 'src/app/@core/const/app-local-storage.const';
import { DataField, ItemImportColumnModel } from '../models/common/item-import-column.model';
import { FileImportModel, MfrItemImportModel, ResponseMfrItemImport, ResponseMfrItemImportFileCreate } from '../api/mfr/item/dto';
import { MfrItemApiClient } from '../api/mfr/item/api-client';

const MFR_ITEM_RAW_DATA_KEY = 'mfr-item-raw-item-data';
const MFR_FILE_INFO_KEY = 'mfr-file-info';
const MFR_COLUMNS_MAPPING_KEY = 'mfr-columns-mapping';

@Injectable({ providedIn: 'root', })
export class MfrItemImportService {

    /**
     *  Fields
     */
    public mfrItemImportData: Array<MfrItemImportModel>;

    constructor(
        private cus: CurrentUserService,
        private mfrClient: MfrItemApiClient) {
    }

    //#region Company

    selCompanyChanged = new Subject<CompanyModel>();

    get selCompany(): CompanyModel {
        const onjStr = localStorage.getItem(AppStore.SelCompany);
        let obj: CompanyModel;
        if (onjStr) {
            obj = Object.assign(new CompanyModel(), JSON.parse(onjStr));
        }
        return obj;
    }

    set selCompany(value: CompanyModel) {
        const oldValue = this.selCompany;
        if (value) {
            localStorage.setItem(AppStore.SelCompany, JSON.stringify(value));
        } else {
            localStorage.removeItem(AppStore.SelCompany);
        }

        this.selCompanyChanged.next(value);
    }

    //#endregion

    //#region Item Data

    get fileInfo(): FileImportModel {
        const onjStr = localStorage.getItem(MFR_FILE_INFO_KEY);
        let obj: FileImportModel;
        if (onjStr) {
            obj = Object.assign(new FileImportModel(), JSON.parse(onjStr));
        }
        return obj;
    }

    set fileInfo(value: FileImportModel) {
        if (value) {
            localStorage.setItem(MFR_FILE_INFO_KEY, JSON.stringify(value));
        } else {
            localStorage.removeItem(MFR_FILE_INFO_KEY);
        }
    }

    get rawItemData(): Array<any> {
        const onjStr = localStorage.getItem(MFR_ITEM_RAW_DATA_KEY);
        let obj: Array<any>;
        if (onjStr) {
            obj = Object.assign(new Array<any>(), JSON.parse(onjStr));
        }
        return obj;
    }

    set rawItemData(value: Array<any>) {
        if (value) {
            localStorage.setItem(MFR_ITEM_RAW_DATA_KEY, JSON.stringify(value));
        } else {
            localStorage.removeItem(MFR_ITEM_RAW_DATA_KEY);
        }
    }

    rawItemDataFields(headerRowIndex: number = 0): Array<DataField> {
        //  console.log('getColumns called with data:',data, ' skipRows:', skipRows);
        const columns = new Array<DataField>();
        if (this.rawItemData && this.rawItemData.length > headerRowIndex) {
            const header: [] = this.rawItemData[headerRowIndex];
            if (header && header.length > 0) {
                for (let idx = 0; idx < header.length; idx++) {
                    const fieldName = header[idx];
                    const df = new DataField();
                    df.idx = idx;
                    df.name = fieldName;
                    df.value = headerRowIndex < this.rawItemData.length ?
                        this.rawItemData[headerRowIndex + 1][idx] ?
                            this.rawItemData[headerRowIndex + 1][idx] : 'EMPTY' : 'END OF FILE';
                    columns.push(df);
                }

            }

        }
        return columns;
    }

    get columnMappings(): Array<ItemImportColumnModel> {
        const onjStr = localStorage.getItem(MFR_COLUMNS_MAPPING_KEY);
        let obj: Array<ItemImportColumnModel>;
        if (onjStr) {
            obj = Object.assign(new Array<ItemImportColumnModel>(), JSON.parse(onjStr));
        }
        return obj;
    }

    set columnMappings(value: Array<ItemImportColumnModel>) {
        if (value) {
            localStorage.setItem(MFR_COLUMNS_MAPPING_KEY, JSON.stringify(value));
        } else {
            localStorage.removeItem(MFR_COLUMNS_MAPPING_KEY);
        }
    }

    genMfrItemImportArray(): Array<MfrItemImportModel> {
        this.mfrItemImportData = new Array<MfrItemImportModel>();
        const columns = this.columnMappings;
        const fileData = this.rawItemData;
        let canProceed = fileData && fileData.length > 0 && columns && columns.length > 0;
        if (!canProceed) {
            return this.mfrItemImportData;
        }

        const required: ItemImportColumnModel[] = columns.filter(col => col.required && !col.dataField);
        const mapped: ItemImportColumnModel[] = columns.filter(col => col.dataField);
        console.log('mapped -> ', mapped);
        canProceed = required.length === 0 && mapped.length > 0;

        console.log('canProceed -> ', canProceed);
        if (!canProceed) {
            return this.mfrItemImportData;
        }

        for (let idx = 1; idx < fileData.length; idx++) {

            const row: Array<any> = fileData[idx];
            // console.log('row -> ', row);
            const item = this.getMfrItemImportModel(row);
            // console.log('item -> ', item);
            this.mfrItemImportData.push(item);
        }

        return this.mfrItemImportData;
    }

    private getMfrItemImportModel(row: Array<any>): MfrItemImportModel {

        const item = new MfrItemImportModel();
        const columns = this.columnMappings;
        item.user_id = this.cus.user.user_id;
        item.mfr_company_id = this.selCompany.company_id;
        let col = columns.find(x => x.dbColumn === 'item_status_id');
        if (col && col.dataField) {
            item.item_status_id = parseInt(row[col.dataField.idx] ? row[col.dataField.idx] : '2', 10);
        }
        col = columns.find(x => x.dbColumn === 'upc');
        if (col && col.dataField) {
            item.upc = row[col.dataField.idx] ? row[col.dataField.idx] : undefined;
        }
        col = columns.find(x => x.dbColumn === 'ean');
        if (col && col.dataField) {
            item.ean = row[col.dataField.idx] ? row[col.dataField.idx] : undefined;
        }
        col = columns.find(x => x.dbColumn === 'gtin');
        if (col && col.dataField) {
            item.gtin = row[col.dataField.idx] ? row[col.dataField.idx] : undefined;
        }
        col = columns.find(x => x.dbColumn === 'mfr_item_pik');
        if (col && col.dataField) {
            item.mfr_item_pik = row[col.dataField.idx] ? parseInt(row[col.dataField.idx], 10) : undefined;
        }
        col = columns.find(x => x.dbColumn === 'mfr_catalog_code');
        if (col && col.dataField) {
            item.mfr_catalog_code = row[col.dataField.idx] ? row[col.dataField.idx] : undefined;
        }
        col = columns.find(x => x.dbColumn === 'mfr_item_name');
        if (col && col.dataField) {
            item.mfr_item_name = row[col.dataField.idx] ? row[col.dataField.idx] : undefined;
        }
        col = columns.find(x => x.dbColumn === 'mfr_ucc');
        if (col && col.dataField) {
            item.mfr_ucc = row[col.dataField.idx] ? row[col.dataField.idx] : undefined;
        }
        col = columns.find(x => x.dbColumn === 'mfr_country_of_origin_code');
        if (col && col.dataField) {
            item.mfr_country_of_origin_code = row[col.dataField.idx] ? row[col.dataField.idx] : undefined;
        }
        col = columns.find(x => x.dbColumn === 'industry_id');
        if (col && col.dataField) {
            item.industry_id = parseInt(row[col.dataField.idx] ? row[col.dataField.idx] : '2', 10);
        }
        col = columns.find(x => x.dbColumn === 'mfr_item_description');
        if (col && col.dataField) {
            item.mfr_item_description = row[col.dataField.idx] ? row[col.dataField.idx] : undefined;
        }
        col = columns.find(x => x.dbColumn === 'invoice_description');
        if (col && col.dataField) {
            item.invoice_description = row[col.dataField.idx] ? row[col.dataField.idx] : undefined;
        }
        col = columns.find(x => x.dbColumn === 'image_url');
        if (col && col.dataField) {
            item.image_url = row[col.dataField.idx] ? row[col.dataField.idx] : undefined;
        }
        col = columns.find(x => x.dbColumn === 'thumb_url');
        if (col && col.dataField) {
            item.thumb_url = row[col.dataField.idx] ? row[col.dataField.idx] : undefined;
        }
        col = columns.find(x => x.dbColumn === 'web_thumb_ur');
        if (col && col.dataField) {
            item.web_thumb_ur = row[col.dataField.idx] ? row[col.dataField.idx] : undefined;
        }
        col = columns.find(x => x.dbColumn === 'brand_name');
        if (col && col.dataField) {
            item.brand_name = row[col.dataField.idx] ? row[col.dataField.idx] : undefined;
        }
        col = columns.find(x => x.dbColumn === 'item_product_id');
        if (col && col.dataField) {
            item.item_product_id = row[col.dataField.idx] ? parseInt(row[col.dataField.idx], 10) : undefined;
        }
        col = columns.find(x => x.dbColumn === 'lead_free');
        if (col && col.dataField) {
            item.lead_free = row[col.dataField.idx] ? parseInt(row[col.dataField.idx], 10) === 1 : false;
        }
        col = columns.find(x => x.dbColumn === 'buy_amerika_complain');
        if (col && col.dataField) {
            item.buy_amerika_complain = row[col.dataField.idx] ? parseInt(row[col.dataField.idx], 10) === 1 : false;
        }
        col = columns.find(x => x.dbColumn === 'edms_pik');
        if (col && col.dataField) {
            item.edms_pik = row[col.dataField.idx] ? parseInt(row[col.dataField.idx], 10) : undefined;
        }
        col = columns.find(x => x.dbColumn === 'item_id');
        if (col && col.dataField) {
            item.item_id = row[col.dataField.idx] ? parseInt(row[col.dataField.idx], 10) : undefined;
        }
        col = columns.find(x => x.dbColumn === 'mfr_item_id');
        if (col && col.dataField) {
            item.mfr_item_id = row[col.dataField.idx] ? parseInt(row[col.dataField.idx], 10) : undefined;
        }

        return item;
    }

    //#endregion

    importItems(items: MfrItemImportModel[]): Observable<ResponseMfrItemImport> {
        return new Observable<ResponseMfrItemImport>(subscriber => {
            this.mfrClient.mfrItemsImport(items, this.fileInfo)
                .subscribe((resp: ResponseMfrItemImport) => {
                    console.log('importItems -> resp:', resp);
                    // this.spinerService.display(false);
                    // console.log('importItems -> resp:', resp);
                    subscriber.next(resp);
                });
        });
    }

    importItemsPromise(items: MfrItemImportModel[]): Promise<ResponseMfrItemImport> {
        return new Promise<ResponseMfrItemImport>((resolve, reject) => {
            this.mfrClient.mfrItemsImport(items, this.fileInfo).toPromise()
                .then((resp: ResponseMfrItemImport) => {
                    // this.spinerService.display(false);
                    // console.log('importItems -> resp:', resp);
                    resolve(resp);
                });
        });

    }

    importFileSave(fi: FileImportModel): Observable<ResponseMfrItemImportFileCreate> {
        return new Observable<ResponseMfrItemImportFileCreate>(subscriber => {
            this.mfrClient.mfrItemsImportFileSave( fi)
                .subscribe((resp: ResponseMfrItemImportFileCreate) => {
                    // console.log('importFileCreate -> resp:', resp);
                    if (resp && resp.data && resp.data.file_import && resp.data.file_import.length > 0) {
                        const finfo = FileImportModel.fromJSON(resp.data.file_import[0]);
                        this.fileInfo = finfo;
                    }
                    subscriber.next(resp);
                });
        });
    }

    importFileDeleteItems(fi: FileImportModel): Observable<ResponseMfrItemImportFileCreate> {
        return new Observable<ResponseMfrItemImportFileCreate>(subscriber => {
            this.mfrClient.mfrItemsImportDeleteByFileId( fi)
                .subscribe((resp: ResponseMfrItemImportFileCreate) => {
                    // console.log('importFileCreate -> resp:', resp);
                    if (resp && resp.data && resp.data.file_import && resp.data.file_import.length > 0) {
                        const finfo = FileImportModel.fromJSON(resp.data.file_import[0]);
                        this.fileInfo = finfo;
                    }
                    subscriber.next(resp);
                });
        });
    }
}


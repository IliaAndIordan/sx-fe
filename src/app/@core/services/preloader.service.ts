import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { SpinnerService } from './spinner.service';

@Injectable({
  providedIn: 'root',
})
export class PreloaderService {
  isOpen = true;
  isOpenChanged = new Subject<boolean>();

  selector = 'globalLoader';

  constructor(private spinnerService: SpinnerService) {}

  private getElement() {
    return document.getElementById(this.selector);
  }

  hide() {
    this.isOpen = false;
    this.isOpenChanged.next(this.isOpen);
    /*
    const el = this.getElement();
    console.log('PreloaderService hide-> el', el);
    if (el) {
      el.addEventListener('transitionend', () => {
        el.className = 'global-loader-hidden';
      });

      el.className = 'global-loader-hidden';
    }
    */
  }

  show() {
    this.isOpen = true;
    this.isOpenChanged.next(this.isOpen);
    /*
    const el = this.getElement();
    console.log('PreloaderService show -> el', el);
    if (el) {
      el.className = 'global-loader global-loader-fade-in';
    }
    */
  }
}

import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class SpinnerService {

    public status: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    public message: BehaviorSubject<string> = new BehaviorSubject<string>(null);


    display(value: boolean) {
        // clear any message
        if (!value) {
            this.message.next(null);
        }
        this.status.next(value);
    }

    setMessage(value: string) {
        this.message.next(value);
    }

}
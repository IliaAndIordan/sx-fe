import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';

import { MenuService } from './menu.service';
import { MenuOpt } from '../models/app/menu.model';

@Injectable()
export class StartupService {
  constructor(private menuService: MenuService, private http: HttpClient) {}

  load(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.menuService.set(MenuOpt);
      resolve();
      /*
      this.http
        .get('/assets/data/menu.json')
        .pipe(
          catchError(res => {
            resolve();
            return res;
          })
        )
        .subscribe(
          (res: any) => {
            this.menuService.set(res.menu);
          },
          () => {},
          () => {
            resolve();
          }
        );*/
    });
  }
}

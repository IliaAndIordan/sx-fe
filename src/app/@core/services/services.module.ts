import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuService } from './menu.service';
import { PreloaderService } from './preloader.service';
import { SettingsService } from './settings.service';
import { StartupService } from './startup.service';
import { HttpClientModule } from '@angular/common/http';
import { SpinnerService } from './spinner.service';
import { MfrItemImportService } from './mfr-item-import-service';



@NgModule({
    declarations: [
    ],
    imports: [
        CommonModule,
        HttpClientModule,
    ],
    exports: [
    ],
    providers:[
        MenuService,
        PreloaderService,
        SettingsService,
        StartupService,
        SpinnerService,
        MfrItemImportService,

    ]
})
export class ServicesModule { }

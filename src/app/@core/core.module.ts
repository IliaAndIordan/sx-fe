import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { throwIfAlreadyLoaded } from './guards/module-import-guard';
import { ServicesModule } from './services/services.module';
import { HttpClientModule } from '@angular/common/http';
import { JwtModule, JWT_OPTIONS } from '@auth0/angular-jwt';
import { TokenFactory } from './auth/token-factory';
import { SxAuthModule } from './auth/auth.module';
import { SxApiClientsModule } from './api/api-clients.module';
import { GravatarModule } from '@infinitycube/gravatar';
import { GuardsModule } from './guards/guards.module';
import { SxCommonPipesModule } from './common-pipes.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    SxCommonPipesModule,
    GravatarModule,
    // --- Local
    ServicesModule,
    SxApiClientsModule,
    GuardsModule,
  ]
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    throwIfAlreadyLoaded(parentModule, 'CoreModule');
  }
}

import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SxAuthModule } from '../auth/auth.module';
import { SxApiCompanyClient } from './company/api-client';
import { CompanyTypeDisplayPipe } from './company/enums';
import { SxApiCountryClient } from './country/api-client';
import { SxUserClient } from './user/api-client';
import { UserRoleDisplayPipe } from '../auth/api/enums';
import { SxApiPriceFileClient } from './price_file/api-client';
import { SxApiProjectClient } from './project/api-client';
import { MfrApiClientsModule } from './mfr/mfr-clients.module';
// Services

@NgModule({

  imports: [
    CommonModule,
    //
    SxAuthModule,
    MfrApiClientsModule,
  ],
  declarations: [

  ],
  exports: [
    SxAuthModule,
    MfrApiClientsModule,
  ],
  providers: [
    // ---Clients
    SxApiCompanyClient,
    SxUserClient,
    SxApiCountryClient,
    SxApiPriceFileClient,
    SxApiProjectClient,
    // ---Pipes
    CompanyTypeDisplayPipe,
  ]

})

export class SxApiClientsModule { }

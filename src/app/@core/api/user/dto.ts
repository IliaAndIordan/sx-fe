import { ResponseModel } from '../responce.model';
import { IUserModel } from '../../auth/api/dto';


export class ResponseUserSaveData {
    public user: IUserModel;
}

export interface ResponseUserSave extends ResponseModel {
    data: ResponseUserSaveData;
}

import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpParams, HttpResponse, HttpHeaders, HttpHeaderResponse } from '@angular/common/http';
import { Subject, Observable, of, BehaviorSubject } from 'rxjs';
// Services
import { ApiBaseClient } from '../../auth/api-base.client';
import { ApiHelper } from '../../auth/api.helper';
import { CurrentUserService } from '../../auth/current-user.service';
// --- Models
import { ServiceNames } from '../../const/app-local-storage.const';
import { AppRoutes } from '../../const/app-routes.const';
import { tap, catchError, map } from 'rxjs/operators';
import { resolve } from 'dns';
import { UserModel } from '../../auth/api/dto';
import { ResponseUserSave } from './dto';


// import { JwtHelperService } from '@auth0/angular-jwt';


@Injectable()
export class SxUserClient extends ApiBaseClient {


  constructor(
    private client: HttpClient,
    private apiHelper: ApiHelper,
    private cus: CurrentUserService) {
    super();
  }

  private get sertviceUrl(): string {
    return this.apiHelper.getServiceUrl(ServiceNames.auth);
  }

  //#region User

  userSave(value: UserModel): Observable<UserModel> {
    console.log('userSave-< value', value);

    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'user_save',
      { user: value },
      { headers: hdrs }).pipe(
        map((res: ResponseUserSave) => {
          console.log('userSave-> res=', res);
          let rv: UserModel;
          if (res && res.success) {
            rv = UserModel.fromJSON(res.data.user);
          }
          return rv;
        }),
        catchError(this.handleError)
      );

  }


  //#endregion
}

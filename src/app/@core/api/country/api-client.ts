import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpParams, HttpResponse, HttpHeaders, HttpHeaderResponse } from '@angular/common/http';
import { Subject, Observable, of, BehaviorSubject } from 'rxjs';
// Services
import { ApiBaseClient } from '../../auth/api-base.client';
import { ApiHelper } from '../../auth/api.helper';
import { CurrentUserService } from '../../auth/current-user.service';
// --- Models
import { ServiceNames } from '../../const/app-local-storage.const';
import { AppRoutes } from '../../const/app-routes.const';
import { CountryModel, ICountryModel, ResponseCountryListModel } from './dto';
import { CompanyModel } from '../company/dto';
import { tap, catchError, map } from 'rxjs/operators';
import { resolve } from 'dns';


// import { JwtHelperService } from '@auth0/angular-jwt';


@Injectable()
export class SxApiCountryClient extends ApiBaseClient {

    protected countryListObs: Observable<any>;
    protected countryListData: Array<CountryModel>;
    protected countryListTime: Date;

    constructor(
        private client: HttpClient,
        private apiHelper: ApiHelper,
        private cus: CurrentUserService) {
        super();
    }

    private get sertviceUrl(): string {
        return this.apiHelper.getServiceUrl(ServiceNames.company) ;
    }

    //#region Company



    //#endregion

    //#region  CASHE Country List

    get countryList(): Observable<Array<CountryModel>> {

        const data = this.countryListData;
        if (data && data.length > 0) {
            // console.log('countryList -> data:', data);
            // console.log('countryList -> time:', this.countryListTime);
        }

        if (data && data.length > 0) {
            return of(data);
        } else {

            return new Observable<Array<CountryModel>>(subscriber => {
                this.postCountryList()
                    .subscribe((resp: Array<CountryModel>) => {
                        this.countryListData = resp;
                        this.countryListTime = new Date();
                        subscriber.next(resp);
                    });
            });

        }

    }


    postCountryList(): Observable<any> {
        // const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' });
        const url = this.sertviceUrl + 'country_list';
        const req =  { user_id: this.cus.user.user_id };
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                observe: 'response',
            }),
        };
        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' });
        return this.client.post<HttpResponse<any>>(url, req, { headers: hdrs, observe: 'response' })
            .pipe(
                map((response: HttpResponse<any>) => {
                    // console.log('loadCountryList-> response=', response);
                    let rv: Array<CountryModel>;
                    if (response.body) {
                        const res = Object.assign(new ResponseCountryListModel(), response.body);
                        // console.log('loadCountryList-> res=', res);
                        if (res && res.data && res.data.country_list && res.data.country_list.length > 0) {
                            this.countryListData = new Array<CountryModel>();
                            res.data.country_list.forEach((element: ICountryModel) => {
                                const tmp: CountryModel = CountryModel.fromJSON(element);
                                this.countryListData.push(tmp);
                            });
                            // console.log('loadCountryList ->  countryListData', this.countryListData);
                            this.countryListTime = new Date();
                            rv = this.countryListData;
                        }

                    }
                    return rv;
                }),
                catchError(this.handleError)
            );
    }


    //#endregion
}

import { DateModel, IDateModel } from '../../models/common/date.model';
import { ResponseModel } from '../responce.model';
import { NO_IMG_URL, URL_COMMON_IMAGE_FLAGS } from '../../const/app-local-storage.const';
import { CompanyType, CompanyStatus } from './enums';


export class CountryModel {
    country_id: number;
    country_name: string;
    country_iso2: string;
    region_name: string;
    subregion_name: string;
    region_id: number;
    subregion_id: number;

    flagUrl: string;


    public static fromJSON(json: ICountryModel): CountryModel {
        const vs = Object.create(CountryModel.prototype);
        return Object.assign(vs, json, {
            flagUrl: URL_COMMON_IMAGE_FLAGS + json.country_iso2.toLowerCase() + '.png',
        });
    }

    // reviver can be passed as the second parameter to JSON.parse
    // to automatically call User.fromJSON on the resulting value.
    public static reviver(key: string, value: any): any {

        return key === '' ? CountryModel.fromJSON(value) : value;
    }

    public toJSON(): ICountryModel {
        const vs = Object.create(CountryModel.prototype);
        return Object.assign(vs, this);
    }
}

export interface ICountryModel {
    country_id: number;
    country_name: string;
    country_iso2: string;
    region_name: string;
    subregion_name: string;
    region_id: number;
    subregion_id: number;
}

export class DataCountryListModel {
    country_list: Array<ICountryModel>;
}

export class ResponseCountryListModel extends ResponseModel {
    data: DataCountryListModel;
}


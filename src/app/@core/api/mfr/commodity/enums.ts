import { EnumViewModel } from '../../../models/common/enum-view.model';
import { Pipe, PipeTransform } from '@angular/core';

//#region CommodityType

export enum CommodityType {
    CSI2010 = 2,
    CSI2016 = 3,
    Archive = 4,
}


export const CommodityTypeOpt: EnumViewModel[] = [
    {
        id: 2,
        name: 'CSI 2010'
    },
    {
        id: 3,
        name: 'CSI 2016'
    }
];


@Pipe({ name: 'commoditytype' })
export class CommodityDisplayDisplayPipe implements PipeTransform {
    transform(value, args?: string[]): string {
        let rv: string = CommodityType[value];
        const data: EnumViewModel = CommodityTypeOpt.find(x => x.id === value);
        if (data) {
            rv = data.name;
        }
        return rv;
    }
}

//#endregion



import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpParams, HttpResponse, HttpHeaders, HttpHeaderResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { catchError, map, tap } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
// Services
import { ApiBaseClient } from '../../../auth/api-base.client';
import { ApiHelper } from '../../../auth/api.helper';
import { CurrentUserService } from '../../../auth/current-user.service';
// --- Models
import { ServiceNames } from '../../../const/app-local-storage.const';
import { AppRoutes } from '../../../const/app-routes.const';
import { LocalUserSettings } from '../../../models/app/settings';
import { ResponseCommodityList } from './dto';
import { CommodityType } from './enums';
// Models


@Injectable()
export class CommodityApiClient extends ApiBaseClient {

    constructor(
        private client: HttpClient,
        private apiHelper: ApiHelper,
        private cus: CurrentUserService) {
        super();
    }

    private get sertviceUrl(): string {
        return this.apiHelper.getServiceUrl(ServiceNames.commodity);
    }

    //#region Project

    commodityList(
        orderCol: string = 'display_idx',
        isDesc: boolean = false,
        commodityType: number = CommodityType.CSI2016,
        parentId?: number): Observable<ResponseCommodityList> {

        console.log('commodityList-> commodityType=', commodityType);
        console.log('commodityList-> parentId=', parentId);

        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.client.post(this.sertviceUrl + 'commodity_list',
            {
                commodity_type_id: commodityType,
                parent_commodity_id: parentId,
                qry_orderCol: orderCol,
                qry_isDesc: isDesc,
            },
            { headers: hdrs }).pipe(
                map((resp: ResponseCommodityList) => {
                    const res = Object.assign(new ResponseCommodityList(), resp);
                    console.log('commodityList -> response=', res);
                    return res;
                }),
                tap(event => {
                    this.log(`tap commodityList event: ` + JSON.stringify(event));
                    // There may be other events besides the response.
                    if (event instanceof HttpResponse) {
                        // cache.put(req, event); // Update the cache.
                    }
                }),
                catchError(this.handleError)
            );
    }


    //#endregion


}

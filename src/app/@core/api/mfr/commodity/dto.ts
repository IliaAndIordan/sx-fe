import { DateModel, IDateModel } from '../../../models/common/date.model';
import { ResponseModel } from '../../responce.model';
import { URL_COMMON_IMAGE_COMMODITY } from '../../../const/app-local-storage.const';
import { IUserTimestampModel } from '../../../auth/api/dto';
import { CommodityType } from './enums';
import { timeStamp } from 'console';

//#region SxProjectModel

export class CommodityModel {
    public commodity_id: number;
    public parent_commodity_id: number;
    public commodity_type_id: CommodityType;
    public commodity_code: string;
    public commodity_name: string;

    public commodity_description: string;
    public estimator_name: string;
    public commodity_image_url: string;
    image_url: string;
    public display_idx: number;

    public electrical: boolean;
    public hvacr: boolean;
    public industrial: boolean;
    public plumbing: boolean;

    public static fromJSON(json: ICommodityModel): CommodityModel {
        const vs = Object.create(CommodityModel.prototype);
        return Object.assign(vs, json, {
            commodity_type_id: json.commodity_type_id ? json.commodity_type_id as CommodityType : CommodityType.CSI2010,
            image_url: URL_COMMON_IMAGE_COMMODITY + json.commodity_code.replace(/\s/g, '') + '.png',
            electrical: json.electrical ** json.electrical === 1 ? true : false,
            hvacr: json.hvacr ** json.hvacr === 1 ? true : false,
            industrial: json.industrial ** json.industrial === 1 ? true : false,
            plumbing: json.plumbing ** json.plumbing === 1 ? true : false,
        });
    }

    // reviver can be passed as the second parameter to JSON.parse
    // to automatically call User.fromJSON on the resulting value.
    public static reviver(key: string, value: any): any {

        return key === '' ? CommodityModel.fromJSON(value) : value;
    }

    public toJSON(): ICommodityModel {
        const vs = Object.create(CommodityModel.prototype);
        return Object.assign(vs, this);
    }
}

export interface ICommodityModel {
    commodity_id: number;
    parent_commodity_id: number;
    commodity_type_id: CommodityType;
    commodity_code: string;
    commodity_name: string;

    commodity_description: string;
    estimator_name: string;
    commodity_image_url: string;

    display_idx: number;
    electrical: number;
    hvacr: number;
    industrial: number;
    plumbing: number;
}

export class ResponseCommodityData {
    commodity: ICommodityModel;

}


export class ResponseCommodity extends ResponseModel {
    data: ResponseCommodityData;
}

export class ResponseCommoditySaveData {
    project: ICommodityModel[];

}

export class ResponseCommoditySave extends ResponseModel {
    data: ResponseCommoditySaveData;
}


export class ResponseCommodityListData {
    commodity_list: ICommodityModel[];
}

export class ResponseCommodityList extends ResponseModel {
    data: ResponseCommodityListData;
}

export class CommodityListCriteria {
    orderCol: string;
    isDesc: boolean;
    commodityType: number = CommodityType.CSI2016;
    parentId?: number;

    limit: number;
    offset: number;

    get pageIndex(): number {
        let rv = 0;
        rv = this.offset ? (Math.floor(this.offset / (this.limit ? this.limit : 25))) : 0;
        return rv;
    }

    set pageIndex(value: number) {
        this.offset = value ? (value) * (this.limit ? this.limit : 25) : 0;
    }

    path?: CommodityModel[];

    addPath(cm: CommodityModel) {
        if (cm) {
            if (!this.path || !cm.parent_commodity_id) {
                this.path = new Array<CommodityModel>();
            }
            this.path.push(cm);
        }
    }

    removePath(cm: CommodityModel) {

        if (cm && this.path && this.path.length > 0) {
            const idx = this.path.findIndex(x => x.commodity_id === cm.commodity_id);
            console.log('removePath-> idx:', idx);
            if (idx !== -1) {
                if (idx === this.path.length) {
                    this.path.splice(idx, 1);
                }
                else {
                    this.path = this.path.slice(0, (idx + 1));
                }
            }

            console.log('removePath-> path:', this.path);
            if (this.path && this.path.length > 0) {
                const cm = this.path[this.path.length - 1];
                this.parentId = cm.commodity_id;
            }
        }
    }

}

//#endregion



import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpParams, HttpResponse, HttpHeaders, HttpHeaderResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { catchError, map, tap } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
// Services
import { ApiBaseClient } from '../../../auth/api-base.client';
import { ApiHelper } from '../../../auth/api.helper';
import { CurrentUserService } from '../../../auth/current-user.service';
// --- Models
import { ServiceNames } from '../../../const/app-local-storage.const';
import { AppRoutes } from '../../../const/app-routes.const';
import { LocalUserSettings } from '../../../models/app/settings';
import { FileImportModel, MfrItemImportModel, ResponseMfrItemImport, ResponseMfrItemImportDeleteByFile, ResponseMfrItemImportFileCreate, ResponseMfrItemImportFileTable } from './dto';
// Models


@Injectable()
export class MfrItemApiClient extends ApiBaseClient {

    constructor(
        private client: HttpClient,
        private apiHelper: ApiHelper,
        private cus: CurrentUserService) {
        super();
    }

    private get sertviceUrl(): string {
        return this.apiHelper.getServiceUrl(ServiceNames.commodity);
    }

    //#region Mfr Items Import

    mfrItemsImportFileSave(fi: FileImportModel): Observable<ResponseMfrItemImportFileCreate> {

        // value.actor_id = this.cus.user.user_id;
        console.log('mfrItemsImportFileSave-> fi=', fi);
        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.client.post(this.sertviceUrl + 'mfr_item_import_file_save',
            {
                mfr_item_import_file: fi,
            },
            { headers: hdrs }).pipe(
                map((response: ResponseMfrItemImportFileCreate) => {
                    // console.log('mfrItemsImportFileCreate-> response=', response);
                    const res = Object.assign(new ResponseMfrItemImportFileCreate(), response);
                    // console.log('companySave-> success=', res.success);
                    return res;
                }),
                tap(event => {
                    this.log(`tap ResponseMfrItemImportFileCreate event: ` + JSON.stringify(event));
                    // There may be other events besides the response.
                    if (event instanceof HttpResponse) {
                        // cache.put(req, event); // Update the cache.
                    }
                }),
                catchError(this.handleError)
            );

    }

    mfrItemsImportDeleteByFileId(fi: FileImportModel): Observable<ResponseMfrItemImportDeleteByFile> {

        // value.actor_id = this.cus.user.user_id;
        console.log('mfrItemsImportFileSave-> fi=', fi);
        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.client.post(this.sertviceUrl + 'mfr_items_import_delete',
            {
                file_import_id: fi.file_import_id,
            },
            { headers: hdrs }).pipe(
                map((response: ResponseMfrItemImportDeleteByFile) => {
                    // console.log('mfrItemsImportFileCreate-> response=', response);
                    const res = Object.assign(new ResponseMfrItemImportDeleteByFile(), response);
                    console.log('companySave-> res=', res);
                    return res;
                }),
                tap(event => {
                    this.log(`tap mfrItemsImportDeleteByFileId event: ` + JSON.stringify(event));
                    // There may be other events besides the response.
                    if (event instanceof HttpResponse) {
                        // cache.put(req, event); // Update the cache.
                    }
                }),
                catchError(this.handleError)
            );

    }

    mfrItemsImportFileTable(
        limit: number = 25,
        offset: number = 0,
        orderCol: string = 'usecase_order',
        isDesc: boolean = false,
        coompanyId?: number,
        filter?: string): Observable<ResponseMfrItemImportFileTable> {

        console.log('mfrItemsImportFileTable-> offset=', offset);
        console.log('mfrItemsImportFileTable-> limit=', limit);

        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.client.post(this.sertviceUrl + 'mfr_item_import_file_table',
            {
                company_id: coompanyId,
                qry_offset: offset,
                qry_limit: limit,
                qry_orderCol: orderCol,
                qry_isDesc: isDesc,
                qry_filter: filter,
            },
            { headers: hdrs }).pipe(
                map((resp: ResponseMfrItemImportFileTable) => {
                    const res = Object.assign(new ResponseMfrItemImportFileTable(), resp);
                    console.log('mfrItemsImportFileTable -> response=', res);
                    return res;
                }),
                tap(event => {
                    this.log(`tap mfrItemsImportFileTable event: ` + JSON.stringify(event));
                    // There may be other events besides the response.
                    if (event instanceof HttpResponse) {
                        // cache.put(req, event); // Update the cache.
                    }
                }),
                catchError(this.handleError)
            );
    }

    mfrItemsImport(value: MfrItemImportModel[], fi?: FileImportModel): Observable<ResponseMfrItemImport> {

        // value.actor_id = this.cus.user.user_id;
        console.log('mfrItemsImport-> value=', value);
        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.client.post(this.sertviceUrl + 'mfr_items_import',
            {
                mfr_items_import: value,
                file_info: fi
            },
            { headers: hdrs }).pipe(
                map((response: ResponseMfrItemImport) => {
                     console.log('mfrItemsImport-> response=', response);
                    const res = Object.assign(new ResponseMfrItemImport(), response);
                    // console.log('companySave-> success=', res.success);
                    return res;
                }),
                tap(event => {
                    this.log(`tap Project save event: ` + JSON.stringify(event));
                    // There may be other events besides the response.
                    if (event instanceof HttpResponse) {
                        // cache.put(req, event); // Update the cache.
                    }
                }),
                catchError(this.handleError)
            );

    }


    //#endregion


}

import { DateModel, IDateModel } from '../../../models/common/date.model';
import { ResponseModel } from '../../responce.model';
import { URL_COMMON_IMAGE_COMMODITY } from '../../../const/app-local-storage.const';
import { IUserTimestampModel } from '../../../auth/api/dto';
import { IndustryType, ItemProcessingStatus, ItemStatus } from './enums';
import { timeStamp } from 'console';
import { jitOnlyGuardedExpression } from '@angular/compiler/src/render3/util';
import { TableCriteriaBase } from 'src/app/@core/models/common/table-criteria.model';

//#region Item Import

export class FileImportModel {
    public file_import_id: number;
    public file_name: string;
    public file_size: number;
    public file_rows: number;
    public file_data_fields: number;
    public file_header: string;
    public company_id: number;

    public user_id: number;
    public processing_status_id: ItemProcessingStatus;
    public stat_rows_total: number;
    public stat_rows_processed_ok: number;
    public adate: string;
    public udate: string;

    public user_name?: string;
    public company_name?: string;

    public static fromJSON(json: IFileImportModel): FileImportModel {
        const vs = Object.create(FileImportModel.prototype);
        return Object.assign(vs, json, {
            processing_status_id: json.processing_status_id ?
                json.processing_status_id as ItemProcessingStatus : ItemProcessingStatus.NotProcessed,
            adate: (json && json.adate) ? new Date(json.adate) : undefined,
            udate: (json && json.udate) ? new Date(json.udate) : undefined,
        });
    }

    // reviver can be passed as the second parameter to JSON.parse
    // to automatically call User.fromJSON on the resulting value.
    public static reviver(key: string, value: any): any {

        return key === '' ? FileImportModel.fromJSON(value) : value;
    }

    public toJSON(): IFileImportModel {
        const vs = Object.create(FileImportModel.prototype);
        return Object.assign(vs, this);
    }
}

export interface IFileImportModel {
    file_import_id: number;
    file_name: string;
    file_size: number;
    file_rows: number;
    file_data_fields: number;
    file_header: string;
    company_id: number;
    user_id: number;
    processing_status_id: number;
    stat_rows_total: number;
    stat_rows_processed_ok: number;
    adate: string;
    udate: string;

    user_name?: string;
    company_name?: string;
}


export class MfrItemImportModel {
    public item_status_id: ItemStatus;
    public upc?: string;
    public ean?: string;
    public gtin?: string;
    public mfr_item_pik?: number;
    public mfr_company_id: number;
    public mfr_catalog_code: string;
    public mfr_item_name: string;
    public mfr_ucc?: string;
    public mfr_country_of_origin_code: string;
    public country_of_origin_id?: number;
    public industry_id: IndustryType;
    public mfr_item_description?: string;
    public invoice_description?: string;
    public image_url?: string;
    public thumb_url?: string;
    public web_thumb_ur?: string;
    public brand_name?: string;
    public item_product_id?: number;
    public lead_free?: boolean;
    public buy_amerika_complain?: boolean;
    public edms_pik?: number;
    public item_id?: number;
    public mfr_item_id?: number;
    public adate?: string;
    public user_id: number;
}

export class ResponseMfrItemImportData {
    affected_row_ids: Array<number>;
    file_info: IFileImportModel;

}


export class ResponseMfrItemImport extends ResponseModel {
    data: ResponseMfrItemImportData;
}

//#region  Files

export class ResponseMfrItemImportFile {
    file_import: Array<IFileImportModel>;

}


export class ResponseMfrItemImportFileCreate extends ResponseModel {
    data: ResponseMfrItemImportFile;
}

export class ResponseMfrItemImportDeleteByFileData {
    file_import: Array<IFileImportModel>;
    affected_rows: Array<number>;

}


export class ResponseMfrItemImportDeleteByFile extends ResponseModel {
    data: ResponseMfrItemImportDeleteByFileData;
}



export class MfrItemImportFileTableCriteria extends TableCriteriaBase {

}


export class ResponseMfrItemImportFileTableData {
    file_list: IFileImportModel[];
    totals: {
        total_rows: number;
    };
}

export class ResponseMfrItemImportFileTable extends ResponseModel {
    data: ResponseMfrItemImportFileTableData;
}



//#endregion



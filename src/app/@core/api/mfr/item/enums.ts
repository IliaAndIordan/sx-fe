import { EnumViewModel } from '../../../models/common/enum-view.model';
import { Pipe, PipeTransform } from '@angular/core';

//#region IndustryType

export enum IndustryType {
    Plumbing = 1,
    Industrial = 2,
    HVACR = 3,
    Electrical = 4,
}


export const IndustryTypeOpt: EnumViewModel[] = [
    {
        id: 1,
        name: 'Plumbing'
    },
    {
        id: 2,
        name: 'Industrial'
    },
    {
        id: 3,
        name: 'HVACR'
    },
    {
        id: 4,
        name: 'Electrical'
    }
];


@Pipe({ name: 'industrytype' })
export class IndustryTypeDisplayDisplayPipe implements PipeTransform {
    transform(value, args?: string[]): string {
        let rv: string = IndustryType[value];
        const data: EnumViewModel = IndustryTypeOpt.find(x => x.id === value);
        if (data) {
            rv = data.name;
        }
        return rv;
    }
}

//#endregion

//#region ItemStatus

export enum ItemStatus {
    Plumbing = 1,
    Industrial = 2,
    HVACR = 3,
    Electrical = 4,
}


export const ItemStatusOpt: EnumViewModel[] = [
    {
        id: 1,
        name: 'Discontinued'
    },
    {
        id: 2,
        name: 'User Added'
    },
    {
        id: 3,
        name: 'Inactive'
    },
    {
        id: 4,
        name: 'Deleted'
    },
    {
        id: 5,
        name: 'SX Only'
    },
    {
        id: 6,
        name: 'Active'
    }
];


@Pipe({ name: 'itemstatus' })
export class ItemStatusDisplayDisplayPipe implements PipeTransform {
    transform(value, args?: string[]): string {
        let rv: string = ItemStatus[value];
        const data: EnumViewModel = ItemStatusOpt.find(x => x.id === value);
        if (data) {
            rv = data.name;
        }
        return rv;
    }
}

//#endregion

//#region ItemProcessingStatus

export enum ItemProcessingStatus {
    NotProcessed = 1,
    ProcessedOK = 2,
    ProcessedWithError = 3,
}


export const ItemProcessingStatusOpt: EnumViewModel[] = [
    {
        id: 1,
        name: 'Not Processed'
    },
    {
        id: 2,
        name: 'Processed OK'
    },
    {
        id: 3,
        name: 'Processed With Error'
    },

];


@Pipe({ name: 'itemprocessingstatus' })
export class ItemProcessingStatusDisplayPipe implements PipeTransform {
    transform(value, args?: string[]): string {
        let rv: string = ItemProcessingStatus[value];
        const data: EnumViewModel = ItemProcessingStatusOpt.find(x => x.id === value);
        if (data) {
            rv = data.name;
        }
        return rv;
    }
}

//#endregion

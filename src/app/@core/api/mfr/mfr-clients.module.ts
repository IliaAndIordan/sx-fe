import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SxAuthModule } from '../../auth/auth.module';
import { CommodityApiClient } from './commodity/api-client';
import { CommodityDisplayDisplayPipe } from './commodity/enums';
import { MfrItemApiClient } from './item/api-client';

// Services

@NgModule({

  imports: [
    CommonModule,
    //
    SxAuthModule,
  ],
  declarations: [
  ],
  exports: [
  ],
  providers: [
    // ---Clients
    CommodityApiClient,
    MfrItemApiClient,
  ]

})

export class MfrApiClientsModule { }

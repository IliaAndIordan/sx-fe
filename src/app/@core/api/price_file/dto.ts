import { ResponseModel } from '../responce.model';

export class ResponcePriceFileUploadData {
    price_file_id: number;
}

export class ResponcePriceFileUpload extends ResponseModel {
    data: ResponcePriceFileUploadData;
}
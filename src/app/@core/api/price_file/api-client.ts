import { Injectable } from '@angular/core';
import { ApiBaseClient } from '../../auth/api-base.client';
import { HttpClient, HttpHeaders, HttpRequest, HttpEventType } from '@angular/common/http';
import { ApiHelper } from '../../auth/api.helper';
import { CurrentUserService } from '../../auth/current-user.service';
import { ServiceNames } from '../../const/app-local-storage.const';
import { ResponcePriceFileUpload } from './dto';


@Injectable()
export class SxApiPriceFileClient extends ApiBaseClient {

    // refreshTockenSubscriver: Subscription;

    constructor(
        private client: HttpClient,
        private apiHelper: ApiHelper,
        private cus: CurrentUserService) {
        super();
    }

    private get sertviceUrl(): string {
        return this.apiHelper.getServiceUrl(ServiceNames.pricefile);
    }

    //#region Price File

    priceFileUpload(comId: number, file: any): Promise<ResponcePriceFileUpload> {

        const promise = new Promise<ResponcePriceFileUpload>((resolve, reject) => {
            // let headers: HttpHeaders = new HttpHeaders({ 'Authorization': + this.client.bearerToken });

            const formdata: FormData = new FormData();
            // formdata.append('file', file, fileName);
            formdata.append('price_file', file);
            const fileMetadata = { company_id: comId };

            const blobFileMetadata = new Blob([JSON.stringify(fileMetadata)], {
                type: 'application/json',
            });

            formdata.append('fileMetadata', blobFileMetadata);
            const url = this.sertviceUrl + '/price_file_upload';

            const req = new HttpRequest('POST', url, formdata, {
                /*headers: headers,*/
                reportProgress: true,
                responseType: 'json'
            });

            this.client.request(req)
                .subscribe(resp => {
                    // console.log('priceFileUpload-> resp: ', resp);
                    if (resp.type === HttpEventType.Response) {
                        if (resp.ok) {
                            const res = Object.assign(new ResponcePriceFileUpload(), resp.body);
                            resolve(res);
                        } else {
                            reject();
                        }
                    }
                }, err => {
                    // console.log('newPriceFile err', err);
                    let msg = 'Error occured uploading file. Please try again later.';
                    if (err) {

                        if (err.error instanceof ErrorEvent) {
                            msg = 'Error occured uploading file. ' + err.error.message;
                        } else {
                            msg = 'Error occured uploading file. ' + err.statusText;
                        }
                    }
                    reject(msg);
                });
        });
        return promise;
    }

    //#endregion


}

import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpParams, HttpResponse, HttpHeaders, HttpHeaderResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { catchError, map, tap } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
// Services
import { ApiBaseClient } from '../../auth/api-base.client';
import { ApiHelper } from '../../auth/api.helper';
import { CurrentUserService } from '../../auth/current-user.service';
// --- Models
import { ServiceNames } from '../../const/app-local-storage.const';
import { AppRoutes } from '../../const/app-routes.const';
import { LocalUserSettings } from '../../models/app/settings';
import { ResponseSxProject, ResponseSxProjectSave, ResponseSxProjectTable, ResponseSxUseCaseTable, ResponseSxUseCasetSave, SxProjectModel, SxUseCaseModel } from './dto';

// Models

// import { JwtHelperService } from '@auth0/angular-jwt';


@Injectable()
export class SxApiProjectClient extends ApiBaseClient {

    constructor(
        private client: HttpClient,
        private apiHelper: ApiHelper,
        private cus: CurrentUserService) {
        super();
    }

    private get sertviceUrl(): string {
        return this.apiHelper.getServiceUrl(ServiceNames.project);
    }

    //#region Project

    projectById(id: number): Observable<any> {
        // const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' });
        const url = this.sertviceUrl + '/project';
        const req = { project_id: id };
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                observe: 'response',
            }),
        };

        return this.client.post<HttpResponse<any>>(url, req, httpOptions)
            .pipe(
                map((response: HttpResponse<any>) => {
                    console.log('projectById-> response=', response);
                    let rv: SxProjectModel;
                    if (response.body) {
                        const res = Object.assign(new ResponseSxProject(), response.body);
                        console.log('projectById-> res=', res);
                        if (res && res.data && res.data.project) {
                            rv = SxProjectModel.fromJSON(res.data.project);
                        }

                    }
                    return rv;
                }),
                catchError(this.handleError)
            );
    }

    projectSave(value: SxProjectModel): Observable<SxProjectModel> {


        // value.actor_id = this.cus.user.user_id;
        console.log('projectSave-> value=', value);
        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.client.post(this.sertviceUrl + 'project_save',
            { project: value },
            { headers: hdrs }).pipe(
                map((response: ResponseSxProjectSave) => {
                    console.log('companySave-> response=', response);
                    const res = Object.assign(new ResponseSxProjectSave(), response);
                    // console.log('companySave-> success=', res.success);
                    let rv: SxProjectModel;
                    if (res && res.success) {
                        // console.log('companySave-> res=', res);
                        if (res && res.data && res.data.project) {
                            rv = SxProjectModel.fromJSON(res.data.project[0]);
                        }
                    }
                    return rv;
                }),
                tap(event => {
                    this.log(`tap Project save event: ` + JSON.stringify(event));
                    // There may be other events besides the response.
                    if (event instanceof HttpResponse) {
                        // cache.put(req, event); // Update the cache.
                    }
                }),
                catchError(this.handleError)
            );

    }


    projectTable(
        limit: number = 25,
        offset: number = 0,
        orderCol: string = 'project_name',
        isDesc: boolean = false,
        companyId?: number,
        filter?: string): Observable<ResponseSxProjectTable> {

        console.log('projectTable-> offset=', offset);
        console.log('projectTable-> limit=', limit);

        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.client.post(this.sertviceUrl + 'project_table',
            {
                company_id: companyId,
                qry_offset: offset,
                qry_limit: limit,
                qry_orderCol: orderCol,
                qry_isDesc: isDesc,
                qry_filter: filter,
            },
            { headers: hdrs }).pipe(
                map((resp: ResponseSxProjectTable) => {
                    const res = Object.assign(new ResponseSxProjectTable(), resp);
                    console.log('projectTable -> response=', res);
                    return res;
                }),
                tap(event => {
                    this.log(`tap projectTable event: ` + JSON.stringify(event));
                    // There may be other events besides the response.
                    if (event instanceof HttpResponse) {
                        // cache.put(req, event); // Update the cache.
                    }
                }),
                catchError(this.handleError)
            );
    }


    //#endregion

    //#region UseCase

    usecaseSave(value: SxUseCaseModel): Observable<SxUseCaseModel> {


        // value.actor_id = this.cus.user.user_id;
        console.log('usecaseSave-> value=', value);
        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.client.post(this.sertviceUrl + 'usecase_save',
            { usecase: value },
            { headers: hdrs }).pipe(
                map((response: ResponseSxUseCasetSave) => {
                    console.log('usecaseSave-> response=', response);
                    const res = Object.assign(new ResponseSxUseCasetSave(), response);
                    // console.log('companySave-> success=', res.success);
                    let rv: SxUseCaseModel;
                    if (res && res.success) {
                        // console.log('companySave-> res=', res);
                        if (res && res.data && res.data.usecase) {
                            rv = SxUseCaseModel.fromJSON(res.data.usecase[0]);
                        }
                    }
                    return rv;
                }),
                tap(event => {
                    this.log(`tap Project save event: ` + JSON.stringify(event));
                    // There may be other events besides the response.
                    if (event instanceof HttpResponse) {
                        // cache.put(req, event); // Update the cache.
                    }
                }),
                catchError(this.handleError)
            );

    }


    usecaseTable(
        limit: number = 25,
        offset: number = 0,
        orderCol: string = 'usecase_order',
        isDesc: boolean = false,
        projectId?: number,
        filter?: string): Observable<ResponseSxUseCaseTable> {

        console.log('usecaseTable-> offset=', offset);
        console.log('usecaseTable-> limit=', limit);

        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.client.post(this.sertviceUrl + 'usecase_table',
            {
                project_id: projectId,
                qry_offset: offset,
                qry_limit: limit,
                qry_orderCol: orderCol,
                qry_isDesc: isDesc,
                qry_filter: filter,
            },
            { headers: hdrs }).pipe(
                map((resp: ResponseSxUseCaseTable) => {
                    const res = Object.assign(new ResponseSxUseCaseTable(), resp);
                    console.log('usecaseTable -> response=', res);
                    return res;
                }),
                tap(event => {
                    this.log(`tap usecaseTable event: ` + JSON.stringify(event));
                    // There may be other events besides the response.
                    if (event instanceof HttpResponse) {
                        // cache.put(req, event); // Update the cache.
                    }
                }),
                catchError(this.handleError)
            );
    }

    //#endregion


}

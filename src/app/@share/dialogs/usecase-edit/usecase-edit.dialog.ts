import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
// Services
import { UserModel } from 'src/app/@core/auth/api/dto';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
import { SxApiCompanyClient } from 'src/app/@core/api/company/api-client';
import { Observable, of } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { SxProjectModel, SxUseCaseModel } from 'src/app/@core/api/project/dto';
import { SxApiProjectClient } from 'src/app/@core/api/project/api-client';
import { SxProjectStatus, SxProjectStatusOpt } from 'src/app/@core/api/project/enums';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
// Constants

export interface ISxUseCaseEdit {
    project: SxProjectModel;
    usecase: SxUseCaseModel;
}

@Component({
    templateUrl: './usecase-edit.dialog.html',
})

// tslint:disable-next-line:component-class-suffix
export class SxUseCaseEditDialog implements OnInit {

    formGrp: FormGroup;

    project: SxProjectModel;
    usecase: SxUseCaseModel;
    userList: Array<UserModel>;
    estimatorOpt: Observable<UserModel[]>;
    managerOpt: Observable<UserModel[]>;
    statusOpt = SxProjectStatusOpt;

    hasSpinner = false;
    errorMessage: string;
    title: string;

    constructor(
        private fb: FormBuilder,
        private router: Router,
        private toastr: ToastrService,
        private spinerService: SpinnerService,
        private cus: CurrentUserService,
        private cClient: SxApiCompanyClient,
        private pClient: SxApiProjectClient,
        public dialogRef: MatDialogRef<ISxUseCaseEdit>,
        @Inject(MAT_DIALOG_DATA) public data: ISxUseCaseEdit) {
        this.project = data.project;
        this.usecase = data.usecase;
    }

    get usecase_name() { return this.formGrp.get('usecase_name'); }
    get usecase_order() { return this.formGrp.get('usecase_order'); }
    get pstatus_id() { return this.formGrp.get('pstatus_id'); }
    get usecase_notes() { return this.formGrp.get('usecase_notes'); }
    get usecase_img_url() { return this.formGrp.get('usecase_img_url'); }


    fError(fname: string): string {
        const field = this.formGrp.get(fname);
        return field.hasError('required') ? 'Field ' + fname + '  is required.' :
            field.hasError('minlength') ? 'Field ' + fname + ' to short.' :
                field.hasError('maxlength') ? 'Field ' + fname + ' to long.' : '';
    }

    ngOnInit(): void {

        this.errorMessage = undefined;
        this.hasSpinner = false;
        this.title =  this.usecase && this.usecase.usecase_id > 0 ? 'Update' : 'Create';

        this.formGrp = this.fb.group({
            usecase_name: new FormControl(this.usecase ? this.usecase.usecase_name : '', [Validators.required, Validators.maxLength(512)]),
            usecase_order: new FormControl(this.usecase ? this.usecase.usecase_order : 1, []),
            pstatus_id: new FormControl(this.usecase ? this.usecase.pstatus_id : SxProjectStatus.Backlog, [Validators.required]),
            usecase_notes: new FormControl(this.usecase ? this.usecase.usecase_notes : '', [Validators.maxLength(2000)]),
            usecase_img_url: new FormControl(this.usecase ? this.usecase.usecase_img_url : '', [Validators.maxLength(1024)]),
        });

        // this.loadUserList();
    }


    onCansel(): void {
        this.dialogRef.close();
    }

    onSubmit() {
        this.formGrp.updateValueAndValidity();
        if (this.formGrp.valid) {
            this.errorMessage = undefined;
            this.hasSpinner = true;

            if (!this.usecase) {
                this.usecase = new SxUseCaseModel();
            }
            const message = this.usecase.project_id > 0 ? 'Update' : 'Create';
            const userId = this.cus.user.user_id;

            this.usecase.usecase_name = this.usecase_name.value;
            this.usecase.project_id = this.project.project_id;
            this.usecase.usecase_order = this.usecase_order.value;
            this.usecase.pstatus_id = this.pstatus_id.value;
            this.usecase.usecase_notes = this.usecase_notes.value;
            this.usecase.usecase_img_url = this.usecase_img_url.value;
            this.usecase.user_id = this.cus.user.user_id;

            // this.spinerService.display(true);

            this.pClient.usecaseSave(this.usecase)
                .subscribe((res: SxUseCaseModel) => {
                    // this.spinerService.display(false);
                    console.log('usecaseSave -> res:', res);
                    if (res && res.usecase_id) {
                        this.usecase = res;
                        this.errorMessage = undefined;
                        this.hasSpinner = false;
                        this.toastr.success('Usecase', 'Operation Succesfull: Usecase ' + message);
                        this.dialogRef.close(this.usecase);
                    }
                },
                    err => {
                        // this.spinerService.display(false);
                        console.error('Observer got an error: ' + err);
                        this.errorMessage = 'Usecase ' + message + ' Failed. ' + err;
                        setTimeout((router: Router,) => {
                            this.errorMessage = undefined;
                            this.hasSpinner = false;
                            this.dialogRef.close(this.usecase);
                        }, 2000);
                        // this.spinerService.display(false);
                        // this.toastr.error('Compnay', 'Operation Failed: Compnay ' + message);
                    },
                    () => console.log('Observer got a complete notification'));

        }


    }



}

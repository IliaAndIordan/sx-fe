import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import * as XLSX from 'xlsx';
// -Services
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
// --Models
import { CompanyModel } from 'src/app/@core/api/company/dto';
import { PreloaderService } from 'src/app/@core/services/preloader.service';
import { SxApiPriceFileClient } from 'src/app/@core/api/price_file/api-client';
import { ResponcePriceFileUpload } from 'src/app/@core/api/price_file/dto';
import { MfrItemImportService } from 'src/app/@core/services/mfr-item-import-service';
import { CompanyType } from 'src/app/@core/api/company/enums';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { FileImportModel } from 'src/app/@core/api/mfr/item/dto';


export interface IItemImportFileSelect {
    company: CompanyModel;
}

@Component({
    templateUrl: './item-import-file-select.dialog.html',
})

// tslint:disable-next-line:component-class-suffix
export class ItemImportFileSelectDialog implements OnInit {

    fgSelectFile: FormGroup;
    itemsData: Array<any>;
    company: CompanyModel;
    file: any;
    selFileName: string;
    selFileSize: number;
    isUploading: boolean;

    hasSpinner = false;
    errorMessage: string;

    constructor(
        private fb: FormBuilder,
        private router: Router,
        private toastr: ToastrService,
        private preloader: PreloaderService,
        private cus: CurrentUserService,
        private priceFileclient: SxApiPriceFileClient,
        private mfrIService: MfrItemImportService,
        public dialogRef: MatDialogRef<IItemImportFileSelect>,
        @Inject(MAT_DIALOG_DATA) public data: IItemImportFileSelect) {
        this.company = data.company;
    }


    ngOnInit(): void {
        this.isUploading = false;
        this.fgSelectFile = this.fb.group({
            priceFileIn: ['', Validators.required],
        });

        this.fgSelectFile.updateValueAndValidity();
    }

    get priceFileIn() { return this.fgSelectFile.get('priceFileIn'); }

    fError(fname: string): string {
        const field = this.fgSelectFile.get(fname);
        return field.hasError('required') ? 'Field ' + fname + '  is required.' :
            field.hasError('minlength') ? 'Field ' + fname + ' to short.' :
                field.hasError('maxlength') ? 'Field ' + fname + ' to long.' : '';
    }

    priceFileChange(event: any): void {
        // console.log('priceFileChange-> event=', event);
        const file = event.target.files[0]; // <--- File Object for future use.
        if (file) {
            this.file = file;
            // console.log('priceFileChange-> file=', this.file);
            const fnameExt: string = file.name.replace('C:\\fakepath\\', '');
            // console.log('priceFileChange-> fnameExt=', fnameExt);
            this.checkFile(this.file)
                .then(res => {
                    this.selFileName = fnameExt;
                    this.selFileSize = this.file.size;
                    const fs = new FileImportModel();
                    fs.file_name = this.selFileName;
                    fs.file_size = this.selFileSize;
                    
                    this.mfrIService.fileInfo = fs;
                    // console.log('this.selFileName: ', this.selFileName);
                    this.priceFileIn.setValue(this.file);
                    this.fgSelectFile.updateValueAndValidity();
                    if (this.fgSelectFile.valid) {
                    }

                }, msg => {
                    this.toastr.error(msg);
                    console.log('fileUploadChange: msg:', msg);
                })
                .catch(err => {
                    this.toastr.error('ERROR importing file: ', file.name);
                    console.log('fileUploadChange: err:', err);
                });

        }
    }

    checkFile(file: any): Promise<void> {
        const promise = new Promise<void>((resolve, reject) => {
            if (file) {
                this.hasSpinner = true;
                const reader: FileReader = new FileReader();
                reader.readAsBinaryString(file);

                reader.onload = (e: any) => {
                    /* read workbook */
                    const bstr: string = e.target.result;
                    const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });
                    /* grab first sheet */
                    const wsname: string = wb.SheetNames[0];
                    const ws: XLSX.WorkSheet = wb.Sheets[wsname];
                    /* save data from first sheet*/
                    this.itemsData = new Array<any>();
                    const data: Array<any> = XLSX.utils.sheet_to_json(ws, { header: 1, raw: true });

                    if (data && data.length > 0) {
                        data.forEach(element => {
                            if (element && element.length > 0) {
                                this.itemsData.push(element);
                            }
                        });
                    }
                    const fileInfo = this.mfrIService.fileInfo;
                    fileInfo.file_rows = this.itemsData && this.itemsData.length > 0 ? this.itemsData.length : 0;
                    this.mfrIService.fileInfo = fileInfo;
                    console.log('checkFile-> itemsData:', this.itemsData);
                    this.hasSpinner = false;
                    resolve();
                };
            }
        });

        return promise;
    }

    get isImportReady() {
        return this.itemsData && this.itemsData.length > 0 ? true : false;
    }

    onSubmit() {

        if (this.fgSelectFile.valid) {

            this.errorMessage = undefined;
            /*
            if(this.company && this.company.company_type_id === CompanyType.Manufacturer){
                this.mfrIService.selCompany = this.company;
                this.mfrIService.rawItemData = this.itemsData;
                this.router.navigate([AppRoutes.Root, AppRoutes.Manufacturer, AppRoutes.ImportItems]);
            }*/
            this.dialogRef.close(this.itemsData);
            /*
            this.preloader.show();
            this.isUploading = true;
            const company_id = this.company.company_id;

            console.log('onSubmit -> company_id:', company_id);

            this.priceFileclient.priceFileUpload(company_id, this.file)
                .then((resp: ResponcePriceFileUpload) => {
                    // console.log('priceFileUpload -> resp:', resp);
                    this.isUploading = false;
                    this.preloader.hide();
                    if (resp && resp.success) {
                        this.toastr.success('Price file has been received and scheduled for processing.', 'Price File Uploads');
                        this.dialogRef.close(resp);
                    }

                }, msg => {
                    this.preloader.hide();
                    this.errorMessage = msg;
                    this.toastr.error(msg);
                    this.isUploading = false;
                })
                .catch(ex => {
                    this.preloader.hide();
                    this.errorMessage = ex.errorMessage;
                    this.toastr.error(ex);
                    this.isUploading = false;
                });
                */
        }
    }

}

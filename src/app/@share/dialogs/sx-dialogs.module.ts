
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SxMaterialModule } from '../material.module';
import { SxCommonPipesModule } from 'src/app/@core/common-pipes.module';
import { SxProjectEditDialog } from './project-edit/project-edit.dialog';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgProgressModule } from 'ngx-progressbar';
import { SxUseCaseEditDialog } from './usecase-edit/usecase-edit.dialog';
import { ItemImportFileSelectDialog } from './item-file-select/item-import-file-select.dialog';
import { SxComponentsModule } from '../components/sx-components.module';
import { MfrItemImportService } from 'src/app/@core/services/mfr-item-import-service';
// ---


@NgModule({
    imports: [
        CommonModule,
        FlexLayoutModule,
        NgProgressModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule,
        RouterModule,
        FlexLayoutModule,
        SxMaterialModule,
        SxCommonPipesModule,

        // ---
        SxComponentsModule,
    ],
    declarations: [
        SxProjectEditDialog,
        SxUseCaseEditDialog,
        ItemImportFileSelectDialog,
    ],
    exports: [
        SxProjectEditDialog,
        SxUseCaseEditDialog,
        ItemImportFileSelectDialog,
    ],
    entryComponents: [
        SxProjectEditDialog,
        SxUseCaseEditDialog,
        ItemImportFileSelectDialog,
    ],
    providers:[
        MfrItemImportService,
    ]
})
export class SxSharedDialogModule { }

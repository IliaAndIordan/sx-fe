import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'sx-simple-card',
  templateUrl: './simple-card.component.html'
})
export class SimpleCardComponent implements OnInit {



  /**
   * BINDINGS
   */
  @Input() border: string;
  @Input() title: string;
  @Input() content: string;
  @Input() dragData: any;
  @Input() close: boolean;
  @Input() menuItems: any[];
  @Output() closeSCard: EventEmitter<any> = new EventEmitter<any>();
  @Output() clickSCard: EventEmitter<any> = new EventEmitter<any>();
  @Output() menuSCardClick: EventEmitter<any> = new EventEmitter<any>();


  /**
   * FIELDS
   */

  constructor() { }

  ngOnInit() {
  }

  closeCard() {
    this.closeSCard.emit();
  }

  cardClick() {
    this.clickSCard.emit();
  }

  menuClick(selection: string) {
    this.menuSCardClick.emit(selection);
  }

}


import { Component, OnInit, Input, ViewEncapsulation, HostBinding } from '@angular/core';
import { CountryModel } from 'src/app/@core/api/country/dto';

@Component({
    selector: 'sx-country-line',
    templateUrl: './country-line.component.html',
    encapsulation: ViewEncapsulation.None,
})
export class CountryLineComponent implements OnInit {
    @Input() country: CountryModel;

    constructor() { }

    ngOnInit() { }
}

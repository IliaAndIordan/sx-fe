import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChange, ViewEncapsulation } from '@angular/core';
// ---Services
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
// ---Models
import { UserModel } from 'src/app/@core/auth/api/dto';
import { URL_NO_IMG_SQ } from 'src/app/@core/const/app-local-storage.const';
import { UserRole, UserRoleOpt } from 'src/app/@core/auth/api/enums';


const Wide = 'listcard-image-wide';
const Tall = 'listcard-image-tall';
@Component({
    selector: 'sx-user-card',
    templateUrl: './user-card.component.html',
    encapsulation: ViewEncapsulation.None,
})
export class UserCardComponent implements OnInit, OnChanges {

    /** Bindings */
    @Input() user: UserModel;
    @Output() editUser: EventEmitter<UserModel> = new EventEmitter<UserModel>();
    @Output() sendEmail: EventEmitter<UserModel> = new EventEmitter<UserModel>();

    imgUrl = URL_NO_IMG_SQ;
    imgClass: string;
    roleOpt = UserRole;

    isAdmin: boolean;

    constructor(private cus: CurrentUserService) { }

    ngOnInit() {
        this.isAdmin = this.cus.isAdmin || this.cus.isCompanyAdmin;
        if (this.user) {
            this.imgUrl = this.cus.getAvatarUrl(this.user.e_mail);
        }
        this.imgClass = this.getImageClass();
    }

    ngOnChanges(changes: { [propertyName: string]: SimpleChange }) {
        if (changes['user']) {
            this.user = changes['user'].currentValue;
            this.ngOnInit();
        }

    }

    onImageLoad(): void {
        this.imgClass = this.getImageClass();
    }


    getImageClass() {
        const image = new Image();
        image.src = this.imgUrl;

        if (image.width > image.height + 10 || image.width === image.height) {
            return Tall;
        } else {
            return Wide;
        }
    }

    sendEmailClick() {
        this.sendEmail.emit(this.user);
    }

    editUserClick() {
        this.editUser.emit(this.user);
    }

}

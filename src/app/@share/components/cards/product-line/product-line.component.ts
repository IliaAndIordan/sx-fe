import { Component, OnInit, Input, ViewEncapsulation, HostBinding, Output, EventEmitter } from '@angular/core';
import { CountryModel } from 'src/app/@core/api/country/dto';
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
import { COMMON_IMG_LOGO_RED, URL_NO_IMG, URL_NO_IMG_SQ } from 'src/app/@core/const/app-local-storage.const';
import { SxProduct, SxProductViewModel } from 'src/app/@core/models/pipes/product.pipe';
const Wide = 'listcard-image-wide';
const Tall = 'listcard-image-tall';
@Component({
    selector: 'sx-product-line',
    templateUrl: './product-line.component.html',
    encapsulation: ViewEncapsulation.None,
})
export class SxProductLineComponent implements OnInit {
    @Input() product: SxProductViewModel;
    @Input() isGranted: boolean;
    @Output() grandProduct: EventEmitter<SxProductViewModel> = new EventEmitter<SxProductViewModel>();
    @Output() revokeProduct: EventEmitter<SxProductViewModel> = new EventEmitter<SxProductViewModel>();

    imgUrl = URL_NO_IMG_SQ;
    imgClass: string;
    enableProductAcitavion: boolean;


    constructor(private cus: CurrentUserService) { }

    ngOnInit() {
        this.enableProductAcitavion = this.cus.isAdmin;
        if (this.product) {
            switch (this.product.id) {
                case SxProduct.SupplierExchange:
                    this.imgUrl = COMMON_IMG_LOGO_RED;
                    break;
                default:
                    this.imgUrl = URL_NO_IMG;
                    break;
            }
        }
        this.imgClass = this.getImageClass();
    }

    getImageClass() {
        const image = new Image();
        image.src = this.imgUrl;

        if (image.width > image.height + 10 || image.width === image.height) {
            //return wide image class 
            return Tall;
        } else {
            return Wide;
        }
    }

    activeChecked(e: boolean): void {

        if (e) {
            this.product.isGranted = true;
            this.grandProduct.emit(this.product);
        } else {
            this.product.isGranted = false;
            this.revokeProduct.emit(this.product);
        }
    }
}

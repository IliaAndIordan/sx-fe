import { Component, OnInit, Input, ViewEncapsulation, HostBinding, Output, EventEmitter, OnChanges, SimpleChange } from '@angular/core';
import { CompanyModel } from 'src/app/@core/api/company/dto';
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
import { URL_NO_IMG_SQ } from 'src/app/@core/const/app-local-storage.const';
const Wide = 'listcard-image-wide';
const Tall = 'listcard-image-tall';
@Component({
    selector: 'sx-company-line',
    templateUrl: './company-line.component.html',
    encapsulation: ViewEncapsulation.None,
})
export class SxCompanyLineComponent implements OnInit, OnChanges {
    @Input() company: CompanyModel;

    imgUrl = URL_NO_IMG_SQ;
    imgClass: string;

    constructor(private cus: CurrentUserService) { }

    ngOnInit() {
        if (this.company) {
            this.imgUrl = this.company.sxLogoUrl;
        }
        this.imgClass = this.getImageClass();
    }

    ngOnChanges(changes: { [propertyName: string]: SimpleChange }) {
        if (changes['company']) {
            this.company = changes['company'].currentValue;
            this.ngOnInit();
        }

    }

    onImageLoad(): void {
        this.imgClass = this.getImageClass();
    }


    getImageClass() {
        const image = new Image();
        image.src = this.imgUrl;

        if (image.width > image.height + 10 || image.width === image.height) {
            return Tall;
        } else {
            return Wide;
        }
    }

}

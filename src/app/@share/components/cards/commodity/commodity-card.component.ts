import {
  Component, OnInit, ViewChild, OnChanges, SimpleChange,
  ViewContainerRef, ChangeDetectorRef, OnDestroy, Output, Input, EventEmitter
} from '@angular/core';
import { Route, Router, ActivatedRouteSnapshot, RouterStateSnapshot, ActivatedRoute, Params } from '@angular/router';
import { Observable } from 'rxjs';
import { FormBuilder, FormGroup, FormControl, Validators, AbstractControl, ValidatorFn, FormArray } from '@angular/forms';
// Services
import { ToastrService } from 'ngx-toastr';
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
// ---animate view
import { CommodityModel } from 'src/app/@core/api/mfr/commodity/dto';
import { PageTransition, ShowHideCardTrigger, ShowHideListTrigger, ShowHideTriggerBlock, ShowHideTriggerFlex } from 'src/app/@core/const/animations-triggers';
// Const
const Wide = 'tree-card-image-wide';
const Tall = 'tree-card-image-tall';
const Preload = 'tree-card-image-preload';
const Default = 'tree-card-image-default';

@Component({
  selector: 'sx-cmdty-card',
  templateUrl: './commodity-card.component.html',
  animations: [
    PageTransition,
    ShowHideTriggerFlex,
    ShowHideListTrigger,
    ShowHideCardTrigger,
    ShowHideTriggerBlock]
})
export class CommodityCardComponent implements OnInit {

  constructor(
    private activatedRoute: ActivatedRoute,
    public toastr: ToastrService,
    vcr: ViewContainerRef,
    private changeDetectorRef: ChangeDetectorRef,
    private fb: FormBuilder,
    private cus: CurrentUserService) {

  }

  /**
   * BINDINGS
   */
  @Input() cndty: CommodityModel;
 
  @Output() onTreeCardClick: EventEmitter<any> = new EventEmitter<any>();
  @Output() onCommodityAdClick: EventEmitter<any> = new EventEmitter<any>();

  /**
   * FIELDS
   */
  // Some cards will not have images, this is just a defualt when this happens
  defaultImageUrl = '/assets/images/common/tree-card-default.png';
  imageUrl: string;
  title: string;

  imageClass: string = Preload;

  ngOnInit() {
    this.initFields();
  }

  initFields() {
    this.imageUrl = this.cndty && this.cndty.image_url ? this.cndty.image_url : this.defaultImageUrl;
  }
  onImageLoad() {
    this.imageClass = this.getImageClass();
  }

  onImageError() {
    this.imageUrl = this.defaultImageUrl; // Don't show broken image.
  }

  getImageClass() {
    let image = new Image();
    image.src = this.imageUrl;
    if (image.width > image.height + 20 || image.width === image.height) {
      return Wide;
    } else {
      return Tall;
    }
  }

  public treeCardClick(treeData) {
    this.onTreeCardClick.emit(treeData);
  }


  // NOTE:  TSL-1135 Temporary fix: remove commodity-ads from root-level commodities. below this not in use.

  commodityAdClick(commodity: CommodityModel) {

    this.onCommodityAdClick.emit([commodity.commodity_id]);

  }

}

import { Component, OnInit, ViewEncapsulation, Input, HostBinding, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { MenuService } from 'src/app/@core/services/menu.service';

@Component({
  selector: 'sx-page-header',
  /*host: { class: 'matero-page-header', },*/
  templateUrl: './page-header.component.html',
  styleUrls: ['./page-header.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class PageHeaderComponent implements OnInit {

  /** Binding */
  @Input() title = '';
  @Input() subtitle = '';
  @Input() showBreadCrumb = true;
  @Input() panelIn: boolean;
  @Output() expandFilterPanel: EventEmitter<any> = new EventEmitter<any>();

  constructor(private router: Router, private menuService: MenuService) {
    const states = this.router.url.slice(1).split('/');
    this.title = this.menuService.getMenuItemName(states);
  }

  ngOnInit() { }

  public expandPanelArrowClick(): void {
    this.expandFilterPanel.emit();
  }
}

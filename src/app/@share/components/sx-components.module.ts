
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SxMaterialModule } from '../material.module';
import { BreadcrumbComponent } from './breadcrumb/breadcrumb.component';
import { PageHeaderComponent } from './page-header/page-header.component';
import { SpinnerComponent } from './spinner/spinner.component';
import { MiniProgressComponent } from './mini-progress/mini-progress.component';
import { CountryLineComponent } from './cards/country-line/country-line.component';
import { ButtonRound } from './bittons/round/button.component';
import { FileUploadComponent } from './file-upload/file-upload.component';
import { SxCommonPipesModule } from 'src/app/@core/common-pipes.module';
import { FilterPanelComponent } from './filter-panel/filter-panel.component';
import { FormatControl } from './bittons/format-control/format-control.component';
import { SxProductLineComponent } from './cards/product-line/product-line.component';
import { SxCheckboxLabeled } from './input/checkbox-labeled/checkbox-labeled.component';
import { SxCompanyLineComponent } from './cards/company/lines/company-line.component';
import { BackButtonComponent } from './bittons/back/back-button.component';
import { ButtonInfo } from './bittons/info/button.component';
import { SxButtonFlat } from './bittons/flat/button.component';
import { UserCardComponent } from './cards/users/user-card.component';
import { CommodityCardComponent } from './cards/commodity/commodity-card.component';
import { SimpleCardComponent } from './cards/simple-card/simple-card.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { ButtonPrimary } from './bittons/primary/button.component';
import { SxIconsModule } from './icons/sx-icons.module';
// ---


@NgModule({
    imports: [
        CommonModule,
        DragDropModule,
        SxCommonPipesModule,
        RouterModule,
        FlexLayoutModule,
        SxMaterialModule,
        // ---Compnents Module
        SxIconsModule,
    ],
    declarations: [
        BreadcrumbComponent,
        // ---BUTTONS
        ButtonPrimary,
        ButtonRound,
        BackButtonComponent,
        ButtonInfo,
        SxButtonFlat,
        // ---Cards
        SimpleCardComponent,
        UserCardComponent,
        CommodityCardComponent,
        // --- PAGE
        PageHeaderComponent,
        SpinnerComponent,
        MiniProgressComponent,
        CountryLineComponent,
        FormatControl,
        FileUploadComponent,
        FilterPanelComponent,
        SxProductLineComponent,
        SxCompanyLineComponent,
        SxCheckboxLabeled,
        ],
    exports: [
        SxIconsModule,
        BreadcrumbComponent,
        // ---BUTTONS
        ButtonPrimary,
        ButtonRound,
        BackButtonComponent,
        ButtonInfo,
        SxButtonFlat,
        // ---Cards,
        SimpleCardComponent,
        UserCardComponent,
        CommodityCardComponent,
        // --- PAGE
        PageHeaderComponent,
        FormatControl,
        SpinnerComponent,
        MiniProgressComponent,
        CountryLineComponent,
        
        FileUploadComponent,
        FilterPanelComponent,
        SxProductLineComponent,
        SxCompanyLineComponent,
        SxCheckboxLabeled,
    ],
})
export class SxComponentsModule { }


import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SxCommonPipesModule } from 'src/app/@core/common-pipes.module';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { CompanyTypeDescriptorIcon } from './descriptor/company-type/company-type-descriptor-icon.component';
import { SxMaterialModule } from '../../material.module';
// ---


@NgModule({
    imports: [
        CommonModule,
        DragDropModule,
        SxCommonPipesModule,
        RouterModule,
        FlexLayoutModule,
        SxMaterialModule,
    ],
    declarations: [
        CompanyTypeDescriptorIcon,
        ],
    exports: [
        CompanyTypeDescriptorIcon,
    ],
})
export class SxIconsModule { }

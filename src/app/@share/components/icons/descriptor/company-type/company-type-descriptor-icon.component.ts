import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { CompanyType } from 'src/app/@core/api/company/enums';
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';

@Component({
  selector: 'sx-icon-company-type',
  templateUrl: 'company-type-descriptor-icon.component.html'
})

export class CompanyTypeDescriptorIcon implements OnInit {

  /**
   * BINDINGS
   */
  @Input() companyTypeId: CompanyType;

  // Assets
  //private companyTypeMfrIcon = require('/app/assets/icons/company-type-mfr.png');
  //private companyTypeDistributorIcon = require('/app/assets/icons/company-type-distributor.png');
  //private companyTypeContractorIcon = require('/app/assets/icons/company-type-contractor.png');
  //private companyTypeIcon = require('/app/assets/icons/company-type-any.png');

  /**
   * FIELDS
   */
  public iconSrc: string;
  public showIcon = true;


  constructor(private cus: CurrentUserService) { }

  ngOnInit() {
    this.getIconSrc();
  }

  getIconSrc() {
    switch (this.companyTypeId.toString()) {
      case CompanyType.Manufacturer.toString():
        this.iconSrc = '/assets/images/icons/company-type-mfr.png'; // this.companyTypeMfrIcon;
        break;
      case CompanyType.Contractor.toString():
        this.iconSrc = '/assets/images/icons/company-type-distributor.png';
        break;
      case CompanyType.Distributor.toString():
        this.iconSrc = '/assets/images/icons/company-type-contractor.png';
        break;

      default:
        // this.showIcon = false;
        this.iconSrc = '/assets/images/icons/company-type-any.png';
        break;
    }

  }


}

import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { Animate } from 'src/app/@core/const/animation.const';
import { TogglePanelTrigger } from './animation';

@Component({
  selector: 'sx-filter-panel',
  templateUrl: './filter-panel.component.html',
  animations: [TogglePanelTrigger]
})

export class FilterPanelComponent implements OnInit {

  /** Binding */
  @Output() closePanel: EventEmitter<any> = new EventEmitter<any>();
  @Input() panelVar: string;
  /** FIELDS */
  

  constructor() { }
  

  ngOnInit() {
  }
 
  public openPanelClick(): void {
    // this.panelVar = Animate.out;
  }

  closePanelClick(): void {
    // this.panelVar = Animate.in;
    this.closePanel.emit();
  }

}

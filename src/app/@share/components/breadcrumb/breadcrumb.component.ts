import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { SELECT_ITEM_HEIGHT_EM } from '@angular/material/select';
import { Router } from '@angular/router';
import { SxCommonPipesModule } from 'src/app/@core/common-pipes.module';
import { Breadcrumb } from 'src/app/@core/models/app/menu.model';
import { MenuService } from 'src/app/@core/services/menu.service';

@Component({
  selector: 'sx-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class BreadcrumbComponent implements OnInit {
  routeNames: Array<Breadcrumb> = new Array<Breadcrumb>();

  constructor(
    private router: Router,
    private menuService: MenuService) {
  }

  ngOnInit() {
    this.genBreadcrumb();
  }

  genBreadcrumb() {
    const states = this.router.url.slice(1).split('/');
    // console.log('genBreadcrumb -> states: ', states);

    this.routeNames = this.menuService.getMenuLevel(states);
    const bcHome = new Breadcrumb('home', ['/']);
    // this.routeNames.unshift('home');
    this.routeNames.unshift(bcHome);
  }

  gotoMenu(item: Breadcrumb) {
    if (item && item.path && item.path.length > 0) {
      this.router.navigate(item.path);
    }
  }
}

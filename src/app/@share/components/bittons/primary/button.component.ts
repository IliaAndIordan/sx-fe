import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'sx-button-primary',
  templateUrl: './button.component.html'
})
export class ButtonPrimary {

  constructor( ) { }

  /**
   * BINDINGS
   */
  @Input() buttonType: string;
  @Input() color: string;
  @Input() disabled:boolean;

  /**
   * FIELDS
   */

};
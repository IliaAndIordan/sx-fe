import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';

@Component({
  selector: 'sx-format-control',
  templateUrl: './format-control.component.html'
})
export class FormatControl implements OnInit {

  constructor() { }

  /**
   * BINDINGS
   */
  @Input() icon: string;
  @Input() label: string;
  @Input() color: string;
  @Input() large: boolean;
  @Input() disabled: boolean;
  @Input() tooltip: string = '';
  @Input() tooltipPlacement: string = '';

  /**
   * FIELDS
   */
  labeled: boolean;

  ngOnInit() {

    if (this.label) {
      this.labeled = true;
    } else {
      this.labeled = false;
    }

  }

};
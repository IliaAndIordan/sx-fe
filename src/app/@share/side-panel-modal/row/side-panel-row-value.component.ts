import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'sx-side-panel-row-value',
  templateUrl: 'side-panel-row-value.component.html',
})
export class SidePanelRowValueComponent implements OnInit {

  constructor() { }

  /**
   * BINDINGS
   */
  @Input() label: string;
  @Input() infoMessage: string;
  @Input() value: string;
  @Input() isPrice = false;
  @Input() isDate = false;
  @Input() size = 'small'; // accepts 'small', 'med' or large'
  @Input() valueCap = 30;

  /**
   * FIELDS
   */
  get valueD(): number {
    let rv: number;
    if (this.isPrice) {
      rv = parseFloat(this.value);
    }
    return rv;
  }

  ngOnInit() {
    // console.log(this.size);

    if (this.size === 'large') {
      this.valueCap = 70;
    }
  }

  isNumber(val) { return typeof val === 'number'; }


}// end class

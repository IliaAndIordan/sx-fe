import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { GravatarService } from '@infinitycube/gravatar';
import { CompanyModel } from 'src/app/@core/api/company/dto';
import { SxApiCountryClient } from 'src/app/@core/api/country/api-client';
import { CountryModel } from 'src/app/@core/api/country/dto';
import { SxProjectModel } from 'src/app/@core/api/project/dto';
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
import { URL_NO_IMG, URL_NO_IMG_SQ } from 'src/app/@core/const/app-local-storage.const';

@Component({
    selector: 'sx-spm-company-info',
    templateUrl: './spm-company-info.component.html',
})
export class SxSpmCompanyInfoComponent implements OnInit, OnChanges {

    /**
     * BINDINGS
     */
    @Input() company: CompanyModel;

    /**
     * FIELDS
     */
    logoUrl: string;
    sxLiveryUrl: string;
    sxLogoUrl: string;
    country: CountryModel;
    companyType: number;

    constructor(
        private gravatarService: GravatarService,
        private cus: CurrentUserService,
        private countryClient: SxApiCountryClient) {
    }

    ngOnInit() {
        this.initFields();
    }


    ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
        if (changes['company']) {
            this.company = changes['company'].currentValue;
            this.initFields();
        }
    }

    initFields() {
        if (this.company) {
            const tq = '?m=' + new Date().getTime();
            this.companyType = this.company ? this.company.company_type_id : undefined;

            this.logoUrl = this.company && this.company.logo_url ? this.company.logo_url + tq :
                this.company && this.company.sxLogoUrl ? this.company.sxLogoUrl : URL_NO_IMG;

            this.sxLiveryUrl = this.company.sxLiveryUrl ? this.company.sxLiveryUrl + tq : URL_NO_IMG;

            this.sxLogoUrl = this.company.sxLogoUrl ? this.company.sxLogoUrl + tq : URL_NO_IMG;
            this.getCountry();
        }
    }

    getCountry() {
        this.countryClient.countryList
            .subscribe((list: Array<CountryModel>) => {
                if (list && list.length > 0) {
                    this.country = list.find(x => x.country_id === this.company.country_id);
                }
            });
    }

}

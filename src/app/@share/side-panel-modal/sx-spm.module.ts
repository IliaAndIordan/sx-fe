
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SxCommonPipesModule } from 'src/app/@core/common-pipes.module';
import { SxMaterialModule } from '../material.module';
import { SxComponentsModule } from '../components/sx-components.module';
// ---
import { SxSpmProjectInfoComponent } from './project/spm-project-info.component';
import { SidePanelRowValueComponent } from './row/side-panel-row-value.component';
import { SxSpmCompanyInfoComponent } from './company/spm-company-info.component';


@NgModule({
    imports: [
        CommonModule,
        SxCommonPipesModule,
        RouterModule,
        FlexLayoutModule,
        SxMaterialModule,
        SxComponentsModule,
    ],
    declarations: [
        SidePanelRowValueComponent,
        SxSpmProjectInfoComponent,
        SxSpmCompanyInfoComponent,
        ],
    exports: [
        SidePanelRowValueComponent,
        SxSpmProjectInfoComponent,
        SxSpmCompanyInfoComponent,
    ],
})
export class SxSidePanellModalModule { }

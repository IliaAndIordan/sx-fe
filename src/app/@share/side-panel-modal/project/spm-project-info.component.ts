import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { GravatarService } from '@infinitycube/gravatar';
import { SxProjectModel } from 'src/app/@core/api/project/dto';
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
import { URL_NO_IMG_SQ } from 'src/app/@core/const/app-local-storage.const';

@Component({
    selector: 'sx-spm-project-info',
    templateUrl: './spm-project-info.component.html',
})
export class SxSpmProjectInfoComponent implements OnInit, OnChanges {

    /**
     * BINDINGS
     */
    @Input() project: SxProjectModel;

    /**
     * FIELDS
     */
    managerAvUrl = URL_NO_IMG_SQ;
    estimatorAvUrl = URL_NO_IMG_SQ;

    constructor(
        private gravatarService: GravatarService,
        private cus: CurrentUserService) {
    }

    ngOnInit() {
        this.initFields();
    }


    ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
        if (changes['project']) {
            this.project = changes['project'].currentValue;
            this.initFields();
        }
    }

    initFields() {
        if (this.project) {
            this.managerAvUrl = this.project.manager_email ? 
                this.gravatarService.url(this.project.manager_email, 128, 'identicon') : URL_NO_IMG_SQ;
            this.estimatorAvUrl = this.project.estimator_email ? 
                this.gravatarService.url(this.project.estimator_email, 128, 'identicon') : URL_NO_IMG_SQ;
        }
    }

}

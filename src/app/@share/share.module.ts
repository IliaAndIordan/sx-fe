import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SxMaterialModule } from './material.module';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgProgressModule } from 'ngx-progressbar';
import { LayoutAdminModule } from './layout/admin/layout-admin.module';
import { BrowserModule } from '@angular/platform-browser';
import { SxComponentsModule } from './components/sx-components.module';
import { SxCommonPipesModule } from '../@core/common-pipes.module';
import { SxSharedDialogModule } from './dialogs/sx-dialogs.module';
import { SxSidePanellModalModule } from './side-panel-modal/sx-spm.module';
import { DragDropModule } from '@angular/cdk/drag-drop';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    FlexLayoutModule,
    NgProgressModule,
    FormsModule,
    ReactiveFormsModule,
    DragDropModule,
    SxMaterialModule,
    // ---Pipes
    SxCommonPipesModule,
    // ---Layout
    SxComponentsModule,
    LayoutAdminModule,
    SxSidePanellModalModule,
    SxSharedDialogModule,
  ],
  exports: [
    FlexLayoutModule,
    NgProgressModule,
    FormsModule,
    ReactiveFormsModule,
    SxMaterialModule,
    SxCommonPipesModule,
    // ---Layout
    SxComponentsModule,
    LayoutAdminModule,
    SxSidePanellModalModule,
    SxSharedDialogModule,
  ]
})
export class ShareModule { }

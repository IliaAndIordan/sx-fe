
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FlexLayoutModule } from '@angular/flex-layout';
// ---
import { BrandingComponent } from './header/branding.component';
import { GithubButtonComponent } from './header/github.component';
import { NotificationComponent } from './header/notification.component';
import { SxMaterialModule } from '../../material.module';
import { UserComponent } from './header/user.component';
import { HeaderComponent } from './header/header.component';
import { UserPanelComponent } from './sidebar/user-panel.component';
import { SidemenuComponent } from './sidemenu/sidemenu.component';
import { AccordionDirective } from './sidemenu/accordion.directive';
import { AccordionLinkDirective } from './sidemenu/accordionlink.directive';
import { SidebarComponent } from './sidebar/sidebar.component';
import { SidebarNoticeComponent } from './sidebar-notice/sidebar-notice.component';
import { TopmenuComponent } from './topmenu/topmenu.component';
import { SelSxProjectButtonComponent } from './header/sel-project.component';
import { SxSidePanelModalComponent } from './side-panel-modal/side-panel-modal';
import { SxCommonPipesModule } from 'src/app/@core/common-pipes.module';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        FlexLayoutModule,
        SxMaterialModule,
        SxCommonPipesModule,
    ],
    declarations: [
        BrandingComponent,
        GithubButtonComponent,
        SelSxProjectButtonComponent,
        NotificationComponent,
        UserPanelComponent,
        UserComponent,
        HeaderComponent,
        SidemenuComponent,
        TopmenuComponent,
        SidebarComponent,
        SidebarNoticeComponent,
        SxSidePanelModalComponent,
        // ---
        AccordionDirective,
        AccordionLinkDirective,
    ],
    exports: [
        BrandingComponent,
        GithubButtonComponent,
        SelSxProjectButtonComponent,
        NotificationComponent,
        UserComponent,
        HeaderComponent,
        UserPanelComponent,
        SidemenuComponent,
        TopmenuComponent,
        SidebarComponent,
        SidebarNoticeComponent,
        SxSidePanelModalComponent,
        // ---
        AccordionDirective,
        AccordionLinkDirective,
    ],
})
export class LayoutAdminModule { }

import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { MatDialog } from '@angular/material/dialog';
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
import { URL_NO_IMG } from 'src/app/@core/const/app-local-storage.const';
import { UserModel } from 'src/app/@core/auth/api/dto';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';

@Component({
  selector: 'sx-user-panel',
  templateUrl: './user-panel.component.html',
})
export class UserPanelComponent implements OnInit {

  noImg = URL_NO_IMG;

  avatarUrl: string;
  user: UserModel;

  get isLogged(){
    return this.cus.isLogged;
  }

  constructor(
    private router: Router,
    public dialogService: MatDialog,
    private cus: CurrentUserService) {

  }


  ngOnInit(): void {
    this.user = this.cus.user;
    this.avatarUrl = this.cus.avatarUrl;
  }

  //#region Actions

  logout() {
    console.log('logout ->')
    this.cus.user = undefined;
    this.router.navigate([AppRoutes.Root, AppRoutes.guest]);
  }

  //#endregion

}

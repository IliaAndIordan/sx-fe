import { Component, Input } from '@angular/core';
import { MenuService } from 'src/app/@core/services/menu.service';
import { MenuItem } from 'src/app/@core/models/app/menu.model';

@Component({
  selector: 'sx-sidemenu',
  templateUrl: './sidemenu.component.html',
})
export class SidemenuComponent {
  // -NOTE: Ripple effect make page flashing on mobile
  @Input() ripple = true;

  menus = this.menuService.getAll();

  constructor(private menuService: MenuService) { }

  // Delete empty value in array
  filterStates(states: string[]) {
    return states.filter(item => item && item.trim());
  }

  enable(mi: MenuItem): boolean {
    const rv = this.menuService.enable(mi);
    // console.log('enable-> ' + rv + ' mi:', mi);
    return rv;
  }
}

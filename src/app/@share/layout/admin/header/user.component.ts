import { Component, OnInit } from '@angular/core';
import { COMMON_IMG_AVATAR, URL_NO_IMG } from 'src/app/@core/const/app-local-storage.const';
import { UserModel } from 'src/app/@core/auth/api/dto';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';

@Component({
  selector: 'sx-user',
  templateUrl: './user.component.html',
})
export class UserComponent implements OnInit {

  imageAvatarUrl = COMMON_IMG_AVATAR;
  noImg = URL_NO_IMG;

  avatarUrl: string;
  user: UserModel;
  username: string;

  get isLogged() {
    return this.cus.isLogged;
  }

  constructor(
    private router: Router,
    public dialogService: MatDialog,
    private cus: CurrentUserService) {

  }


  ngOnInit(): void {
    this.user = this.cus.user;
    this.avatarUrl = this.cus.avatarUrl;
    this.username = this.user ? (this.user.user_name ? this.user.user_name : this.user.e_mail) : 'Guest';
  }


  //#region Actions

  logout() {
    // console.log('logout ->');
    this.cus.user = undefined;
    this.router.navigate([AppRoutes.Root, AppRoutes.guest]);
  }

  //#endregion
}

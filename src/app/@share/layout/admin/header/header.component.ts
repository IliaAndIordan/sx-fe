import {
  Component, OnInit, Output, EventEmitter,
  Input, ChangeDetectionStrategy,
} from '@angular/core';
import { COMMON_IMG_LOGO_RED } from 'src/app/@core/const/app-local-storage.const';
// import * as screenfull from 'screenfull';

@Component({
  selector: 'sx-header',
  templateUrl: './header.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderComponent implements OnInit {

  /*
  * Bindings
  */
  @Input() showToggle = true;
  @Input() showBranding = true;

  @Output() toggleSidenav = new EventEmitter<void>();
  @Output() toggleSidenavNotice = new EventEmitter<void>();

  /*
    private get screenfull(): screenfull.Screenfull {
      return screenfull as screenfull.Screenfull;
    }

    // TODO:
    toggleFullscreen() {
      if (this.screenfull.enabled) {
        this.screenfull.toggle();
      }
    }

  */
  constructor() { }

  ngOnInit() { }


}

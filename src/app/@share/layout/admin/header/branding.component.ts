import { Component } from '@angular/core';
import { COMMON_IMG_LOGO_RED, URL_NO_IMG } from 'src/app/@core/const/app-local-storage.const';

@Component({
  selector: 'sx-branding',
  template: `
    <a class="matero-branding" href="#/">
      <img [src]="sxLogo" class="matero-branding-logo-expanded" alt="" />
      <span class="matero-branding-name"></span>
    </a>
  `,
})
export class BrandingComponent {

  
  /*
  * Fields
  */
 sxLogo = COMMON_IMG_LOGO_RED;

}

import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
    templateUrl: './welcome-component.html',
})

export class WelcomeComponent implements OnInit, OnDestroy {

    constructor(private router: Router,
                private route: ActivatedRoute,
                private toastr: ToastrService, ) { }

    ngOnInit(): void {
    }

    ngOnDestroy(): void {
    }

}

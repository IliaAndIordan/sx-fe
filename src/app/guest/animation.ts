import { trigger, state, style, transition, animate } from '@angular/animations';
import { Animate } from '../@core/const/animation.const';



export const ExpandTab = 
trigger('expandTab',[
      state(Animate.in , style({
        width: '72px'
      })),
      state(Animate.out, style({
        width: '500px'
      })),
      transition(Animate.in + '=> ' + Animate.out, animate('500ms')),
      transition(Animate.out + ' =>' +  Animate.in , animate('500ms'))
    ])

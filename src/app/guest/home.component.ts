import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
// --- Services
// --- Animations
import { Animate } from '../@core/const/animation.const';
import { ExpandTab } from './animation';
// --- Models
import { AppRoutes } from '../@core/const/app-routes.const';
import { COMMON_IMG_LOGO_RED } from '../@core/const/app-local-storage.const';
import { PageTransition, ShowHideTriggerBlock, ShowHideTriggerFlex, SpinExpandIconTrigger } from 'src/app/@core/const/animations-triggers';
import { MatDialog } from '@angular/material/dialog';
import { LoginModal } from './dialogs/login/login.dialog';

@Component({
  templateUrl: './home.component.html',
  animations: [PageTransition,
    ShowHideTriggerBlock,
    ShowHideTriggerFlex,
    SpinExpandIconTrigger,
    ExpandTab]
})
export class HomeComponent implements OnInit, OnDestroy {

  /**
   * Fields
   */
  sxLogo = COMMON_IMG_LOGO_RED;
  // --- Side Tab
  expandTabVar: string = Animate.in;
  showTabContentsVar: string = Animate.hide;
  total: number = 0;
  aggressiveTotal: number = 0;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    public dialogService: MatDialog) { }


  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    // console.log('HomeComponent:ngOnDestroy()->');
  }

  exapandTabClick() {
    this.expandTabVar = this.expandTabVar === Animate.in ? Animate.out : Animate.in;
    this.showTabContentsVar = this.showTabContentsVar === Animate.show ? Animate.hide : Animate.show;
  }

  public loginDialogShow(): void {
    const dialogRef = this.dialogService.open(LoginModal, {
      width: '500px',
      height: '320px',
      data: { email: null, password: null }
    });

    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed', result);
      // this.password = result.password;
    });
  }

}

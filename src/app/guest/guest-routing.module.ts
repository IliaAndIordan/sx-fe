import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
// Component
import { GuestComponent } from './guest.component';
import { HomeComponent } from './home.component';
import { AppRoutes } from '../@core/const/app-routes.const';
import { WelcomeComponent } from './welcome/welcome-component';

const routes: Routes = [
  {
    path: AppRoutes.Root,
    component: GuestComponent,
    children: [
      { path: AppRoutes.Root, component: HomeComponent,
        children: [
          { path: AppRoutes.Root, pathMatch: 'full', component: WelcomeComponent },
          // { path: AppRoutes.Login, component: LoginComponent },
          // { path: AppRoutes.Dashboard, component: DashboardComponent },
        ]
      },
      // { path: 'copper-pricing', component: CopperPricingComponent }
    ]
  }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class GuestRoutingModule { }

export const routedComponents = [GuestComponent, HomeComponent, WelcomeComponent];

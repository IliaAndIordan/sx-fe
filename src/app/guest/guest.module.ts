import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShareModule } from '../@share/share.module';
import { GuestRoutingModule, routedComponents } from './guest-routing.module';
import { LoginModal } from './dialogs/login/login.dialog';
import { SxMaterialModule } from '../@share/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({

    imports: [
        CommonModule,
        ShareModule,
        GuestRoutingModule,
    ],
    declarations: [
        routedComponents,
        // --- Dialogs
        LoginModal,
    ],
    entryComponents: [
        LoginModal
    ]
})

export class GuestModule { }
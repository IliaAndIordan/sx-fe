import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
// -Services
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
// --Models
import { CompanyModel } from 'src/app/@core/api/company/dto';
import { PreloaderService } from 'src/app/@core/services/preloader.service';
import { SxApiPriceFileClient } from 'src/app/@core/api/price_file/api-client';
import { ResponcePriceFileUpload } from 'src/app/@core/api/price_file/dto';


export interface IPriceFileUpload {
    company: CompanyModel;
}

@Component({
    templateUrl: './price-file-upload.dialog.html',
})

// tslint:disable-next-line:component-class-suffix
export class PriceFileUploadDialog implements OnInit {

    fgSelectFile: FormGroup;

    company: CompanyModel;
    file: any;
    selFileName: string;
    selFileSize: number;
    isUploading: boolean;

    hasSpinner = false;
    errorMessage: string;

    constructor(
        private fb: FormBuilder,
        private router: Router,
        private toastr: ToastrService,
        private preloader: PreloaderService,
        private cus: CurrentUserService,
        private priceFileclient: SxApiPriceFileClient,
        public dialogRef: MatDialogRef<IPriceFileUpload>,
        @Inject(MAT_DIALOG_DATA) public data: IPriceFileUpload) {
        this.company = data.company;
    }


    ngOnInit(): void {
        this.isUploading = false;
        this.fgSelectFile = this.fb.group({
            priceFileIn: ['', Validators.required],
        });

        this.fgSelectFile.updateValueAndValidity();
    }

    get priceFileIn() { return this.fgSelectFile.get('priceFileIn'); }

    fError(fname: string): string {
        const field = this.fgSelectFile.get(fname);
        return field.hasError('required') ? 'Field ' + fname + '  is required.' :
            field.hasError('minlength') ? 'Field ' + fname + ' to short.' :
                field.hasError('maxlength') ? 'Field ' + fname + ' to long.' : '';
    }

    priceFileChange(event: any): void {
        // console.log('priceFileChange-> event=', event);
        let file = event.target.files[0]; // <--- File Object for future use. 
        if (file) {
            this.file = file;
            // console.log('priceFileChange-> file=', this.file);
            const fnameExt: string = file.name.replace('C:\\fakepath\\', '');
            // console.log('priceFileChange-> fnameExt=', fnameExt);
            this.checkFile(this.file)
                .then(res => {
                    this.selFileName = fnameExt;
                    this.selFileSize = this.file.size;
                    // console.log('this.selFileName: ', this.selFileName);
                    this.priceFileIn.setValue(this.file);
                    this.fgSelectFile.updateValueAndValidity();
                    if (this.fgSelectFile.valid) {
                    }

                }, msg => {
                    this.toastr.error(msg);
                    console.log('fileUploadChange: msg:', msg);
                })
                .catch(err => {
                    this.toastr.error("ERROR importing file: ", file.name);
                    console.log('fileUploadChange: err:', err);
                });

        }
    }

    checkFile(file: any): Promise<string> {
        let promise = new Promise<string>((resolve, reject) => {
            if (file) {
                const reader: FileReader = new FileReader();
                reader.readAsBinaryString(file);

                reader.onload = (e: any) => {
                    /* read workbook */
                    const bstr: string = e.target.result;

                    console.log('checkFile: bstr:', bstr);
                    resolve(bstr);

                };
            }
        });

        return promise;
    }

    onSubmit() {

        if (this.fgSelectFile.valid) {

            this.errorMessage = undefined;
            this.preloader.show();
            this.isUploading = true;
            const company_id = this.company.company_id;

            console.log('onSubmit -> company_id:', company_id);

            this.priceFileclient.priceFileUpload(company_id, this.file)
                .then((resp: ResponcePriceFileUpload) => {
                    // console.log('priceFileUpload -> resp:', resp);
                    this.isUploading = false;
                    this.preloader.hide();
                    if (resp && resp.success) {
                        this.toastr.success('Price file has been received and scheduled for processing.', 'Price File Uploads');
                        this.dialogRef.close(resp);
                    }

                }, msg => {
                    this.preloader.hide();
                    this.errorMessage = msg;
                    this.toastr.error(msg);
                    this.isUploading = false;
                })
                .catch(ex => {
                    this.preloader.hide();
                    this.errorMessage = ex.errorMessage;
                    this.toastr.error(ex);
                    this.isUploading = false;
                });
        }
    }

}

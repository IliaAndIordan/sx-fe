import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
// Services
import { UserModel } from 'src/app/@core/auth/api/dto';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
import { CompanyModel } from 'src/app/@core/api/company/dto';
import { CompanyService } from '../../company.service';
import { SxApiCompanyClient } from 'src/app/@core/api/company/api-client';
import { CountryModel } from 'src/app/@core/api/country/dto';
import { Observable, of } from 'rxjs';
import { SxApiCountryClient } from 'src/app/@core/api/country/api-client';
import { startWith, map } from 'rxjs/operators';
import { SxUserClient } from 'src/app/@core/api/user/api-client';
import { UserRoleOpt, UserRole } from 'src/app/@core/auth/api/enums';
// Constants

export interface IUserEdit {
    user: UserModel;
    company_id?: number;
}

@Component({
    templateUrl: './user-edit.dialog.html',
})
// tslint:disable-next-line: component-class-suffix
export class UserEditDialog implements OnInit {

    formGrp: FormGroup;

    user: UserModel;
    company_id: number;

    roleOpt = UserRoleOpt;
    title = 'Edit user';
    hasSpinner = false;
    errorMessage: string;

    constructor(
        private fb: FormBuilder,
        private router: Router,
        private toastr: ToastrService,
        private cus: CurrentUserService,
        private cService: CompanyService,
        private countryApiCliente: SxApiCountryClient,
        private uApiClient: SxUserClient,
        public dialogRef: MatDialogRef<IUserEdit>,
        @Inject(MAT_DIALOG_DATA) public data: IUserEdit) {
        this.user = data.user;
        this.company_id = data.company_id;
    }

    get user_name() { return this.formGrp.get('user_name'); }
    get e_mail() { return this.formGrp.get('e_mail'); }
    get user_role() { return this.formGrp.get('user_role'); }
    get is_receive_emails() { return this.formGrp.get('is_receive_emails'); }
    get u_password() { return this.formGrp.get('u_password'); }

    fError(fname: string): string {
        const field = this.formGrp.get(fname);
        return field.hasError('required') ? 'Field ' + fname + '  is required.' :
            field.hasError('minlength') ? 'Field ' + fname + ' to short.' :
                field.hasError('maxlength') ? 'Field ' + fname + ' to long.' : '';
    }

    ngOnInit(): void {

        this.errorMessage = undefined;
        this.hasSpinner = false;
        console.log('ngOnInit-> this.company_id', this.company_id);
        this.title = this.user && this.user.user_id ? 'Edit User' : 'Invite User';
        this.formGrp = this.fb.group({
            user_name: new FormControl(this.user ? this.user.user_name : '', [Validators.required, Validators.maxLength(256)]),
            e_mail: new FormControl(this.user ? this.user.e_mail : '', [Validators.required, Validators.maxLength(512)]),
            u_password: new FormControl(this.user ? this.user.u_password : '',
                [Validators.required, Validators.minLength(3), Validators.maxLength(50)]),
                
            user_role: new FormControl(this.user ? this.user.user_role :
                (this.company_id > -1 ? UserRole.User : UserRole.Guest), [Validators.required]),
            is_receive_emails: new FormControl(this.user ? this.user.is_receive_emails : '', []),
        });

        // this.loadCountryList();
    }


    onCansel(): void {
        this.dialogRef.close();
    }

    onSubmit() {
        this.formGrp.updateValueAndValidity();
        if (this.formGrp.valid) {
            this.errorMessage = undefined;
            this.hasSpinner = true;

            if (!this.user) {
                this.user = new UserModel();
                this.user.company_id = this.company_id;
            }
            const message = this.user.user_id > 0 ? 'Updated' : 'Created';

            this.user.user_name = this.user_name.value;
            this.user.e_mail = this.e_mail.value;
            this.user.u_password = this.u_password.value;
            this.user.user_role = this.user_role.value;
            this.user.is_receive_emails = this.is_receive_emails.value;

            this.uApiClient.userSave(this.user)
                .subscribe((userRes: UserModel) => {
                    console.log('onSubmit -> userRes:', userRes);
                    this.user = userRes;
                    this.errorMessage = undefined;
                    this.hasSpinner = false;
                    this.toastr.success( 'Succes: User ' + message, 'User');
                    this.dialogRef.close(this.user);
                },
                    err => {
                        console.error('Observer got an error: ' + err);
                        this.errorMessage = 'User ' + message + ' Failed. ' + err;
                        setTimeout((router: Router) => {
                            this.errorMessage = undefined;
                            this.hasSpinner = false;
                        }, 2000);
                        // this.spinerService.display(false);
                        // this.toastr.error('Compnay', 'Operation Failed: Compnay ' + message);
                    },
                    () => console.log('Observer got a complete notification'));

        }
    }

}

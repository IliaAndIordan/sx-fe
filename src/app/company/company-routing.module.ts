import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
// Component
import { AppRoutes } from '../@core/const/app-routes.const';
import { CompanyComponent } from './company.component';
import { HomeComponent } from './home.component';
import { CompanyCreateComponent } from './create/company-create.component';
import { AuthGuard } from '../@core/guards/auth.guard';
import { CompanyTypeComponent } from './create/company-type.component';
import { MyCompanyComponent } from './my-company/my-company.component';
import { CompanyDashboardComponent } from './my-company/dashboard/company-dashboard.component';
import { MyCompanyUsersComponent } from './my-company/users/company-users.component';
import { UserListComponent } from './user-list/user-list.component';
import { MyCompanyHomeComponent } from './dashboard/my-company-home.component';

const routes: Routes = [
  {
    path: AppRoutes.Root, component: CompanyComponent,
    children: [
      {
        path: AppRoutes.Root, component: HomeComponent,
        children: [
          { path: AppRoutes.Root, component: MyCompanyHomeComponent,
            children: [
              {  path: AppRoutes.Root,   pathMatch: 'full', component: CompanyDashboardComponent },
              { path: AppRoutes.Root,  component: MyCompanyHomeComponent },
              { path: AppRoutes.Dashboard, component: MyCompanyHomeComponent },
              { path: 'users', component: MyCompanyUsersComponent },
            ]},
         
          
          { path: 'type', component: CompanyTypeComponent },
          { path: 'create', component: CompanyCreateComponent },
          // { path: AppRoutes.Dashboard, component: DashboardComponent },
        ]
      },
      // { path: 'copper-pricing', component: CopperPricingComponent }
    ],
  },
  { path: 'companycreate', redirectTo: 'type', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CompanyRoutingModule { }

export const routedComponents = [
  CompanyComponent,
  HomeComponent,
  CompanyTypeComponent,
  CompanyCreateComponent,
  MyCompanyComponent,
  CompanyDashboardComponent,
  MyCompanyUsersComponent,
  // ---
  MyCompanyHomeComponent,
  UserListComponent,
];

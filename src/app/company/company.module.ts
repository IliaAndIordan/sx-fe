import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// --- Modules
import { ShareModule } from '../@share/share.module';
import { SxMaterialModule } from '../@share/material.module';
import { CompanyRoutingModule, routedComponents } from './company-routing.module';
import { CompanyCreateService } from './create/company-create.service';
import { CompanyService } from './company.service';
import { CompanyEditDialog } from './dialogs/company-edit/company-edit.dialog';
import { CompanyUsersDataSource } from './my-company/users/company-users.datasource';
import { UserEditDialog } from './dialogs/user-edit/user-edit.dialog';
import { PriceFileUploadDialog } from './dialogs/price-file-upload/price-file-upload.dialog';
import { MyCompanyUtilityBarComponent } from './utility-bar/utility-bar.component';
import { MyCompanyTreeComponent } from './dashboard/tree/my-company-tree.component';
import { MyCompanyTabUsersComponent } from './dashboard/tabs/my-company-home-tab-users.component';
import { MyCompanyTabsComponent } from './dashboard/tabs/my-company-home-tabs.component';
import { MyCompanyTabDashboardComponent } from './dashboard/tabs/my-company-home-tab-dashboard.component';


@NgModule({

    imports: [
        CommonModule,
        ShareModule,
        SxMaterialModule,
        CompanyRoutingModule,
    ],
    declarations: [
        MyCompanyUtilityBarComponent,
        routedComponents,
        // ---Components
        MyCompanyTreeComponent,
        MyCompanyTabsComponent,
        MyCompanyTabUsersComponent,
        MyCompanyTabDashboardComponent,
        // --- Dialogs
        CompanyEditDialog,
        UserEditDialog,
    ],
    entryComponents: [
        CompanyEditDialog,
        UserEditDialog,
    ],
    providers: [
        CompanyCreateService,
        CompanyService,
        CompanyUsersDataSource,

    ]
})

export class CompanyModule { }

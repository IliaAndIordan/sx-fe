import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { PreloaderService } from 'src/app/@core/services/preloader.service';
import { MatDialog } from '@angular/material/dialog';
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
import { CompanyModel } from 'src/app/@core/api/company/dto';
import { PriceFileUploadDialog } from '../dialogs/price-file-upload/price-file-upload.dialog';
import { ResponcePriceFileUpload } from 'src/app/@core/api/price_file/dto';
import { CompanyType } from 'src/app/@core/api/company/enums';
import { ItemImportFileSelectDialog } from 'src/app/@share/dialogs/item-file-select/item-import-file-select.dialog';
import { MfrItemImportService } from 'src/app/@core/services/mfr-item-import-service';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';

@Component({
    selector: 'sx-my-company-utility-bar',
    templateUrl: './utility-bar.component.html',
})
export class MyCompanyUtilityBarComponent implements OnInit, OnDestroy {
    /**
     * Binding
     */


    /**
     * Fields
     */
    company: CompanyModel;
    isDistributor: boolean;
    isManufacturer: boolean;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastrService,
        private preloader: PreloaderService,
        public dialogService: MatDialog,
        private mfrIService: MfrItemImportService,
        private cus: CurrentUserService) {

    }


    ngOnInit(): void {
        this.isManufacturer = false;
        this.isManufacturer = false;
        // console.log('user:', this.cus.user);
        if (!this.cus.user.company_id && !this.cus.isAdmin) {
            // this.router.navigate([AppRoutes.Company, 'create']);
        }
        this.initFields();
    }

    ngOnDestroy(): void {
        // console.log('HomeComponent:ngOnDestroy()->');

    }

    initFields() {
        this.company = this.cus.company;
        this.isDistributor = this.company && this.company.company_type_id === CompanyType.Distributor ? true : false;
        this.isManufacturer = this.company && this.company.company_type_id === CompanyType.Manufacturer ? true : false;
    }


    //#region Actions

    priceFileUpload(): void {
        const dialogRef = this.dialogService.open(PriceFileUploadDialog, {
            width: '721px',
            height: '320px', // 520px
            data: { company: this.company }
        });

        dialogRef.afterClosed().subscribe((res: ResponcePriceFileUpload) => {
            if (res && res.success) {
                // this.cus.company = res;
            }
            this.initFields();
        });
    }

    ItemImportFileSelect(): void {
        const dialogRef = this.dialogService.open(ItemImportFileSelectDialog, {
            width: '721px',
            height: '320px', // 520px
            data: { company: this.cus.company }
        });

        dialogRef.afterClosed().subscribe((res: any) => {
            console.log('afterClosed -> res:', res);
            if (res) {
                if (this.cus.company && this.cus.company.company_type_id === CompanyType.Manufacturer) {
                    this.mfrIService.selCompany = this.company;
                    this.mfrIService.rawItemData = res;
                    this.router.navigate([AppRoutes.Root, AppRoutes.Manufacturer, AppRoutes.ItemsImport]);
                }
            }
            else {
                this.initFields();
            }

        });
    }


    //#endregion

}

import { Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { CompanyModel } from 'src/app/@core/api/company/dto';
import { SxProjectModel, SxUseCaseModel } from 'src/app/@core/api/project/dto';
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
import { PreloaderService } from 'src/app/@core/services/preloader.service';
import { SxProjectService } from 'src/app/projects/project/project.service';
import { CompanyService } from '../../company.service';

@Component({
    selector: 'sx-my-company-tabs',
    templateUrl: './my-company-home-tabs.component.html',
})
export class MyCompanyTabsComponent implements OnInit, OnDestroy {
    /**
     * Binding
     */
    @Output() spmProjectOpen: EventEmitter<SxProjectModel> = new EventEmitter<SxProjectModel>();
    @Output() spmCompanyOpen: EventEmitter<CompanyModel> = new EventEmitter<CompanyModel>();
    /**
     * Fields
     */
    company: CompanyModel;
    companyChanged: Subscription;

    project: SxProjectModel;


    selTabIdx: number;
    selTabIdxChanged: Subscription;


    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastrService,
        private preloader: PreloaderService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private aService: CompanyService,
        private pService: SxProjectService) {

    }


    ngOnInit(): void {
        this.preloader.show();
        this.company = this.aService.company;

        this.companyChanged = this.cus.companyChanged.subscribe((com: CompanyModel) => {
            this.initFields();
        });

        this.selTabIdxChanged = this.aService.selTabIdxChanged.subscribe(tabIdx => {
            // console.log('packageCahnged -> pkg', pkg);
            this.initFields();
        });

        this.initFields();
    }

    ngOnDestroy(): void {
        if (this.companyChanged) { this.companyChanged.unsubscribe(); }
        if (this.selTabIdxChanged) { this.selTabIdxChanged.unsubscribe(); }

    }

    initFields() {
        this.company = this.aService.company;
        this.selTabIdx = this.aService.selTabIdx;
        


        this.preloader.hide();
    }

    //#region  Tab Panel Actions

    selectedTabChanged(tabIdx: number) {
        // console.log('selectedTabChanged -> tabIdx=', tabIdx);
        const oldTab = this.selTabIdx;
        this.selTabIdx = tabIdx;
        this.aService.selTabIdx = this.selTabIdx;
        // console.log('ngOnInit -> rootFolder ', this.rootFolder);
    }


    //#endregion

    //#region Action Methods


    spmProjectOpenClick() {
        this.project = this.pService.selProject;
        console.log('spmProjectOpen -> spmProjectObj=', this.project);
        if (this.project) {
            this.spmProjectOpen.emit(this.project);
        }

    }

    spmCompanyOpenClick() {
        console.log('spmProjectOpen -> spmProjectObj=', this.project);
        if (this.company) {
            this.spmCompanyOpen.emit(this.company);
        }

    }

    //#endregion

}

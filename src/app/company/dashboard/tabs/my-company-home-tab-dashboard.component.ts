import { Component, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Subscription } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
// ---Services
import { ToastrService } from 'ngx-toastr';
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
import { PreloaderService } from 'src/app/@core/services/preloader.service';
// ---Models
import { CompanyService } from '../../company.service';
import { CompanyModel } from 'src/app/@core/api/company/dto';


@Component({
    selector: 'sx-my-company-tab-dashboard',
    templateUrl: './my-company-home-tab-dashboard.component.html',
})
export class MyCompanyTabDashboardComponent implements OnInit, OnDestroy {
    /**
     * Binding
     */
    @Input() tabIdx: number;
    /**
     * Fields
     */
    selTabIdx: number;
    selTabIdxChanged: Subscription;

    company: CompanyModel;
    companyChanged: Subscription;


    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastrService,
        private preloader: PreloaderService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private aService: CompanyService) {

    }


    ngOnInit(): void {
        this.preloader.show();

        this.companyChanged = this.cus.companyChanged.subscribe((company: CompanyModel) => {
            this.initFields();
        });

        this.selTabIdxChanged = this.aService.selTabIdxChanged.subscribe(tabIdx => {
            // console.log('packageCahnged -> pkg', pkg);
            this.initFields();
        });

        this.initFields();
    }

    ngOnDestroy(): void {
        if (this.selTabIdxChanged) { this.selTabIdxChanged.unsubscribe(); }
        if (this.companyChanged) { this.companyChanged.unsubscribe(); }
    }

    initFields() {
        this.selTabIdx = this.aService.selTabIdx;
        this.company = this.aService.company;
        this.preloader.hide();
    }

}

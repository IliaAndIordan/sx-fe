import { Component, EventEmitter, Input, NgZone, OnDestroy, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { SxApiCompanyClient } from 'src/app/@core/api/company/api-client';
import { CompanyModel } from 'src/app/@core/api/company/dto';
import { SxProjectModel } from 'src/app/@core/api/project/dto';
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
import { URL_NO_IMG } from 'src/app/@core/const/app-local-storage.const';
import { PreloaderService } from 'src/app/@core/services/preloader.service';
import { CompanyService } from '../../company.service';
import { CompanyEditDialog } from '../../dialogs/company-edit/company-edit.dialog';

@Component({
    selector: 'sx-my-company-tree',
    templateUrl: './my-company-tree.component.html',
})

export class MyCompanyTreeComponent implements OnInit, OnDestroy {

    /** Binding */
    @Output() closePanel: EventEmitter<any> = new EventEmitter<any>();
    @Output() spmCompanyOpen: EventEmitter<CompanyModel> = new EventEmitter<CompanyModel>();
    @Input() panelVar: string;

    /** FIELDS */
    company: CompanyModel;
    companyChanged: Subscription;

    logoUrl: string;
    sxLiveryUrl: string;
    sxLogoUrl: string;


    constructor(
        private preloader: PreloaderService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private cClient: SxApiCompanyClient,
        private aService: CompanyService,
        private zone: NgZone) {
    }


    ngOnInit() {
        this.company = this.aService.company;

        this.companyChanged = this.cus.companyChanged.subscribe((company: CompanyModel) => {
            this.initFields();
        });
        this.initFields();
    }

    ngOnDestroy(): void {
        if (this.companyChanged) { this.companyChanged.unsubscribe(); }
    }


    initFields() {
        this.company = this.aService.company;
        if (this.company) {
            const tq = '?m=' + new Date().getTime();

            this.logoUrl = this.company && this.company.logo_url ? this.company.logo_url + tq : undefined;

            this.sxLiveryUrl = this.company.sxLiveryUrl ? this.company.sxLiveryUrl + tq : URL_NO_IMG;

            this.sxLogoUrl = this.company.sxLogoUrl ? this.company.sxLogoUrl + tq : URL_NO_IMG;
        }
    }

    public openPanelClick(): void {
        // this.panelVar = Animate.out;
    }

    closePanelClick(): void {
        // this.panelVar = Animate.in;
        this.closePanel.emit();
    }

    //#region Action Methods


    spmCompanyOpenClick(): void {
        if (this.company) {
            console.log('spmCompanyOpenClick -> company=', this.company);
            this.spmCompanyOpen.emit(this.company);
        }
    }

    refresh() {
        console.log('refreshProject->');
        this.cus.companyChanged.next(this.aService.company);
    }

    updateLivery() {
        this.preloader.show();
        this.aService.updateLivery(this.company)
            .subscribe((res: CompanyModel) => {
                this.preloader.hide();
                this.initFields();
            });
    }

    companyEdit(): void {
        const dialogRef = this.dialogService.open(CompanyEditDialog, {
            width: '721px',
            height: '520px',
            data: { company: this.company }
        });

        dialogRef.afterClosed().subscribe(res => {
            if (res) {
                this.updateLivery();
            }
            this.initFields();
        });
    }

    //#endregion

}

import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { MatDialog } from '@angular/material/dialog';
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
import { PreloaderService } from 'src/app/@core/services/preloader.service';
import { CompanyCreateService } from './company-create.service';
import { CompanyType } from 'src/app/@core/api/company/enums';
import { userInfo } from 'os';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';

@Component({
    templateUrl: './company-type.component.html',
})
export class CompanyTypeComponent implements OnInit, OnDestroy {
    /**
     * Binding
     */


    /**
     * Fields
     */

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastrService,
        private preloader: PreloaderService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private ccService: CompanyCreateService) {

    }


    ngOnInit(): void {
        this.preloader.show();
        this.initFields();
    }

    ngOnDestroy(): void {
        // console.log('HomeComponent:ngOnDestroy()->');
    }

    initFields() {

    }

    selectCompanyType(companyType: CompanyType) {
        this.ccService.companyType = companyType;
        if (!this.cus.user.company_id || this.cus.isAdmin) {
            this.router.navigate([AppRoutes.Company, 'create']);
        }
    }

    //#region Actions

    //#endregion

}

import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { MatDialog } from '@angular/material/dialog';
import { startWith, map } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { PreloaderService } from 'src/app/@core/services/preloader.service';
import { CompanyCreateService } from './company-create.service';
import { CompanyType, CompanyTypeDisplayPipe } from 'src/app/@core/api/company/enums';
import { CompanyModel } from 'src/app/@core/api/company/dto';
import { SxApiCountryClient } from 'src/app/@core/api/country/api-client';
import { CountryModel } from 'src/app/@core/api/country/dto';
import { SxApiCompanyClient } from 'src/app/@core/api/company/api-client';
import { timingSafeEqual } from 'crypto';
import { SxUserClient } from 'src/app/@core/api/user/api-client';
import { UserModel } from 'src/app/@core/auth/api/dto';
import { UserRole } from 'src/app/@core/auth/api/enums';

@Component({
    templateUrl: './company-create.component.html',
})
export class CompanyCreateComponent implements OnInit, OnDestroy {
    /**
     * Binding
     */


    /**
     * Fields
     */
    fdCompany: FormGroup;

    companyType: CompanyType;
    company: CompanyModel;

    selCountry: CountryModel;
    countryList: Array<CountryModel>;
    countryOpt: Observable<CountryModel[]>;


    hasSpinner = false;
    errorMessage: string;
    title = 'Create Company';

    get isManufacturer() {
        return this.companyType ? (this.companyType === CompanyType.Manufacturer) : false;
    }

    constructor(
        private fb: FormBuilder,
        private preloader: PreloaderService,
        private router: Router,
        private toastr: ToastrService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private ctdpipe: CompanyTypeDisplayPipe,
        private countryService: SxApiCountryClient,
        private ccService: CompanyCreateService,
        private userClient: SxUserClient,
        private companyClient: SxApiCompanyClient) {

    }

    ngOnInit(): void {
        this.preloader.show();
        this.initFields();
    }

    ngOnDestroy(): void {
        // console.log('HomeComponent:ngOnDestroy()->');
    }

    initFields() {
        this.companyType = this.ccService.companyType;
        this.company = this.ccService.company;
        this.title = this.companyType ?
            'Create ' + this.ctdpipe.transform(this.companyType, undefined) + ' Company' :
            'Create Company';
        this.fdCompany = this.fb.group({
            company_name: new FormControl(
                this.company && this.company.company_name ? this.company.company_name : '',
                [Validators.required, Validators.minLength(2), Validators.maxLength(511)]),
            branch_code: new FormControl(
                this.company && this.company.branch_code ? this.company.branch_code : '',
                [Validators.required, Validators.minLength(2), Validators.maxLength(255)]),
            country: new FormControl(
                this.company && this.company.country_id ? this.company.country_id : '',
                [Validators.required]),
            ean_mfr_code: new FormControl(
                this.company && this.company.ean_mfr_code ? this.company.ean_mfr_code : '',
                [Validators.maxLength(1000)]),
            logo_url: new FormControl(
                this.company && this.company.logo_url ? this.company.logo_url : '',
                [Validators.maxLength(1000)]),
            web_url: new FormControl(
                this.company && this.company.web_url ? this.company.web_url : '',
                [Validators.maxLength(1000)]),
        });
        this.loadCountryList();
    }

    fError(fname: string): string {
        const field = this.fdCompany.get(fname);
        return field.hasError('required') ? 'Field ' + fname + '  is required.' :
            field.hasError('minlength') ? 'Field ' + fname + ' to short.' :
                field.hasError('maxlength') ? 'Field ' + fname + ' to long.' : '';
    }

    get company_name() { return this.fdCompany.get('company_name'); }
    get branch_code() { return this.fdCompany.get('branch_code'); }
    get country() { return this.fdCompany.get('country'); }
    get ean_mfr_code() { return this.fdCompany.get('ean_mfr_code'); }
    get logo_url() { return this.fdCompany.get('logo_url'); }
    get web_url() { return this.fdCompany.get('web_url'); }



    //#region Actions
    onCansel(): void {
        // this.dialogRef.close();
    }

    onSubmit() {

        if (this.fdCompany.valid) {
            this.errorMessage = undefined;
            this.hasSpinner = true;
            if (!this.company) {
                this.company = new CompanyModel();
            }
            const country = this.country.value as CountryModel;
            this.company.company_type_id = this.companyType;
            this.company.company_name = this.company_name.value;
            this.company.branch_code = this.branch_code.value;
            this.company.country_id = country.country_id;
            this.company.logo_url = this.logo_url.value;
            this.company.web_url = this.web_url.value;

            this.company.actor_id = this.cus.user.user_id;
            const message = this.company.company_id > 0 ? 'Update' : 'Create';
            // console.log('updated book:', this.book);
            /*
            setTimeout((form: any = this) => {
              form.errorMessage = 'Airline Update Not Impllemented. ';
              form.hasSpinner = false;
            }, 300);
            */
            this.companyClient.companySave(this.company)
                .subscribe((res: CompanyModel) => {
                    console.log('companySave -> res:', res);
                    if (res && res.company_id) {
                        // TODO: Change user role to Company Admin
                        const user = this.cus.user;
                        user.company_id = res.company_id;
                        this.cus.company = res;
                        if(user.user_role !== UserRole.Admin){
                            user.user_role = UserRole.CompanyAdmin;
                        }
                        this.userClient.userSave(user)
                            .subscribe((userRes: UserModel) => {
                                console.log('onSubmit -> userRes:', userRes);
                                this.cus.user = userRes;
                                // TODO: Redirect to company home page
                                this.toastr.success('Compnay', 'Operation Succesfull: Compnay ' + message);
                                this.hasSpinner = false;
                            },
                            err => {
                                console.error('Observer got an error: ' + err);
                                this.errorMessage = 'Compnay ' + message + ' Failed. ' + err;
                                setTimeout((router: Router) => {
                                    this.errorMessage = undefined;
                                    this.hasSpinner = false;
                                }, 2000);
                                // this.spinerService.display(false);
                                // this.toastr.error('Compnay', 'Operation Failed: Compnay ' + message);
                            },
                            () => console.log('Observer got a complete notification'));

                    } else {
                        this.errorMessage = 'Compnay ' + message + ' Failed';
                        setTimeout((router: Router) => {
                            this.errorMessage = undefined;
                            this.hasSpinner = false;
                        }, 2000);
                        // this.spinerService.display(false);
                        this.toastr.error('Compnay', 'Operation Failed: Compnay ' + message);
                    }


                },
                    err => {
                        console.error('Observer got an error: ' + err);
                        this.errorMessage = 'Compnay ' + message + ' Failed. ' + err;
                        setTimeout((router: Router) => {
                            this.errorMessage = undefined;
                            this.hasSpinner = false;
                        }, 2000);
                        // this.spinerService.display(false);
                        this.toastr.error('Compnay', 'Operation Failed: Compnay ' + message);
                    },
                    () => console.log('Observer got a complete notification'));


        } else {
            this.errorMessage = 'Not valid input';
            this.toastr.error('Please enter valid values for fields', 'Not valid input');
        }
    }

    //#endregion

    //#region  Country Filter

    countryValueChange(event: CountryModel) {
        console.log('countryValueChange: event:', event);
        console.log('countryValueChange: msg:', this.country.value);
        if (this.country.valid) {
            const country: CountryModel = this.country.value;
        }
    }

    displayCountry(cm: CountryModel): string {
        let rv: string = '';
        console.log('displayCountry -> cm', cm);
        if (cm) {
            this.selCountry = cm;
            rv = '<img class="icon-18" [src]="' + cm.flagUrl + '" alt="flag">';
            rv = cm.country_name;
            rv += cm.country_iso2 ? ' - ' + cm.region_name : '';
        }
        return rv;
    }

    initContryOpt() {
        this.countryOpt = of(this.countryList);
        this.countryOpt = this.country.valueChanges
            .pipe(
                startWith(null),
                map(state => state ? this.filterContry(state) :
                    (this.countryList ? this.countryList.slice() : new Array<CountryModel>()))
            );
        this.fdCompany.updateValueAndValidity();
    }

    filterContry(val: any): CountryModel[] {
        // console.log('filterDpc -> val', val);
        if (val && val.country_id) {
            this.selCountry = val as CountryModel;
        }

        // console.log('filterBranches -> this.selDpc', this.selDpc);
        const value = val.country_name || val; // val can be companyName or string
        const filterValue = value ? value.toLowerCase() : '';
        // console.log('_filter -> filterValue', filterValue);
        let rv = new Array<CountryModel>()
        if (this.countryList && this.countryList.length) {
            rv = this.countryList.filter(x => (
                x.country_name.toLowerCase().indexOf(filterValue) > -1 ||
                x.country_iso2.toLowerCase().indexOf(filterValue) > -1 ||
                x.region_name.toLowerCase().indexOf(filterValue) > -1));
        }
        // console.log('_filter -> rv', rv);
        return rv;
    }


    loadCountryList() {
        this.countryService.countryList
            .subscribe((resp: Array<CountryModel>) => {
                // console.log('loadCountryList -> data:', resp);
                this.countryList = resp;
                this.initContryOpt();
            });
    }

    //#endregion

}

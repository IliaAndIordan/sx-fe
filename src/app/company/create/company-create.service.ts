import { Injectable } from '@angular/core';
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
import { CompanyType } from 'src/app/@core/api/company/enums';
import { CompanyModel } from 'src/app/@core/api/company/dto';
import { SxApiCompanyClient } from 'src/app/@core/api/company/api-client';

@Injectable({
    providedIn: 'root',
  })
  export class CompanyCreateService {

    /**
     *  Fields
     */
    companyType: CompanyType;
    company: CompanyModel;

    constructor(
        private cus: CurrentUserService,
        private companyClient: SxApiCompanyClient){
    }


  }
  
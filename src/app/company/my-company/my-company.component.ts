import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { PreloaderService } from 'src/app/@core/services/preloader.service';
import { MatDialog } from '@angular/material/dialog';
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
import { CompanyCreateService } from '../create/company-create.service';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { CompanyModel } from 'src/app/@core/api/company/dto';
import { CompanyType } from 'src/app/@core/api/company/enums';
import { COMMON_IMG_AVATAR } from 'src/app/@core/const/app-local-storage.const';
import { CountryModel } from 'src/app/@core/api/country/dto';
import { SxApiCountryClient } from 'src/app/@core/api/country/api-client';
import { CompanyEditDialog } from '../dialogs/company-edit/company-edit.dialog';
import { SxApiCompanyClient } from 'src/app/@core/api/company/api-client';
import { PriceFileUploadDialog } from '../dialogs/price-file-upload/price-file-upload.dialog';
import { ResponcePriceFileUpload } from 'src/app/@core/api/price_file/dto';
import { JsonPipe } from '@angular/common';
import { HomePageSettings } from 'src/app/@core/models/common/home-page-settings.model';
import { Animate } from 'src/app/@core/const/animation.const';
import { TogglePanelTrigger } from 'src/app/@share/components/filter-panel/animation';

export const STORE_KEY_PANEL_IN = 'panel-in-mycompany-key';
@Component({
    templateUrl: './my-company.component.html',
    animations: [TogglePanelTrigger]
})
export class MyCompanyComponent implements OnInit, OnDestroy {
    /**
     * Binding
     */


    /**
     * Fields
     */
    company: CompanyModel;
    title = 'Company';
    companyType: number;
    logoUrl: string;
    sxLiveryUrl: string;
    sxLogoUrl: string;
    country: CountryModel;

    //#region HPS
    panelVar: string = Animate.out;

    get hps(): HomePageSettings {
        let rv: HomePageSettings;
        const valStr = localStorage.getItem(STORE_KEY_PANEL_IN);
        if (valStr) {
            rv = Object.assign(new HomePageSettings(), JSON.parse(valStr));
        } else {
            rv = HomePageSettings.default();
            localStorage.setItem(STORE_KEY_PANEL_IN, JSON.stringify(rv));
        }
        return rv;
    }

    set hps(value: HomePageSettings) {
        if (!value) { value = HomePageSettings.default(); }
        localStorage.setItem(STORE_KEY_PANEL_IN, JSON.stringify(value));
    }

    get panelIn(): boolean {
        // console.log('panelIn-> hps:', this.hps);
        this.panelVar = this.hps.panelIn ? Animate.out : Animate.in;
        return this.hps.panelIn;
    }

    set panelIn(value: boolean) {
        const settings = this.hps;
        settings.panelIn = value;
        this.hps = settings;
        
    }

    //#endregion

    get isDistributor(): boolean {
        let rv = false;
        rv = this.company && this.company.company_type_id === CompanyType.Distributor ? true : false;
        return rv;
    }


    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastrService,
        private preloader: PreloaderService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private countryClient: SxApiCountryClient,
        private companyApiClient: SxApiCompanyClient,
        private ccService: CompanyCreateService) {

    }


    ngOnInit(): void {
        this.preloader.show();
        // console.log('user:', this.cus.user);
        if (!this.cus.user.company_id && !this.cus.isAdmin) {
            // this.router.navigate([AppRoutes.Company, 'create']);
        }
        this.initFields();
    }

    ngOnDestroy(): void {
        // console.log('HomeComponent:ngOnDestroy()->');

    }

    initFields() {
        this.company = this.cus.company;
        // this.panelVar = this.panelIn ? Animate.in : Animate.out;
        // console.log('initFields-> company:', this.company);
        const tq = '?m=' + new Date().getTime();
        this.companyType = this.company ? this.company.company_type_id : undefined;
        this.title = this.company ? this.company.company_name : 'Company';
        this.logoUrl = this.company && this.company.logo_url ? this.company.logo_url + tq :
            this.company && this.company.sxLogoUrl ? this.company.sxLogoUrl : COMMON_IMG_AVATAR;

        this.sxLiveryUrl = this.company.sxLiveryUrl ? this.company.sxLiveryUrl + tq : COMMON_IMG_AVATAR;

        this.sxLogoUrl = this.company.sxLogoUrl ? this.company.sxLogoUrl + tq : COMMON_IMG_AVATAR;
        this.getCountry();
        this.preloader.hide();
    }

    //#region Tree Panel

    public closeFilterPanel(): void {
        this.panelIn = false;
    }

    public expandFilterPanelClick(): void {
        // this.treeComponent.openPanelClick();
        this.panelIn = true;
    }

    //#endregion

    //#region Actions

    getCountry() {
        this.countryClient.countryList
            .subscribe((list: Array<CountryModel>) => {
                if (list && list.length > 0) {
                    this.country = list.find(x => x.country_id === this.company.country_id);
                }
            });
    }

    companyEdit(): void {
        const dialogRef = this.dialogService.open(CompanyEditDialog, {
            width: '721px',
            height: '520px',
            data: { company: this.company }
        });

        dialogRef.afterClosed().subscribe(res => {
            if (res) {
                this.cus.company = res;
            }
            this.initFields();
        });
    }

    updateLivery() {
        this.preloader.show();
        this.companyApiClient.liveryUpdate(this.company.company_id)
            .subscribe((res: CompanyModel) => {
                console.log('liveryUpdate-> res:', res);
                this.cus.company = res;
                this.preloader.hide();
                this.initFields();
            })
    }

    priceFileUpload(): void {
        const dialogRef = this.dialogService.open(PriceFileUploadDialog, {
            width: '721px',
            height: '320px', // 520px
            data: { company: this.company }
        });

        dialogRef.afterClosed().subscribe((res: ResponcePriceFileUpload) => {
            if (res && res.success) {
                // this.cus.company = res;
            }
            this.initFields();
        });
    }

    comapnyUsers() {
        this.router.navigate([AppRoutes.Root, AppRoutes.Company, AppRoutes.Users]);
    }

    goToDashboard() {
        this.router.navigate([AppRoutes.Root, AppRoutes.Company, AppRoutes.Dashboard]);
    }
    //#endregion

}

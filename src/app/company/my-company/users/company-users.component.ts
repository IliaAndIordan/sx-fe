import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { DataSource } from '@angular/cdk/table';
import { BehaviorSubject, Observable, merge, Subscription } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
// ---


import { PreloaderService } from 'src/app/@core/services/preloader.service';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
import { SxApiCompanyClient } from 'src/app/@core/api/company/api-client';
import { CompanyUsersDataSource, UserTableCriteria } from './company-users.datasource';
// ---
import { CompanyModel } from 'src/app/@core/api/company/dto';
import { UserModel } from 'src/app/@core/auth/api/dto';
import { UserRole, UserRoleDisplayPipe } from 'src/app/@core/auth/api/enums';
import { PAGE_SIZE_OPTIONS } from 'src/app/@core/const/app-local-storage.const';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { UserEditDialog } from '../../dialogs/user-edit/user-edit.dialog';

@Component({
    templateUrl: './company-users.component.html',
})
export class MyCompanyUsersComponent implements OnInit, OnDestroy, AfterViewInit {
    /**
     * Binding
     */
    // @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatTable, { static: true }) table: MatTable<UserModel>; // initialize
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;


    /**
     * Fields
     */
    company: CompanyModel;
    criteria: UserTableCriteria;
    criteriaChanged: Subscription;

    pageSizeOpt = PAGE_SIZE_OPTIONS;
    pageIndex: number = 0;
    pageSize: number = 25;
    sortActive: string;
    sortDirection: string;

    dataCount = 0;
    dataChanged: Subscription;
    displayedColumns = ['action', 'user_id', 'user_name', 'e_mail', 'user_role', 'is_receive_emails', 'adate'];
    canEdit: boolean;

    /*
    user_id: number;
    public user_name: string;
    public e_mail: string;
    public u_password: string;
    public company_id: number;
    public user_role: UserRole;
    public is_receive_emails: boolean;
    public ip_address: string;
    public udate: Date;
    public adate: Date;
    */
    constructor(
        private toastr: ToastrService,
        private preloader: PreloaderService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        public userDs: CompanyUsersDataSource,
        private cdref: ChangeDetectorRef) {

    }


    ngOnInit(): void {
        // console.log('user:', this.cus.user);
        if (!this.cus.user.company_id && !this.cus.isAdmin) {
            // this.router.navigate([AppRoutes.Company, 'create']);
        }

        this.dataChanged = this.userDs.userListSubject.subscribe((users: Array<UserModel>) => {
            console.log('MyCompanyUsersComponent:userListSubject()->', users);
            this.dataCount = this.userDs.itemsCount;
            this.initFields();
            this.cdref.detectChanges();

        });
        this.criteriaChanged = this.userDs.criteriaChanged.subscribe((criteria: UserTableCriteria) => {
            this.criteria = this.userDs.criteria;
            console.log('MyCompanyUsersComponent:criteriaChanged()->', criteria);
            // this.paginator._changePageSize(this.paginator.pageSize);
            this.initFields();
            this.loadPage();
        });

        this.initFields();
        this.canEdit = (parseInt(this.cus.user.user_role.toString(), 10) < parseInt(UserRole.User.toString(), 10)) ? true : false;
        if (this.criteria.company_id !== this.company.company_id) {
            this.criteria.company_id = this.company.company_id;
            this.criteria.pageIndex = 1;
            this.userDs.criteria = this.criteria;
        } else {
            // console.log('ngOnInit()->', this.criteria.limit);
            this.loadPage();
        }

    }

    ngOnDestroy(): void {
        // console.log('HomeComponent:ngOnDestroy()->');
        if (this.dataChanged) { this.dataChanged.unsubscribe(); }
        if (this.criteriaChanged) { this.criteriaChanged.unsubscribe(); }
    }

    initFields() {
        this.company = this.cus.company;
        this.criteria = this.userDs.criteria;
        this.pageIndex = this.criteria.pageIndex;
        this.pageSize = this.criteria.limit;

        this.sortActive = this.criteria.sortCol;
        this.sortDirection = this.criteria.sortDesc ? 'desc' : 'asc';

        // console.log('initFields-> criteria:', this.criteria);
        const tq = '?m=' + new Date().getTime();


    }


    ngAfterViewInit() {

        this.sort.sortChange.subscribe(() => {
            this.criteria.pageIndex = 0;
            this.criteria.sortCol = this.sort.active;
            this.criteria.sortDesc = this.sort.direction !== 'asc';
            this.userDs.criteria = this.criteria;
        });
        this.paginator.page
            .pipe(
                tap(() => {
                    this.criteria.pageIndex = this.paginator.pageIndex;
                    this.criteria.limit = this.paginator.pageSize;
                    this.userDs.criteria = this.criteria;
                })
            )
            .subscribe();
    }

    //#region Table events

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.criteria.filter = filterValue.trim().toLowerCase();
        this.userDs.criteria = this.criteria;
    }

    loadPage() {
        this.userDs.loadData()
            .subscribe(res => {
                this.initFields();
            });
    }

    //#endregion

    //#region Actions

    editUser(data: UserModel) {
        console.log('editUser-> user:', data);
        const dialogRef = this.dialogService.open(UserEditDialog, {
            width: '721px',
            height: '400px',
            data: { user: data }
        });

        dialogRef.afterClosed().subscribe(res => {
            if (res) {
                console.log('editUser-> res:', res);
                this.table.renderRows();
            }
            this.initFields();
        });
    }

    invaitUser() {
        console.log('invaitUser-> user:', this.company.company_id);
        if (this.company) {

            const dialogRef = this.dialogService.open(UserEditDialog, {
                width: '721px',
                height: '400px',
                data: { company_id: this.company.company_id }
            });

            dialogRef.afterClosed().subscribe(res => {
                if (res) {
                    console.log('invaitUser-> res:', res);
                    // this.paginator._changePageSize(this.paginator.pageSize);
                    this.table.renderRows();
                }
                this.initFields();
            });
        }
    }
    sendEmail(user: UserModel) {
        console.log('sendEmail-> user:', user);
    }

}


import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
// ---
import { PreloaderService } from 'src/app/@core/services/preloader.service';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
// ---
import { UserModel } from 'src/app/@core/auth/api/dto';
import { UserRole, UserRoleDisplayPipe } from 'src/app/@core/auth/api/enums';
import { PAGE_SIZE_OPTIONS, COMMON_IMG_AVATAR } from 'src/app/@core/const/app-local-storage.const';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { UserListTableCriteria, UserListDataSource } from './user-list.datasource';

@Component({
    selector: 'sx-my-company-user-list',
    templateUrl: './user-list.component.html',
})
export class UserListComponent implements OnInit, OnDestroy, AfterViewInit {
    /**
     * Binding
     */
    // @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatTable, { static: true }) table: MatTable<UserModel>; // initialize
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;


    /**
     * Fields
     */
    user: UserModel;
    criteria: UserListTableCriteria;
    criteriaChanged: Subscription;
    imageAvatarUrl = COMMON_IMG_AVATAR;

    pageSizeOpt = PAGE_SIZE_OPTIONS;
    pageIndex = 0;
    pageSize = 25;
    sortActive: string;
    sortDirection: string;

    dataCount = 0;
    dataChanged: Subscription;
    displayedColumns = ['action', 'user_id', 'user_name', 'e_mail', 'user_role', 'is_receive_emails', 'company_id', 'adate'];
    canEdit: boolean;

    constructor(
        private router: Router,
        private toastr: ToastrService,
        private preloader: PreloaderService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        public ds: UserListDataSource,
        private cdref: ChangeDetectorRef) {

    }


    ngOnInit(): void {
        // console.log('user:', this.cus.user);
        if (!this.cus.user.company_id && !this.cus.isAdmin) {
            this.router.navigate(['/', AppRoutes.Company, AppRoutes.MyCompany]);
        }
        this.canEdit = this.cus.user && this.cus.user.user_role === UserRole.Admin;
        this.dataChanged = this.ds.listSubject.subscribe((users: Array<UserModel>) => {
            console.log('UserListComponent:listSubject()->', users);
            this.dataCount = this.ds.itemsCount;
            this.initFields();
        });
        this.criteriaChanged = this.ds.criteriaChanged.subscribe((criteria: UserListTableCriteria) => {
            this.criteria = this.ds.criteria;
            // console.log('MyCompanyUsersComponent:criteriaChanged()->', criteria);
            // this.paginator._changePageSize(this.paginator.pageSize);
            this.initFields();
            this.loadPage();
        });

        this.initFields();
        this.loadPage();
    }

    ngOnDestroy(): void {
        // console.log('HomeComponent:ngOnDestroy()->');
        if (this.dataChanged) { this.dataChanged.unsubscribe(); }
        if (this.criteriaChanged) { this.criteriaChanged.unsubscribe(); }
    }

    initFields() {
        this.user = this.cus.user;
        this.criteria = this.ds.criteria;
        this.pageIndex = this.criteria.pageIndex;
        this.pageSize = this.criteria.limit;

        this.sortActive = this.criteria.sortCol;
        this.sortDirection = this.criteria.sortDesc ? 'desc' : 'asc';

        // console.log('initFields-> criteria:', this.criteria);
        const tq = '?m=' + new Date().getTime();


    }


    ngAfterViewInit() {

        this.sort.sortChange.subscribe(() => {
            this.criteria.pageIndex = 0;
            this.criteria.sortCol = this.sort.active;
            this.criteria.sortDesc = this.sort.direction !== 'asc';
            this.ds.criteria = this.criteria;
        });
        this.paginator.page
            .pipe(
                tap(() => {
                    this.criteria.pageIndex = this.paginator.pageIndex;
                    this.criteria.limit = this.paginator.pageSize;
                    this.ds.criteria = this.criteria;
                })
            )
            .subscribe();
    }

    //#region Table events

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.criteria.filter = filterValue.trim();
        this.ds.criteria = this.criteria;
    }

    loadPage() {
        this.ds.loadData()
            .subscribe(res => {
                this.initFields();
            });
    }

    //#endregion

    //#region Actions

    editUser(data: UserModel) {
        console.log('editUser-> user:', data);
        /*
        const dialogRef = this.dialogService.open(AdminCompanyEditDialog, {
            width: '721px',
            height: '520px',
            data: { company: data }
        });

        dialogRef.afterClosed().subscribe(res => {
            console.log('editCompany-> res:', res);
            if (res) {
                this.table.renderRows();
            }
            this.initFields();
        });
        */
    }

    sendEmail(data: UserModel){
        console.log('sendEmail-> user:', data);
    }

    //#endregion
}


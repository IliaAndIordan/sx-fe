import { NgModule, Injector, ApplicationRef, APP_INITIALIZER } from '@angular/core';
// Common Modules
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
// Application
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ShareModule } from './@share/share.module';
import { CoreModule } from './@core/core.module';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { StartupService } from './@core/services/startup.service';
import { PriceFileUploadDialog } from './company/dialogs/price-file-upload/price-file-upload.dialog';

export function StartupServiceFactory(startupService: StartupService) {
  return () => startupService.load();
}


@NgModule({

  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule,
    ShareModule,
    // ---
    ToastrModule.forRoot(),
    // ---
    CoreModule,

    AppRoutingModule,
  ],
  declarations: [
    AppComponent,
    PriceFileUploadDialog,
  ],
  entryComponents: [
  ],
  providers: [
    StartupService,
    {
      provide: APP_INITIALIZER,
      useFactory: StartupServiceFactory,
      deps: [StartupService],
      multi: true,
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule {

}

import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Observable, of } from 'rxjs';
import { SxApiCompanyClient } from 'src/app/@core/api/company/api-client';
import { CompanyModel, CompanyProductModel } from 'src/app/@core/api/company/dto';
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
import { ProductOpt, SxProductViewModel } from 'src/app/@core/models/pipes/product.pipe';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { AdminService } from '../../admin.service';

export interface ICompanyProductsEdit {
    company: CompanyModel;
}

@Component({
    templateUrl: './company-product-edit.dialog.html',
})

// tslint:disable-next-line:component-class-suffix
export class AdminCompanyProductsEditDialog implements OnInit {

    formGrp: FormGroup;

    company: CompanyModel;

    companyProducts: Array<CompanyProductModel>;
    // productOpt = ProductOpt;
    productOpt: SxProductViewModel[];
    hasSpinner = false;
    errorMessage: string;

    constructor(
        private fb: FormBuilder,
        private router: Router,
        private toastr: ToastrService,
        private spinetService: SpinnerService,
        private cus: CurrentUserService,
        private cService: AdminService,
        private cClient: SxApiCompanyClient,
        public dialogRef: MatDialogRef<ICompanyProductsEdit>,
        @Inject(MAT_DIALOG_DATA) public data: ICompanyProductsEdit) {
        this.company = data.company;
    }

    ngOnInit(): void {

        this.errorMessage = undefined;
        this.hasSpinner = false;
        this.productOpt = new Array<SxProductViewModel>();
        this.formGrp = this.fb.group({
        });
        this.cService.selCompany = this.company;
        this.loadCompanyProductList();
    }


    onCansel(): void {
        this.dialogRef.close();
    }

    addProduct(product: SxProductViewModel) {
        console.log('addProduct -> product:', product);
        if (product) {
            this.errorMessage = undefined;
            this.hasSpinner = true;

            if (product.isGranted) {
                this.spinetService.display(true);
                this.cClient.productAdd(this.company.company_id, product.id)
                    .subscribe((res: Array<CompanyProductModel>) => {
                        console.log('productAdd -> res:', res);
                        this.spinetService.display(false);
                        if (res) {
                            this.cService.companyProducts = res;
                            this.companyProducts = this.cService.companyProducts;
                            this.initProductOpt();
                            this.errorMessage = undefined;
                            this.hasSpinner = false;
                            this.toastr.success('Compnay', 'Operation Succesfull: Compnay Product Add');
                            // this.dialogRef.close(this.company);
                        }
                    },
                    err => {
                        console.error('Observer got an error: ' + err);
                        this.errorMessage = 'Compnay Product Add  Failed. ' + err;
                        this.spinetService.display(false);
                    },
                    () => console.log('Observer got a complete notification'));
            } else {
                const cp = this.companyProducts.find(x => x.product_id === product.id);
                console.log('productDelete -> cp:', cp);
                if (cp) {
                    this.spinetService.display(true);
                    this.cClient.productDelete(this.company.company_id, cp.company_product_id)
                        .subscribe((res: Array<CompanyProductModel>) => {
                            console.log('productDelete -> res:', res);
                            this.spinetService.display(false);
                            if (res) {
                                this.cService.companyProducts = res;
                                this.companyProducts = this.cService.companyProducts;
                                this.initProductOpt();
                                this.errorMessage = undefined;
                                this.hasSpinner = false;
                                this.toastr.success('Compnay', 'Operation Succesfull: Compnay Product Add');
                            }
                        },
                        err => {
                            console.error('Observer got an error: ' + err);
                            this.errorMessage = 'Compnay Product Add  Failed. ' + err;
                            this.spinetService.display(false);
                            /*
                            setTimeout((router: Router,) => {
                                this.errorMessage = undefined;
                                this.hasSpinner = false;
                                this.dialogRef.close(this.company);
                            }, 2000);
                            */
                        },
                        () => console.log('Observer got a complete notification'));
                }

            }



        }


    }

    //#region  Country Filter

    initProductOpt() {
        this.productOpt = new Array<SxProductViewModel>();
        ProductOpt.forEach(product => {
            if (this.companyProducts && this.companyProducts.length > 0) {
                const cp = this.companyProducts.find(x => x.product_id === product.id);
                product.isGranted = cp ? true : false;
                this.productOpt.push(product);
            } else {
                product.isGranted = false;
                this.productOpt.push(product);
            }
        });
    }



    loadCompanyProductList() {
        this.cService.loadCompanyProducts()
            .subscribe((resp: Array<CompanyProductModel>) => {
                // console.log('loadCompanyProductList -> data:', resp);
                this.companyProducts = resp;
                this.initProductOpt();
            });
    }

    //#endregion

}

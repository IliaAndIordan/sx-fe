import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
// Services
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
import { SxApiCompanyClient } from 'src/app/@core/api/company/api-client';
import { SxApiCountryClient } from 'src/app/@core/api/country/api-client';
import { CompanyService } from 'src/app/company/company.service';
// Models and Components
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { UserModel, ResponseAuthenticate } from 'src/app/@core/auth/api/dto';
import { CompanyModel } from 'src/app/@core/api/company/dto';
import { CountryModel } from 'src/app/@core/api/country/dto';
import { CompanyTypeOpt } from 'src/app/@core/api/company/enums';

export interface ICompanyEdit {
    company: CompanyModel;
}

@Component({
    templateUrl: './company-edit.dialog.html',
})

// tslint:disable-next-line:component-class-suffix
export class AdminCompanyEditDialog implements OnInit {

    formGrp: FormGroup;

    company: CompanyModel;
    selCountry: CountryModel;
    countryList: Array<CountryModel>;
    countryOpt: Observable<CountryModel[]>;
    companyTypeOpt = CompanyTypeOpt;

    hasSpinner = false;
    errorMessage: string;

    constructor(
        private fb: FormBuilder,
        private router: Router,
        private toastr: ToastrService,
        private cus: CurrentUserService,
        private cService: CompanyService,
        private countryApiCliente: SxApiCountryClient,
        private cApiClient: SxApiCompanyClient,
        public dialogRef: MatDialogRef<ICompanyEdit>,
        @Inject(MAT_DIALOG_DATA) public data: ICompanyEdit) {
        this.company = data.company;
    }

    get company_name() { return this.formGrp.get('company_name'); }
    get branch_code() { return this.formGrp.get('branch_code'); }
    get company_type_id() { return this.formGrp.get('company_type_id'); }
    get logo_url() { return this.formGrp.get('logo_url'); }
    get web_url() { return this.formGrp.get('web_url'); }
    get notes() { return this.formGrp.get('notes'); }
    get country() { return this.formGrp.get('country'); }

    fError(fname: string): string {
        const field = this.formGrp.get(fname);
        return field.hasError('required') ? 'Field ' + fname + '  is required.' :
            field.hasError('minlength') ? 'Field ' + fname + ' to short.' :
                field.hasError('maxlength') ? 'Field ' + fname + ' to long.' : '';
    }

    ngOnInit(): void {

        this.errorMessage = undefined;
        this.hasSpinner = false;

        this.formGrp = this.fb.group({
            company_name: new FormControl(this.company ? this.company.company_name : '', [Validators.required, Validators.maxLength(512)]),
            branch_code: new FormControl(this.company ? this.company.branch_code : '', [Validators.required, Validators.maxLength(256)]),
            company_type_id: new FormControl(this.company ? this.company.company_type_id : '', [Validators.required]),
            country: new FormControl('', [Validators.required]),
            logo_url: new FormControl(this.company ? this.company.logo_url : '', []),
            web_url: new FormControl(this.company ? this.company.web_url : '', []),
            notes: new FormControl(this.company ? this.company.notes : '', []),

        });

        this.loadCountryList();
    }


    onCansel(): void {
        this.dialogRef.close();
    }

    onSubmit() {
        this.formGrp.updateValueAndValidity();
        if (this.formGrp.valid) {
            this.errorMessage = undefined;
            this.hasSpinner = true;

            if (!this.company) {
                this.company = new CompanyModel();
            }
            const message = this.company.company_id > 0 ? 'Update' : 'Create';
            const country = this.country.value as CountryModel;
            this.company.company_name = this.company_name.value;
            this.company.branch_code = this.branch_code.value;

            this.company.company_type_id = this.company_type_id.value;
            this.company.country_id = country.country_id;
            this.company.logo_url = this.logo_url.value;
            this.company.web_url = this.web_url.value;
            this.company.notes = this.notes.value;

            this.company.actor_id = this.cus.user.user_id;

            this.cApiClient.companySave(this.company)
                .subscribe((res: CompanyModel) => {
                    console.log('companySave -> res:', res);
                    if (res && res.company_id) {
                        this.company = res;
                        this.errorMessage = undefined;
                        this.hasSpinner = false;
                        this.toastr.success('Compnay', 'Operation Succesfull: Compnay ' + message);
                        this.dialogRef.close(this.company);
                    }
                },
                    err => {
                        console.error('Observer got an error: ' + err);
                        this.errorMessage = 'Compnay ' + message + ' Failed. ' + err;
                        setTimeout((router: Router,) => {
                            this.errorMessage = undefined;
                            this.hasSpinner = false;
                            this.dialogRef.close(this.company);
                        }, 2000);
                        // this.spinerService.display(false);
                        // this.toastr.error('Compnay', 'Operation Failed: Compnay ' + message);
                    },
                    () => console.log('Observer got a complete notification'));

        }


    }

    //#region  Country Filter

    countryValueChange(event: CountryModel) {
        console.log('countryValueChange: event:', event);
        console.log('countryValueChange: msg:', this.country.value);
        if (this.country.valid) {
            const country: CountryModel = this.country.value;
        }
    }

    displayCountry(cm: CountryModel): string {
        let rv: string = '';
        console.log('displayCountry -> cm', cm);
        if (cm) {
            this.selCountry = cm;
            rv = '<img class="icon-18" [src]="' + cm.flagUrl + '" alt="flag">';
            rv = cm.country_name;
            rv += cm.country_iso2 ? ' - ' + cm.region_name : '';
        }
        return rv;
    }

    initContryOpt() {
        this.countryOpt = of(this.countryList);
        this.countryOpt = this.country.valueChanges
            .pipe(
                startWith(null),
                map(state => state ? this.filterContry(state) :
                    (this.countryList ? this.countryList.slice() : new Array<CountryModel>()))
            );
        this.formGrp.patchValue({ 'country': this.selCountry });
        this.formGrp.updateValueAndValidity();
    }

    filterContry(val: any): CountryModel[] {
        // console.log('filterDpc -> val', val);
        if (val && val.country_id) {
            this.selCountry = val as CountryModel;
        }

        // console.log('filterBranches -> this.selDpc', this.selDpc);
        const value = val.country_name || val; // val can be companyName or string
        const filterValue = value ? value.toLowerCase() : '';
        // console.log('_filter -> filterValue', filterValue);
        let rv = new Array<CountryModel>()
        if (this.countryList && this.countryList.length) {
            rv = this.countryList.filter(x => (
                x.country_name.toLowerCase().indexOf(filterValue) > -1 ||
                x.country_iso2.toLowerCase().indexOf(filterValue) > -1 ||
                x.region_name.toLowerCase().indexOf(filterValue) > -1));
        }
        // console.log('_filter -> rv', rv);
        return rv;
    }


    loadCountryList() {
        this.countryApiCliente.countryList
            .subscribe((resp: Array<CountryModel>) => {
                // console.log('loadCountryList -> data:', resp);
                this.countryList = resp;
                if (this.countryList && this.countryList.length > 0) {
                    this.selCountry = this.countryList.find(x => x.country_id === this.company.country_id);
                }
                this.initContryOpt();
            });
    }

    //#endregion

}

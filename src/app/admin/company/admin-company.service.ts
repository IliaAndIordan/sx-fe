import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';

// ---Services
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
import { SxApiProjectClient } from 'src/app/@core/api/project/api-client';
// ---Models
import { CompanyModel, CompanyProductModel } from 'src/app/@core/api/company/dto';
import { SxProjectModel } from '../../@core/api/project/dto';
import { AppStore } from 'src/app/@core/const/app-local-storage.const';
import { SxApiCompanyClient } from 'src/app/@core/api/company/api-client';
import { UserModel } from 'src/app/@core/auth/api/dto';

const SEL_TABIDX_KEY = 'company-selected-tab-idx';
const COMPANY_USERS_KEY = 'admin-company-user-list';
const COMPANY_USERS_CRITERIA_KEY = 'admin-company-user-list-criteria';

export class ListCriteria {
    public limit: number;
    public offset: number;
    public filter?: string;
    public sortCol: string;
    public sortDesc: boolean;
    public hiddenInactive: boolean;

    get pageIndex(): number {
        let rv = 0;
        rv = this.offset ? (Math.floor(this.offset / (this.limit ? this.limit : 25))) : 0;
        return rv;
    }

    set pageIndex(value: number) {
        this.offset = value ? (value) * (this.limit ? this.limit : 25) : 0;
    }

}

@Injectable({
    providedIn: 'root',
})
export class AdminCompanyService {

    /**
     *  Fields
     */


    constructor(
        private cus: CurrentUserService,
        private cClient: SxApiCompanyClient,
        private pClient: SxApiProjectClient) {
    }

    //#region company

    // tslint:disable-next-line: member-ordering
    companyChanged = new Subject<CompanyModel>();

    get company(): CompanyModel {
        const objStr = localStorage.getItem(AppStore.AdminSelCompany);
        let obj: CompanyModel;
        if (objStr) {
            obj = Object.assign(new CompanyModel(), JSON.parse(objStr));
        }
        // console.log('company-> obj', obj);
        return obj;
    }

    set company(value: CompanyModel) {
        if (value) {
            localStorage.setItem(AppStore.AdminSelCompany, JSON.stringify(value));
        } else {
            localStorage.removeItem(AppStore.AdminSelCompany);
        }
        this.companyChanged.next(value);
    }

    loadCompany(companyId: number): Observable<CompanyModel> {

        return new Observable<CompanyModel>(subscriber => {

            this.cClient.companyById(companyId)
                .subscribe((resp: CompanyModel) => {
                    console.log('companyById -> resp:', resp);
                    // this.company = resp;
                    // return resp;
                    subscriber.next(resp);
                });
        });
    }

    //#region  Users

    //#region criteria

    // tslint:disable-next-line: member-ordering
    public userListCriteriaChanged = new Subject<ListCriteria>();

    public get userListCriteria(): ListCriteria {
        const onjStr = localStorage.getItem(COMPANY_USERS_CRITERIA_KEY);
        let obj: ListCriteria;
        if (onjStr) {
            obj = Object.assign(new ListCriteria(), JSON.parse(onjStr));
        } else {
            obj = new ListCriteria();
            obj.limit = 25;
            obj.offset = 0;
            obj.sortCol = 'company_name';
            obj.sortDesc = false;
            localStorage.setItem(COMPANY_USERS_CRITERIA_KEY, JSON.stringify(obj));
        }
        return obj;
    }

    public set userListCriteria(obj: ListCriteria) {
        if (obj) {
            localStorage.setItem(COMPANY_USERS_CRITERIA_KEY, JSON.stringify(obj));
        } else {
            localStorage.removeItem(COMPANY_USERS_CRITERIA_KEY);
        }
        this.userListCriteriaChanged.next(obj);
    }

    //#endregion

    // tslint:disable-next-line: member-ordering
    companyUsersChanged = new Subject<Array<UserModel>>();

    get companyUsers(): Array<UserModel> {
        const onjStr = localStorage.getItem(COMPANY_USERS_KEY);
        let obj: Array<UserModel>;
        if (onjStr) {
            obj = Object.assign(new Array<UserModel>(), JSON.parse(onjStr));
        }
        return obj;
    }

    set companyUsers(value: Array<UserModel>) {
        const oldValue = this.companyUsers;
        if (value) {
            localStorage.setItem(COMPANY_USERS_KEY, JSON.stringify(value));
        } else {
            localStorage.removeItem(COMPANY_USERS_KEY);
        }

        this.companyUsersChanged.next(value);
    }

    loadUserList(): Observable<Array<UserModel>> {
        return new Observable<Array<UserModel>>(subscriber => {
            this.cClient.companyUserList(this.company.company_id)
            .subscribe((resp: Array<UserModel>) => {
                // this.spinerService.display(false);
                // console.log('loadUserList -> resp:', resp);
                this.companyUsers = resp;
                subscriber.next(this.companyUsers);
            });
        });

    }

    //#endregion

    //#region company ProductM

    products: Array<CompanyProductModel>;

    loadProducts(company: CompanyModel): Observable<Array<CompanyProductModel>> {

        return new Observable<Array<CompanyProductModel>>(subscriber => {

            this.cClient.companyProductList(company.company_id)
                .subscribe((resp: Array<CompanyProductModel>) => {
                    console.log('loadProducts -> data:', resp);
                    this.products = resp;
                    subscriber.next(this.products);
                });
        });
    }

    //#endregion

    //#region Action Methods

    updateLivery(company: CompanyModel): Observable<CompanyModel> {

        return new Observable<CompanyModel>(subscriber => {

            this.cClient.liveryUpdate(this.company.company_id)
                .subscribe((res: CompanyModel) => {
                    console.log('updateLivery-> res:', res);
                    this.company = res;
                    subscriber.next(res);
                });
        });
    }

    //#endregion

    //#endregion

    //#region Project

    // tslint:disable-next-line: member-ordering
    projectChanged = new Subject<SxProjectModel>();

    get project(): SxProjectModel {
        const onjStr = localStorage.getItem(AppStore.AdminSelProject);
        let obj: SxProjectModel;
        if (onjStr) {
            obj = Object.assign(new SxProjectModel(), JSON.parse(onjStr));
        }
        return obj;
    }

    set project(value: SxProjectModel) {
        const oldValue = this.project;
        if (value) {
            localStorage.setItem(AppStore.AdminSelProject, JSON.stringify(value));
        } else {
            localStorage.removeItem(AppStore.AdminSelProject);
        }

        this.projectChanged.next(value);
    }

    //#endregion


    //#region Board Tab Index

    selTabIdxChanged = new BehaviorSubject<number>(undefined);

    get selTabIdx(): number {
        let rv = 0;
        const dataStr = localStorage.getItem(SEL_TABIDX_KEY);
        // console.log('selFolderId-> dataStr', dataStr);
        if (dataStr) {
            try {
                rv = parseInt(dataStr, 10);
            }
            catch {
                localStorage.removeItem(SEL_TABIDX_KEY);
                rv = 1;
            }

        }
        // console.log('selTabIdx-> rv', rv);
        return rv;
    }

    set selTabIdx(value: number) {
        // console.log('selTabIdx->', value);
        const oldValue = this.selTabIdx;

        localStorage.setItem(SEL_TABIDX_KEY, JSON.stringify(value));
        if (oldValue !== value) {
            this.selTabIdxChanged.next(value);
        }
    }

    //#endregion
}

import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
// ---Services
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
import { PreloaderService } from 'src/app/@core/services/preloader.service';
// ---Models
import { SxProjectModel, SxUseCaseModel } from 'src/app/@core/api/project/dto';
import { PageViewType } from 'src/app/@core/const/app-routes.const';
import { Animate } from 'src/app/@core/const/animation.const';
import { AdminCompanyService, ListCriteria } from '../admin-company.service';
import { UserListDataSource, UserListTableCriteria } from '../../user-list/user-list.datasource';
import { CompanyModel } from 'src/app/@core/api/company/dto';
import { UserModel } from 'src/app/@core/auth/api/dto';
import { UserEditDialog } from 'src/app/company/dialogs/user-edit/user-edit.dialog';

export const PAGE_VIEW_TYPE = 'sx_project_usecase_pageview';

@Component({
    selector: 'sx-admin-company-tab-users',
    templateUrl: './admin-company-home-tab-users.component.html',
})
export class AdminCompanyTabUsersComponent implements OnInit, OnDestroy {
    /**
     * Binding
     */
    @Output() spmProjectOpen: EventEmitter<SxProjectModel> = new EventEmitter<SxProjectModel>();
    @Output() editUseCase: EventEmitter<SxUseCaseModel> = new EventEmitter<SxUseCaseModel>();
    @Input() tabIdx: number;
    /**
     * Fields
     */
    selTabIdx: number;
    selTabIdxChanged: Subscription;

    company: CompanyModel;
    companyChanged: Subscription;
    users: Array<UserModel>
    uersChanged: Subscription;

    dataCount: number;
    pages: number;
    criteria: ListCriteria;
    criteriaChanged: Subscription;

    project: SxProjectModel;
    projectChanged: Subscription;


    pvtOpt = PageViewType;
    showCardVar: string = Animate.show;
    showListVar: string = Animate.hide;
    showTableVar: string = Animate.hide;



    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastrService,
        private preloader: PreloaderService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        public tableds: UserListDataSource,
        private aService: AdminCompanyService) {

    }


    ngOnInit(): void {
        this.preloader.show();
        this.dataCount = 0;

        this.projectChanged = this.cus.selProjectChanged.subscribe((project: SxProjectModel) => {
            this.initFields();
        });

        this.companyChanged = this.aService.companyChanged.subscribe((company: CompanyModel) => {
            this.initFields();
            this.loadUserList();
        });

        this.criteriaChanged = this.aService.userListCriteriaChanged.subscribe((criteria: ListCriteria) => {
            this.initFields();
            this.initUsers();
        });

        this.selTabIdxChanged = this.aService.selTabIdxChanged.subscribe(tabIdx => {
            // console.log('packageCahnged -> pkg', pkg);
            this.initFields();
            this.initUsers();
        });

        this.initFields();
        this.loadUserList();
    }

    ngOnDestroy(): void {
        if (this.selTabIdxChanged) { this.selTabIdxChanged.unsubscribe(); }
        if (this.companyChanged) { this.companyChanged.unsubscribe(); }

        if (this.projectChanged) { this.projectChanged.unsubscribe(); }

    }

    initFields() {
        this.selTabIdx = this.aService.selTabIdx;
        this.company = this.aService.company;
        this.criteria = this.aService.userListCriteria;

        this.project = this.cus.selProject;


        this.preloader.hide();
    }

    //#region  Page View 

    get pageViewType(): string {
        const settings = localStorage.getItem(PAGE_VIEW_TYPE);
        let rv = PageViewType.Card;
        if (settings) {
            rv = settings;
        } else {
            this.pageViewType = rv;
        }
        return rv;
    }

    set pageViewType(value: string) {
        if (value) {
            localStorage.setItem(PAGE_VIEW_TYPE, value);
        }
    }

    // Toggle how items are being displayed list or card
    public toggleCardView(): void {
        this.showCardVar = Animate.show;
        this.showListVar = Animate.hide;
        this.showTableVar = Animate.hide;
        this.pageViewType = PageViewType.Card;
    }

    public toggleListView(): void {
        this.showCardVar = Animate.hide;
        this.showListVar = Animate.show;
        this.showTableVar = Animate.hide;
        this.pageViewType = PageViewType.List;
    }

    public toggleTableView(): void {
        console.log('toggleTableView -> ');
        this.showCardVar = Animate.hide;
        this.showListVar = Animate.hide;
        this.showTableVar = Animate.show;
        this.pageViewType = PageViewType.Table;
    }

    //#endregion

    //#region Action Methods

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.criteria.filter = filterValue.trim();
        this.aService.userListCriteria = this.criteria;
    }

    createUserClick() {
        console.log('createUserClick -> company=', this.company);
    }
    editUserClick(user: UserModel) {
        console.log('editUserClick -> user=', user);
    }

    spmProjectOpenClick() {

        console.log('spmProjectOpen -> spmProjectObj=', this.project);
        if (this.project) {
            this.spmProjectOpen.emit(this.project);
        }

    }



    editUser(data: UserModel) {
        console.log('editUser-> user:', data);
        if (data) {
            const dialogRef = this.dialogService.open(UserEditDialog, {
                width: '721px',
                height: '400px',
                data: { user: data }
            });

            dialogRef.afterClosed().subscribe(res => {
                if (res) {
                    console.log('editUser-> res:', res);
                    this.loadUserList();
                }
                // this.initFields();
            });
        } else {
            this.inviteUser();
        }

    }

    inviteUser() {
        console.log('inviteUser-> user:', this.company.company_id);
        if (this.company) {

            const dialogRef = this.dialogService.open(UserEditDialog, {
                width: '721px',
                height: '400px',
                data: { company_id: this.company.company_id }
            });

            dialogRef.afterClosed().subscribe(res => {
                if (res) {
                    console.log('inviteUser-> res:', res);
                    // this.paginator._changePageSize(this.paginator.pageSize);
                    this.loadUserList();
                }
            });
        }
    }
    sendEmail(user: UserModel) {
        console.log('sendEmail-> user:', user);
    }

    //#endregion

    //#region Data

    refreshClick() {
        this.loadUserList();
    }


    loadUserList() {
        // this.spinerService.display(true);
        this.preloader.show();
        this.aService.loadUserList()
            .subscribe((resp: Array<UserModel>) => {
                // this.spinerService.display(false);
                this.preloader.hide();
                // console.log('loadUserList -> resp:', resp);
                // this.users = resp;
                this.initUsers();
            });
    }

    initUsers(): void {

        let filteredUsers = new Array<UserModel>();
        const usersAll = this.aService.companyUsers;
        this.dataCount = usersAll ? usersAll.length : 0;

        this.pages = Math.ceil(this.dataCount / this.criteria.limit);
        if (this.pages < this.criteria.pageIndex) {
            this.criteria.pageIndex = 1;
        }
        filteredUsers = usersAll;
        // filteredUsers =  this.filterUsers(usersAll);
        let endIdx = this.criteria.offset ? (this.criteria.offset + this.criteria.limit) : this.criteria.limit;
        // console.log('initUsers -> endIdx1', endIdx);
        if (endIdx > usersAll.length) {
            endIdx = usersAll.length;
        }
        // console.log('initUsers -> endIdx', endIdx);
        // console.log('initUsers -> criteria', this.criteria);
        this.users = filteredUsers.slice(this.criteria.offset, endIdx);
        // console.log('initUsers -> users', this.users);
        /*
        if (this.paginationComponent && this.activePage != this.paginationComponent.activePage) {
            this.paginationComponent.handlePageNumberClick(this.activePage);
        }
        */
    }

    //#endregion
}

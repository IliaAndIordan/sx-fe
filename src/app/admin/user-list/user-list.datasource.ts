import { DataSource } from '@angular/cdk/table';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
// ---Services
import { SxApiCompanyClient } from 'src/app/@core/api/company/api-client';
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
// ---Models
import { UserModel, RespCompanyUsers } from 'src/app/@core/auth/api/dto';
import { CollectionViewer } from '@angular/cdk/collections';
import { CompanyModel, RespCompanyList, RespUserList } from 'src/app/@core/api/company/dto';

export class UserListTableCriteria {
    public limit: number;
    public offset: number;
    public filter?: string;
    public sortCol: string;
    public sortDesc: boolean;

    get pageIndex(): number {
        let rv = 0;
        rv = this.offset ? (Math.floor(this.offset / (this.limit ? this.limit : 25))) : 0;
        return rv;
    }

    set pageIndex(value: number) {
        this.offset = value ? (value) * (this.limit ? this.limit : 25) : 0;
    }

}
export const KEY_CRITERIA = 'admin_user_list_criteria';
@Injectable()
export class UserListDataSource extends DataSource<UserModel> {

    selUser: UserModel;

    private _data: Array<UserModel>;
    private _isLoading = false;

    numberOfPages = 0;
    itemsCount = 0;

    dataChange: BehaviorSubject<UserModel[]> = new BehaviorSubject<UserModel[]>([]);
    listSubject = new BehaviorSubject<UserModel[]>([]);
    private loadingSubject = new BehaviorSubject<boolean>(false);
    loading$ = this.loadingSubject.asObservable();

    get isLoading(): boolean {
        return this._isLoading;
    }

    set isLoading(value: boolean) {
        this._isLoading = value;
        this.loadingSubject.next(this._isLoading);
    }

    get data(): Array<UserModel> {
        return this._data;
    }

    set data(value: Array<UserModel>) {
        this._data = value;
        this.listSubject.next(this._data as UserModel[]);
    }

    constructor(
        private cus: CurrentUserService,
        private companyApiClient: SxApiCompanyClient) {
        super();
    }

    //#region criteria

    public criteriaChanged = new Subject<UserListTableCriteria>();

    public get criteria(): UserListTableCriteria {
        const onjStr = localStorage.getItem(KEY_CRITERIA);
        let obj: UserListTableCriteria;
        if (onjStr) {
            obj = Object.assign(new UserListTableCriteria(), JSON.parse(onjStr));
        } else {
            obj = new UserListTableCriteria();
            obj.limit = 25;
            obj.offset = 0;
            obj.sortCol = 'company_name';
            obj.sortDesc = false;
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        }
        return obj;
    }

    public set criteria(obj: UserListTableCriteria) {
        if (obj) {
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        } else {
            localStorage.removeItem(KEY_CRITERIA);
        }
        this.criteriaChanged.next(obj);
    }

    //#endregion

    //#region DataSource Interface Methods

    /**
     * Connect this data source to the table. The table will only update when
     * the returned stream emits new items.
     * @returns A stream of the items to be rendered.
     */
    connect(collectionViewer: CollectionViewer): Observable<UserModel[]> {
        // return this.userListSubject.asObservable();
        return this.listSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        // this.userListSubject.complete();
        // this.loadingSubject.complete();
    }


    loadData(): Observable<Array<UserModel>> {
        console.log('usersDs loadData->');
        console.log('usersDs loadData-> criteria=', this.criteria);

        this.isLoading = true;
        const limit = this.criteria.limit;
        const offset = this.criteria.offset;

        const orderCol = this.criteria.sortCol;
        const isDesc = this.criteria.sortDesc; // sortDirection && sortDirection === 'asc' ? false : true;
        return new Observable<Array<UserModel>>(subscriber => {

            this.companyApiClient.userListTable(limit, offset, orderCol, isDesc, this.criteria.filter)
                .subscribe((res: RespUserList) => {
                    const resp = Object.assign(new RespUserList(), res);
                    console.log('loadData-> UserModel=', resp.success);
                    this.data = new Array<UserModel>();

                    if (resp && resp.success) {
                        if (resp && resp.data &&
                            resp.data.user_list && resp.data.user_list.length > 0) {
                            resp.data.user_list.forEach(iu => {
                                const user = UserModel.fromTimestampJSON(iu);
                                this.data.push(user);
                            });
                        }

                        this.itemsCount = resp.data.totals[0].total_rows;
                        this.listSubject.next(this.data);
                        subscriber.next(this.data);
                    }
                    console.log('loadData-> data=', this.data);
                    console.log('loadData-> itemsCount=', this.itemsCount);
                    this.isLoading = false;
                }, msg => {
                    console.log('loadData -> ' + msg);

                    this.itemsCount = 0;
                    this.data = new Array<UserModel>();
                    this.isLoading = false;

                    // component.errorMessage = msg;
                });

        });

    }
    //#endregion
}

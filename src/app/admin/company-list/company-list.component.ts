import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { DataSource } from '@angular/cdk/table';
import { BehaviorSubject, Observable, merge, Subscription } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
// ---
import { PreloaderService } from 'src/app/@core/services/preloader.service';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
import { SxApiCompanyClient } from 'src/app/@core/api/company/api-client';
// ---
import { CompanyModel, CompanyProductModel } from 'src/app/@core/api/company/dto';
import { UserModel } from 'src/app/@core/auth/api/dto';
import { UserRole, UserRoleDisplayPipe } from 'src/app/@core/auth/api/enums';
import { PAGE_SIZE_OPTIONS, COMMON_IMG_AVATAR } from 'src/app/@core/const/app-local-storage.const';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { CompanyListDataSource, CompanyListTableCriteria } from './company-list.datasource';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { AdminCompanyEditDialog } from '../dialogs/company-edit/company-edit.dialog';
import { AdminCompanyProductsEditDialog } from '../dialogs/product-edit/company-product-edit.dialog';
import { AdminService } from '../admin.service';
import { AdminCompanyService } from '../company/admin-company.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';

@Component({
    templateUrl: './company-list.component.html',
})
export class CompanyListComponent implements OnInit, OnDestroy, AfterViewInit {
    /**
     * Binding
     */
    // @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatTable, { static: true }) table: MatTable<UserModel>; // initialize
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;


    /**
     * Fields
     */
    company: CompanyModel;
    criteria: CompanyListTableCriteria;
    criteriaChanged: Subscription;
    imageAvatarUrl = COMMON_IMG_AVATAR;

    pageSizeOpt = PAGE_SIZE_OPTIONS;
    pageIndex = 0;
    pageSize = 12;
    sortActive: string;
    sortDirection: string;

    dataCount = 0;
    dataChanged: Subscription;
    displayedColumns = ['action', 'company_id', 'company_name', 'branch_code', 'company_type_id', 'parent_company_id', 'adate', 'notes'];
    canEdit: boolean;
    companyProductChanged: Subscription;

    constructor(
        private router: Router,
        private toastr: ToastrService,
        private preloader: PreloaderService,
        private spiner: SpinnerService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private cService: AdminService,
        private aService: AdminCompanyService,
        public companyListDs: CompanyListDataSource,
        private cdref: ChangeDetectorRef) {

    }


    ngOnInit(): void {
        // console.log('user:', this.cus.user);
        if (!this.cus.user.company_id && !this.cus.isAdmin) {
            this.router.navigate(['/', AppRoutes.Company, AppRoutes.MyCompany]);
        }
        this.canEdit = this.cus.user && this.cus.user.user_role === UserRole.Admin;
        this.dataChanged = this.companyListDs.listSubject.subscribe((users: Array<CompanyModel>) => {
            // console.log('MyCompanyUsersComponent:userListSubject()->', users);
            this.dataCount = this.companyListDs.itemsCount;
            this.initFields();
        });
        this.criteriaChanged = this.companyListDs.criteriaChanged.subscribe((criteria: CompanyListTableCriteria) => {
            this.criteria = this.companyListDs.criteria;
            // console.log('MyCompanyUsersComponent:criteriaChanged()->', criteria);
            // this.paginator._changePageSize(this.paginator.pageSize);
            this.initFields();
            this.loadPage();
        });

        this.companyProductChanged = this.cService.companyProductsChanged.subscribe((companyProducts: Array<CompanyProductModel>) => {
            // console.log('companyProductsChanged ->', companyProducts);
            if (this.cService.selCompany) {
                const com = this.companyListDs.data.find(x => x.country_id === this.cService.selCompany.company_id);

                if (com) {
                    com.productsCount = companyProducts ? companyProducts.length : 0;
                }
            }
        });

        this.initFields();
        this.loadPage();
    }

    ngOnDestroy(): void {
        // console.log('HomeComponent:ngOnDestroy()->');
        if (this.dataChanged) { this.dataChanged.unsubscribe(); }
        if (this.criteriaChanged) { this.criteriaChanged.unsubscribe(); }
    }

    initFields() {
        this.company = this.cus.company;
        this.criteria = this.companyListDs.criteria;
        this.pageIndex = this.criteria.pageIndex;
        this.pageSize = this.criteria.limit;

        this.sortActive = this.criteria.sortCol;
        this.sortDirection = this.criteria.sortDesc ? 'desc' : 'asc';

        // console.log('initFields-> criteria:', this.criteria);
        const tq = '?m=' + new Date().getTime();


    }


    ngAfterViewInit() {

        this.sort.sortChange.subscribe(() => {
            this.criteria.pageIndex = 0;
            this.criteria.sortCol = this.sort.active;
            this.criteria.sortDesc = this.sort.direction !== 'asc';
            this.companyListDs.criteria = this.criteria;
        });
        this.paginator.page
            .pipe(
                tap(() => {
                    this.criteria.pageIndex = this.paginator.pageIndex;
                    this.criteria.limit = this.paginator.pageSize;
                    this.companyListDs.criteria = this.criteria;
                })
            )
            .subscribe();
    }

    //#region Table events

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.criteria.filter = filterValue.trim();
        this.companyListDs.criteria = this.criteria;
    }

    loadPage() {
        this.companyListDs.loadData()
            .subscribe(res => {
                this.initFields();
            });
    }

    //#endregion

    //#region Actions

    gotoCompanyHome(data: CompanyModel) {
        if (data) {
            this.spiner.display(true);
            this.aService.loadCompany(data.company_id)
                .subscribe((res: CompanyModel) => {
                    // console.log('loadCompany-> res:', res);
                    this.spiner.display(false);
                    this.aService.company = res;
                    this.router.navigate([AppRoutes.Admin, AppRoutes.Company]);
            });
        }
    }

    editCompany(data: CompanyModel) {
        // console.log('editCompany-> company:', data);
        const dialogRef = this.dialogService.open(AdminCompanyEditDialog, {
            width: '721px',
            height: '520px',
            data: { company: data }
        });

        dialogRef.afterClosed().subscribe(res => {
            console.log('editCompany-> res:', res);
            if (res) {
                this.table.renderRows();
            }
            this.initFields();
        });
    }

    editCompanyProducts(data: CompanyModel) {
        // console.log('editCompanyProducts-> company:', data);
        const dialogRef = this.dialogService.open(AdminCompanyProductsEditDialog, {
            width: '721px',
            height: '520px',
            data: { company: data }
        });

        dialogRef.afterClosed().subscribe(res => {
            console.log('editCompanyProducts-> res:', res);
            /*if (res) {
                this.table.renderRows();
            }*/
            this.initFields();
            this.loadPage();
        });
    }

    //#endregion
}


import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// --- Modules
import { ShareModule } from '../@share/share.module';
import { SxMaterialModule } from '../@share/material.module';
import { AdminRoutingModule, routedComponents } from './admin-routing.module';
import { CompanyService } from '../company/company.service';
import { CompanyListDataSource } from './company-list/company-list.datasource';
import { UtilityBarComponent } from './utility-bar/utility-bar.component';
import { AdminCompanyEditDialog } from './dialogs/company-edit/company-edit.dialog';
import { UserListDataSource } from './user-list/user-list.datasource';
import { AdminCompanyProductsEditDialog } from './dialogs/product-edit/company-product-edit.dialog';
import { AdminCompanyService } from './company/admin-company.service';
import { AdminCompanyTreeComponent } from './company/tree/admin-company-tree.component';
import { AdminCompanyTabsComponent } from './company/tabs/admin-company-home-tabs.component';
import { AdminCompanyTabUsersComponent } from './company/tabs/admin-company-home-tab-users.component';


@NgModule({

    imports: [
        CommonModule,
        ShareModule,
        SxMaterialModule,
        AdminRoutingModule,
    ],
    declarations: [
        routedComponents,
        // --- Dialogs
        AdminCompanyEditDialog,
        AdminCompanyProductsEditDialog,
        UtilityBarComponent,
        // ---Company
        AdminCompanyTreeComponent,
        AdminCompanyTabsComponent,
        AdminCompanyTabUsersComponent,
    ],
    entryComponents: [
        AdminCompanyEditDialog,
        AdminCompanyProductsEditDialog,
    ],
    providers: [
        CompanyService,
        CompanyListDataSource,
        UserListDataSource,
        AdminCompanyService,
    ]
})

export class AdminModule { }

import { Injectable } from '@angular/core';
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
import { CompanyType } from 'src/app/@core/api/company/enums';
import { CompanyModel, CompanyProductModel } from 'src/app/@core/api/company/dto';
import { SxApiCompanyClient } from 'src/app/@core/api/company/api-client';
import { AppStore } from '../@core/const/app-local-storage.const';
import { Observable, Subject } from 'rxjs';
import { UserModel } from '../@core/auth/api/dto';


@Injectable({
    providedIn: 'root',
})
export class AdminService {

    /**
     *  Fields
     */


    constructor(
        private cus: CurrentUserService,
        private companyClient: SxApiCompanyClient) {
    }



    //#region Company

    selCompanyChanged = new Subject<CompanyModel>();
    companyProducts: Array<CompanyProductModel>;

    get selCompany(): CompanyModel {
        const onjStr = localStorage.getItem(AppStore.SelCompany);
        let obj: CompanyModel;
        if (onjStr) {
            obj = Object.assign(new CompanyModel(), JSON.parse(onjStr));
        }
        return obj;
    }

    set selCompany(value: CompanyModel) {
        const oldValue = this.selCompany;
        if (value) {
            localStorage.setItem(AppStore.SelCompany, JSON.stringify(value));
        } else {
            localStorage.removeItem(AppStore.SelCompany);
        }

        this.selCompanyChanged.next(value);
    }

    //#region Company Products

    companyProductsChanged = new Subject<Array<CompanyProductModel>>();

    get selCompanyProducts(): Array<CompanyProductModel> {
        const onjStr = localStorage.getItem(AppStore.SelCompanyProducts);
        let obj: Array<CompanyProductModel>;
        if (onjStr) {
            obj = Object.assign(new Array<CompanyProductModel>(), JSON.parse(onjStr));
        }
        return obj;
    }

    set selCompanyProducts(value: Array<CompanyProductModel>) {
        const oldValue = this.selCompany;
        if (value) {
            localStorage.setItem(AppStore.SelCompanyProducts, JSON.stringify(value));
        } else {
            localStorage.removeItem(AppStore.SelCompanyProducts);
        }

        this.companyProductsChanged.next(value);
    }

    loadCompanyProducts(): Observable<Array<CompanyProductModel>> {

        return new Observable<Array<CompanyProductModel>>(subscriber => {

            this.companyClient.companyProductList(this.selCompany.company_id)
                .subscribe((resp: Array<CompanyProductModel>) => {
                    console.log('loadCompanyProducts -> data:', resp);
                    this.selCompanyProducts = resp;
                    subscriber.next(this.selCompanyProducts);
                });
        });
    }

    //#endregion

    

    //#endregion
}

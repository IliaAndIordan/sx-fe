import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
// Component
import { AppRoutes } from '../@core/const/app-routes.const';
import { HomeComponent } from './home.component';
import { AuthGuard } from '../@core/guards/auth.guard';
import { AdminComponent } from './admin.component';
import { AdminDashboardComponent } from './dashboard/admin-dashboard.component';
import { CompanyListComponent } from './company-list/company-list.component';
import { UserListComponent } from './user-list/user-list.component';
import { AdminCompanyHomeComponent } from './company/admin-company-home.component';

const routes: Routes = [
  {
    path: AppRoutes.Root, component: AdminComponent,
    children: [
      {
        path: AppRoutes.Root, component: HomeComponent,
        children: [
          { path: AppRoutes.Root, pathMatch: 'full', redirectTo: AppRoutes.Dashboard },
          { path: AppRoutes.Dashboard, component: AdminDashboardComponent },
          { path: AppRoutes.CompanyList, component: CompanyListComponent },
          { path: AppRoutes.UsersList, component: UserListComponent },
          { path: AppRoutes.Company, component: AdminCompanyHomeComponent },
          /*
         { path: AppRoutes.Company, component: AdminCompanyHomeComponent,
         children: [
           { path: AppRoutes.Root,  component: CompanyDashboardComponent },
           { path: AppRoutes.Dashboard, component: CompanyDashboardComponent },
           { path: 'users', component: MyCompanyUsersComponent },
         ]},

         { path: 'type', component: CompanyTypeComponent },
         { path: 'create', component: CompanyCreateComponent },
         // { path: AppRoutes.Dashboard, component: DashboardComponent },
         */
        ]
      },
      // { path: 'copper-pricing', component: CopperPricingComponent }
    ],
  },
  // { path: 'companycreate', redirectTo: 'type', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminRoutingModule { }

export const routedComponents = [
  AdminComponent,
  HomeComponent,
  AdminDashboardComponent,
  CompanyListComponent,
  UserListComponent,
  AdminCompanyHomeComponent,
];

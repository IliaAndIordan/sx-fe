import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PreloadSelectedModulesList } from './@core/guards/preload-strategy';
import { AppRoutes } from './@core/const/app-routes.const';
import { AuthGuard } from './@core/guards/auth.guard';
import { AdminGuard } from './@core/guards/admin.guard';
import { DistributorGuard } from './@core/guards/distributor.guard';
import { MfrGuard } from './@core/guards/mfr.guard';


const routes: Routes = [
  { path: AppRoutes.Root, pathMatch: 'full', redirectTo: AppRoutes.guest },
  {
    path: AppRoutes.guest,
    loadChildren: () => import('./guest/guest.module').then(m => m.GuestModule), data: { preload: false }
  },
  {
    path: AppRoutes.Company,
    loadChildren: () => import('./company/company.module').then(m => m.CompanyModule),
    data: { preload: false },
    canActivate: [AuthGuard],
  },
  {
    path: AppRoutes.Distributor,
    loadChildren: () => import('./distributor/distributor.module').then(m => m.DistributorModule),
    data: { preload: false },
    canActivate: [AuthGuard, DistributorGuard],
  },
  {
    path: AppRoutes.Admin,
    loadChildren: () => import('./admin/admin.module').then(m => m.AdminModule),
    data: { preload: false },
    canActivate: [AuthGuard, AdminGuard],
  },
  {
    path: AppRoutes.Projects,
    loadChildren: () => import('./projects/projects.module').then(m => m.SxProjectsModule),
    data: { preload: false },
    canActivate: [AuthGuard],
  },
  {
    path: AppRoutes.Manufacturer,
    loadChildren: () => import('./manufacturer/mfr.module').then(m => m.MfrModule),
    data: { preload: false },
    canActivate: [AuthGuard, MfrGuard],
  },
  {
    path: AppRoutes.Commodity,
    loadChildren: () => import('./commodity/commodity.module').then(m => m.CommodityModule),
    data: { preload: false },
    canActivate: [AuthGuard],
  },
  { path: 'companycreate', redirectTo: AppRoutes.Company + '/type', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,
    { preloadingStrategy: PreloadSelectedModulesList, paramsInheritanceStrategy: 'always', onSameUrlNavigation: 'reload' })],
  exports: [RouterModule],
  providers: [
    PreloadSelectedModulesList
  ]
})

export class AppRoutingModule { }

export const routableComponents = [];

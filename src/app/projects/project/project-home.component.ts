import { Component, OnInit, OnDestroy, ViewChild, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
// ---Services
import { ToastrService } from 'ngx-toastr';
import { PreloaderService } from 'src/app/@core/services/preloader.service';
import { MatDialog } from '@angular/material/dialog';
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
import { SxProjectsService } from '../projects.service';
// ---Models
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { COMMON_IMG_AVATAR } from 'src/app/@core/const/app-local-storage.const';
import { SxProjectEditDialog } from 'src/app/@share/dialogs/project-edit/project-edit.dialog';
import { SxProjectModel, SxUseCaseModel } from 'src/app/@core/api/project/dto';
import { Animate } from 'src/app/@core/const/animation.const';
import { SxSidePanelModalComponent } from 'src/app/@share/layout/admin/side-panel-modal/side-panel-modal';
import { SxProjectDrawerComponent } from './drawer/project-drawer.component';
import { SxUseCaseEditDialog } from 'src/app/@share/dialogs/usecase-edit/usecase-edit.dialog';

const STORE_KEY_LEFT_PANEL_IN = 'project-home-left-panel-in';

@Component({
    templateUrl: './project-home.component.html',
})
export class SxProjectHomeComponent implements OnInit, OnDestroy, OnChanges {
    /**
     * Binding
     */


    @ViewChild('spmProject') spmProject: SxSidePanelModalComponent;
    @ViewChild('drawerProject') drawerProject: SxProjectDrawerComponent;


    /**
     * Fields
     */
    project: SxProjectModel;
    projectChanged: Subscription;
    title = 'Project';
    subtitle: string;
    panelInVar = Animate.in;


    get panelIn(): boolean {
        let rv = true;
        const valStr = localStorage.getItem(STORE_KEY_LEFT_PANEL_IN);
        if (valStr) {
            rv = JSON.parse(valStr) as boolean;
        }
        this.panelInVar = rv ? Animate.out : Animate.in;
        // console.log('panelIn-> hps:', this.hps);
        return rv;
    }

    set panelIn(value: boolean) {
        localStorage.setItem(STORE_KEY_LEFT_PANEL_IN, JSON.stringify(value));
        this.panelInVar = this.panelIn ? Animate.out : Animate.in;
    }

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastrService,
        private preloader: PreloaderService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private pService: SxProjectsService) {

    }



    ngOnInit(): void {
        this.preloader.show();
        this.project = this.cus.selProject;

        this.projectChanged = this.cus.selProjectChanged.subscribe((project: SxProjectModel) => {
            this.initFields();
        });
        // this.drawerProject.spmProjectOpen.subscribe((prj: SxProjectModel) => { this.spmProjectOpen(prj); });
        this.initFields();
    }

    ngOnDestroy(): void {
        if (this.projectChanged) { this.projectChanged.unsubscribe(); }

    }

    initFields() {
        this.project = this.cus.selProject;
        this.subtitle = this.project ? this.project.project_name : undefined;

        this.preloader.hide();
    }


    ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
        console.log('ngOnChanges-> changes', changes);
        /*
        if (changes['project']) {
            this.project = changes['project'].currentValue;
            this.initFields();
        }
        */
    }


    //#region Tree Panel

    public closePanelLeftClick(): void {
        this.panelIn = false;
    }

    public expandPanelLeftClick(): void {
        // this.treeComponent.openPanelClick();
        this.panelIn = true;
    }

    //#endregion

    //#region Side Info Actions

    // tslint:disable-next-line: member-ordering
    spmProjectObj: SxProjectModel;

    spmProjectClose(): void {
        this.spmProject.closePanel();
    }

    spmProjectOpen(project: SxProjectModel) {
        this.spmProjectObj = project;
        console.log('spmProjectOpen -> spmProjectObj=', this.spmProjectObj);
        if (this.spmProjectObj) {
            this.spmProject.expandPanel();
        }

    }

    //#endregion

    //#region Actions


    gotoProjectList() {
        if (this.cus.isAdmin) {
            this.router.navigate([AppRoutes.Projects, AppRoutes.ProjectList]);
        }
    }

    editProject(data: SxProjectModel) {
        console.log('editProject-> data:', data);
        const dialogRef = this.dialogService.open(SxProjectEditDialog, {
            width: '721px',
            height: '520px',
            data: {
                project: data,
                company: this.pService.selCompany
            }
        });

        dialogRef.afterClosed().subscribe(res => {
            console.log('editProject-> res:', res);
            if (res) {
                // this.table.renderRows();
            }
            this.initFields();
        });
    }

    editUseCase(data: SxUseCaseModel) {
        console.log('editUseCase-> data:', data);
        if (this.pService.selProject) {

            const dialogRef = this.dialogService.open(SxUseCaseEditDialog, {
                width: '721px',
                height: '520px',
                data: {
                    project: this.pService.selProject,
                    usecase: data
                }
            });

            dialogRef.afterClosed().subscribe(res => {
                console.log('editUseCase-> res:', res);
                if (res) {
                    this.cus.selProjectChanged.next(this.cus.selProject);
                }
                this.initFields();
            });
        }
    }


    //#endregion

}

import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';

// ---Services
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
import { SxApiProjectClient } from 'src/app/@core/api/project/api-client';
// ---Models
import { CompanyModel, CompanyProductModel } from 'src/app/@core/api/company/dto';
import { SxProjectModel } from '../../@core/api/project/dto';
import { AppStore } from 'src/app/@core/const/app-local-storage.const';

const SEL_TABIDX_KEY='project-selected-tab-idx';
@Injectable({
    providedIn: 'root',
})
export class SxProjectService {

    /**
     *  Fields
     */


    constructor(
        private cus: CurrentUserService,
        private pClient: SxApiProjectClient) {
    }


    //#region Project

    get selProject(): SxProjectModel {
        return this.cus.selProject;
    }

    set selProject(value: SxProjectModel) {
        this.cus.selProject = value;
    }

    //#endregion

    //#region Board Tab Index

    selTabIdxChanged = new BehaviorSubject<number>(undefined);

    get selTabIdx(): number {
        let rv = 0;
        const dataStr = localStorage.getItem(SEL_TABIDX_KEY);
        // console.log('selFolderId-> dataStr', dataStr);
        if (dataStr) {
            try {
                rv = parseInt(dataStr, 10);
            }
            catch{
                localStorage.removeItem(SEL_TABIDX_KEY);
                rv = 1;
            }

        }
        // console.log('selTabIdx-> rv', rv);
        return rv;
    }

    set selTabIdx(value: number) {
        // console.log('selTabIdx->', value);
        const oldValue = this.selTabIdx;

        localStorage.setItem(SEL_TABIDX_KEY, JSON.stringify(value));
        if (oldValue !== value) {
            this.selTabIdxChanged.next(value);
        }
    }

    //#endregion
}

import { DataSource } from '@angular/cdk/table';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
// ---Services
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
import { SxApiProjectClient } from 'src/app/@core/api/project/api-client';
import { SxProjectService } from '../../project.service';
// ---Models
import { CollectionViewer } from '@angular/cdk/collections';
import {  ResponseSxUseCaseTable, SxProjectModel, SxUseCaseModel } from 'src/app/@core/api/project/dto';


export class SxProjectTableUseCaseCriteria {
    public project_id: number;
    public limit: number;
    public offset: number;
    public filter?: string;
    public sortCol: string;
    public sortDesc: boolean;

    get pageIndex(): number {
        let rv = 0;
        rv = this.offset ? (Math.floor(this.offset / (this.limit ? this.limit : 25))) : 0;
        return rv;
    }

    set pageIndex(value: number) {
        this.offset = value ? (value) * (this.limit ? this.limit : 25) : 0;
    }

}
export const KEY_CRITERIA = 'project_table_usecase_criteria';
@Injectable()
export class SxProjectTableUseCaseDataSource extends DataSource<SxUseCaseModel> {

    selProject: SxProjectModel;

    private _data: Array<SxUseCaseModel>;
    private _isLoading = false;

    numberOfPages = 0;
    itemsCount = 0;

    dataChange: BehaviorSubject<SxUseCaseModel[]> = new BehaviorSubject<SxUseCaseModel[]>([]);
    listSubject = new BehaviorSubject<SxUseCaseModel[]>([]);
    private loadingSubject = new BehaviorSubject<boolean>(false);
    loading$ = this.loadingSubject.asObservable();

    get isLoading(): boolean {
        return this._isLoading;
    }

    set isLoading(value: boolean) {
        this._isLoading = value;
        this.loadingSubject.next(this._isLoading);
    }

    get data(): Array<SxUseCaseModel> {
        return this._data;
    }

    set data(value: Array<SxUseCaseModel>) {
        this._data = value;
        this.listSubject.next(this._data as SxUseCaseModel[]);
    }

    constructor(
        private cus: CurrentUserService,
        private client: SxApiProjectClient,
        private pService: SxProjectService) {
        super();
    }

    //#region criteria

    public criteriaChanged = new Subject<SxProjectTableUseCaseCriteria>();

    public get criteria(): SxProjectTableUseCaseCriteria {
        const onjStr = localStorage.getItem(KEY_CRITERIA);
        let obj: SxProjectTableUseCaseCriteria;
        if (onjStr) {
            obj = Object.assign(new SxProjectTableUseCaseCriteria(), JSON.parse(onjStr));
        } else {
            obj = new SxProjectTableUseCaseCriteria();
            obj.limit = 25;
            obj.project_id = this.pService.selProject.project_id;
            obj.offset = 0;
            obj.sortCol = 'usecase_order';
            obj.sortDesc = false;
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        }
        return obj;
    }

    public set criteria(obj: SxProjectTableUseCaseCriteria) {
        if (obj) {
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        } else {
            localStorage.removeItem(KEY_CRITERIA);
        }
        this.criteriaChanged.next(obj);
    }

    //#endregion

    //#region DataSource Interface Methods

    /**
     * Connect this data source to the table. The table will only update when
     * the returned stream emits new items.
     * @returns A stream of the items to be rendered.
     */
    connect(collectionViewer: CollectionViewer): Observable<SxUseCaseModel[]> {
        // return this.userListSubject.asObservable();
        return this.listSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        // this.userListSubject.complete();
        // this.loadingSubject.complete();
    }


    loadData(): Observable<Array<SxUseCaseModel>> {
        console.log('loadData->');
        console.log('loadData-> criteria=', this.criteria);

        this.isLoading = true;
        const limit = this.criteria.limit;
        const offset = this.criteria.offset;
        const projectId  = this.criteria.project_id?this.criteria.project_id:this.pService.selProject.project_id;
        const orderCol = this.criteria.sortCol;
        const isDesc = this.criteria.sortDesc; // sortDirection && sortDirection === 'asc' ? false : true;
        return new Observable<Array<SxUseCaseModel>>(subscriber => {

            this.client.usecaseTable(limit, offset, orderCol, isDesc, projectId, this.criteria.filter)
                .subscribe((res: ResponseSxUseCaseTable) => {
                    const resp = Object.assign(new ResponseSxUseCaseTable(), res);
                    console.log('loadData-> resp=', resp);
                    this.data = new Array<SxUseCaseModel>();

                    if (resp && resp.success) {
                        if (resp && resp.data &&
                            resp.data.usecase_table && resp.data.usecase_table.length > 0) {
                            resp.data.usecase_table.forEach(iu => {
                                const obj = SxUseCaseModel.fromJSON(iu);
                                this.data.push(obj);
                            });
                        }

                        this.itemsCount = resp.data.totals[0].total_rows;
                        this.listSubject.next(this.data);
                        subscriber.next(this.data);
                    }
                    console.log('loadData-> data=', this.data);
                    console.log('loadData-> itemsCount=', this.itemsCount);
                    this.isLoading = false;
                }, msg => {
                    console.log('loadData -> ' + msg);

                    this.itemsCount = 0;
                    this.data = new Array<SxUseCaseModel>();
                    this.isLoading = false;

                    // component.errorMessage = msg;
                });

        });

    }
    //#endregion
}

import { Component, EventEmitter, Input, NgZone, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { SxProjectModel } from 'src/app/@core/api/project/dto';
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
import { SxProjectService } from '../project.service';

@Component({
    selector: 'sx-project-drawer',
    templateUrl: './project-drawer.component.html',
})

export class SxProjectDrawerComponent implements OnInit {

    /** Binding */
    @Output() closePanel: EventEmitter<any> = new EventEmitter<any>();
    @Output() spmProjectOpen: EventEmitter<SxProjectModel> = new EventEmitter<SxProjectModel>();
    @Input() panelVar: string;

    /** FIELDS */
    project: SxProjectModel;
    projectChanged: Subscription;


    constructor(
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private pService: SxProjectService,
        private zone: NgZone) {
    }


    ngOnInit() {
        this.project = this.cus.selProject;

        this.projectChanged = this.cus.selProjectChanged.subscribe((project: SxProjectModel) => {
            this.initFields();
        });
        this.initFields();
    }

    initFields() {
        this.project = this.cus.selProject;
    }

    public openPanelClick(): void {
        // this.panelVar = Animate.out;
    }

    closePanelClick(): void {
        // this.panelVar = Animate.in;
        this.closePanel.emit();
    }

    //#region Action Methods


    spmProjectOpenClick(): void {
        if (this.project) {
            console.log('spmProjectOpenClick -> project=', this.project);
            /*
            this.zone.run(() => {   // <= USE
                this.spmProjectOpen.emit(this.project);
            });
            */
            this.spmProjectOpen.emit(this.project);
        }
    }

    refreshProject() {
        console.log('refreshProject->');
    }

    //#endregion

}

import { Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { SxProjectModel, SxUseCaseModel } from 'src/app/@core/api/project/dto';
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
import { PreloaderService } from 'src/app/@core/services/preloader.service';
import { SxProjectService } from '../project.service';

@Component({
    selector: 'sx-project-home-tabs',
    templateUrl: './project-home-tabs.component.html',
})
export class SxProjectHomeTabsComponent implements OnInit, OnDestroy {
    /**
     * Binding
     */
    @Output() spmProjectOpen: EventEmitter<SxProjectModel> = new EventEmitter<SxProjectModel>();
    @Output() editUseCase: EventEmitter<SxUseCaseModel> = new EventEmitter<SxUseCaseModel>();
    /**
     * Fields
     */
    project: SxProjectModel;
    projectChanged: Subscription;

    selTabIdx: number;
    selTabIdxChanged: Subscription;


    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastrService,
        private preloader: PreloaderService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private pService: SxProjectService) {

    }


    ngOnInit(): void {
        this.preloader.show();
        this.project = this.cus.selProject;

        this.projectChanged = this.cus.selProjectChanged.subscribe((project: SxProjectModel) => {
            this.initFields();
        });

        this.selTabIdxChanged = this.pService.selTabIdxChanged.subscribe(tabIdx => {
            // console.log('packageCahnged -> pkg', pkg);
            this.initFields();
        });

        this.initFields();
    }

    ngOnDestroy(): void {
        if (this.projectChanged) { this.projectChanged.unsubscribe(); }
        if (this.selTabIdxChanged) { this.selTabIdxChanged.unsubscribe(); }

    }

    initFields() {
        this.selTabIdx = this.pService.selTabIdx;
        this.project = this.cus.selProject;


        this.preloader.hide();
    }

    //#region  Tab Panel Actions

    selectedTabChanged(tabIdx: number) {
        // console.log('selectedTabChanged -> tabIdx=', tabIdx);
        const oldTab = this.selTabIdx;
        this.selTabIdx = tabIdx;
        this.pService.selTabIdx = this.selTabIdx;
        // console.log('ngOnInit -> rootFolder ', this.rootFolder);
    }


    //#endregion

    //#region Action Methods


    spmProjectOpenClick() {

        console.log('spmProjectOpen -> spmProjectObj=', this.project);
        if (this.project) {
            this.spmProjectOpen.emit(this.project);
        }

    }

    //#endregion

}

import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
// ---Services
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
import { PreloaderService } from 'src/app/@core/services/preloader.service';
import { SxProjectService } from '../project.service';
// ---Models
import { SxProjectModel, SxUseCaseModel } from 'src/app/@core/api/project/dto';
import { PageViewType } from 'src/app/@core/const/app-routes.const';
import { Animate } from 'src/app/@core/const/animation.const';
import { SxProjectTableUseCaseDataSource } from '../tables/usecase/project-table-usecase.datasource';

export const PAGE_VIEW_TYPE = 'sx_project_usecase_pageview';

@Component({
    selector: 'sx-project-home-tab-usecase',
    templateUrl: './project-home-tab-usecase.component.html',
})
export class SxProjectHomeTabUseCaseComponent implements OnInit, OnDestroy {
    /**
     * Binding
     */
    @Output() spmProjectOpen: EventEmitter<SxProjectModel> = new EventEmitter<SxProjectModel>();
    @Output() editUseCase: EventEmitter<SxUseCaseModel> = new EventEmitter<SxUseCaseModel>();
    @Input() tabIdx: number;
    /**
     * Fields
     */
    project: SxProjectModel;
    projectChanged: Subscription;


    selTabIdx: number;
    selTabIdxChanged: Subscription;

    totalCount: number;
    pvtOpt = PageViewType;
    showCardVar: string = Animate.show;
    showListVar: string = Animate.hide;
    showTableVar: string = Animate.hide;



    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastrService,
        private preloader: PreloaderService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        public tableds: SxProjectTableUseCaseDataSource,
        private pService: SxProjectService) {

    }


    ngOnInit(): void {
        this.preloader.show();
        this.totalCount = 0;
        this.project = this.cus.selProject;

        this.projectChanged = this.cus.selProjectChanged.subscribe((project: SxProjectModel) => {
            this.initFields();
        });

        this.selTabIdxChanged = this.pService.selTabIdxChanged.subscribe(tabIdx => {
            // console.log('packageCahnged -> pkg', pkg);
            this.initFields();
        });

        this.initFields();
    }

    ngOnDestroy(): void {
        if (this.projectChanged) { this.projectChanged.unsubscribe(); }
        if (this.selTabIdxChanged) { this.selTabIdxChanged.unsubscribe(); }

    }

    initFields() {
        this.selTabIdx = this.pService.selTabIdx;
        this.project = this.cus.selProject;


        this.preloader.hide();
    }

    //#region  Page View 

    get pageViewType(): string {
        const settings = localStorage.getItem(PAGE_VIEW_TYPE);
        let rv = PageViewType.Card;
        if (settings) {
            rv = settings;
        } else{
            this.pageViewType = rv;
        }
        return rv;
    }

    set pageViewType(value: string) {
        if (value) {
            localStorage.setItem(PAGE_VIEW_TYPE, value);
        }
    }

    // Toggle how items are being displayed list or card
    public toggleCardView(): void {
        this.showCardVar = Animate.show;
        this.showListVar = Animate.hide;
        this.showTableVar = Animate.hide;
        this.pageViewType = PageViewType.Card;
    }

    public toggleListView(): void {
        this.showCardVar = Animate.hide;
        this.showListVar = Animate.show;
        this.showTableVar = Animate.hide;
        this.pageViewType = PageViewType.List;
    }

    public toggleTableView(): void {
        console.log('toggleTableView -> ');
        this.showCardVar = Animate.hide;
        this.showListVar = Animate.hide;
        this.showTableVar = Animate.show;
        this.pageViewType = PageViewType.Table;
    }

    //#endregion

    //#region Action Methods
    createUseCaseClick(){
        console.log('createUseCaseClick -> project=', this.project);
    }

    spmProjectOpenClick() {

        console.log('spmProjectOpen -> spmProjectObj=', this.project);
        if (this.project) {
            this.spmProjectOpen.emit(this.project);
        }

    }

    //#endregion

}
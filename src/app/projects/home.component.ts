import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
// --- Services
// --- Animations
import { Animate } from '../@core/const/animation.const';
// --- Models
import { AppRoutes } from '../@core/const/app-routes.const';
import { COMMON_IMG_LOGO_RED } from '../@core/const/app-local-storage.const';
import { PageTransition, ShowHideTriggerBlock, ShowHideTriggerFlex, SpinExpandIconTrigger } from 'src/app/@core/const/animations-triggers';
import { MatDialog } from '@angular/material/dialog';
import { CurrentUserService } from '../@core/auth/current-user.service';
import { AppSettings } from '../@core/models/app/settings';
import { Subscription } from 'rxjs';
import { throwIfAlreadyLoaded } from '../@core/guards/module-import-guard';
import { MatSidenav, MatSidenavContent } from '@angular/material/sidenav';
import { SxProjectModel } from '../@core/api/project/dto';

@Component({
    templateUrl: './home.component.html',
    animations: [PageTransition,
        ShowHideTriggerBlock,
        ShowHideTriggerFlex,
        SpinExpandIconTrigger]
})
export class HomeComponent implements OnInit, OnDestroy {
    /**
     * Binding
     */
    @ViewChild('sidenav', { static: true }) sidenav: MatSidenav;
    @ViewChild('content', { static: true }) content: MatSidenavContent;
   
    /**
     * Fields
     */
    options: AppSettings;
    optionsChanged: Subscription;

    private layoutChanges: Subscription;

    private isMobileScreen = false;
    get isOver(): boolean {
        return this.isMobileScreen;
    }

    private contentWidthFix = true;
    private collapsedWidthFix = true;

    sxLogo = COMMON_IMG_LOGO_RED;
    // --- Side Tab
    expandTabVar: string = Animate.in;
    showTabContentsVar: string = Animate.hide;
    total: number = 0;
    aggressiveTotal: number = 0;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastrService,
        public dialogService: MatDialog,
        private cus: CurrentUserService) { }


    ngOnInit(): void {

        this.optionsChanged = this.cus.optionsChanged
            .subscribe(opt => {
                this.initFields();
            });

        this.initFields();
    }

    ngOnDestroy(): void {
        // console.log('HomeComponent:ngOnDestroy()->');
    }

    initFields() {
        this.options = this.cus.options;
    }

    

    //#region Layout Actions

    toggleCollapsed() {
        this.options.sidenavCollapsed = !this.options.sidenavCollapsed;
        this.resetCollapsedState();
    }

    resetCollapsedState(timer = 400) {
        // TODO: Trigger when transition end
        setTimeout(() => {
            this.cus.setNavState('collapsed', this.options.sidenavCollapsed);
        }, timer);
    }

    sidenavCloseStart() {
        this.contentWidthFix = false;
    }

    sidenavOpenedChange(isOpened: boolean) {
        this.cus.setNavState('opened', isOpened);

        this.collapsedWidthFix = !this.isOver;
        this.resetCollapsedState();
    }

    // Demo purposes only
    receiveOptions(options: AppSettings): void {
        this.options = options;
        this.setTheme(options);
        this.setBodyDir(options);
    }
    setTheme(options: AppSettings) {
        /*
      if (options.theme === 'dark') {
        this.overlay.getContainerElement().classList.add('theme-dark');
      } else {
        this.overlay.getContainerElement().classList.remove('theme-dark');
      }
      */
    }

    setBodyDir(options: AppSettings) {
        if (options.dir === 'rtl') {
            document.body.dir = 'rtl';
        } else {
            document.body.dir = 'ltr';
        }
    }

    //#endregion

}

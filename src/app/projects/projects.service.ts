import { Injectable } from '@angular/core';
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
import { CompanyModel, CompanyProductModel } from 'src/app/@core/api/company/dto';
import { SxApiCompanyClient } from 'src/app/@core/api/company/api-client';
import { AppStore } from '../@core/const/app-local-storage.const';
import { Observable, Subject } from 'rxjs';
import { SxApiProjectClient } from '../@core/api/project/api-client';
import { SxProjectModel } from '../@core/api/project/dto';

@Injectable({
    providedIn: 'root',
})
export class SxProjectsService {

    /**
     *  Fields
     */


    constructor(
        private cus: CurrentUserService,
        private pClient: SxApiProjectClient) {
    }

    //#region Company

    selCompanyChanged = new Subject<CompanyModel>();

    get selCompany(): CompanyModel {
        const onjStr = localStorage.getItem(AppStore.SelCompany);
        let obj: CompanyModel;
        if (onjStr) {
            obj = Object.assign(new CompanyModel(), JSON.parse(onjStr));
        }
        return obj;
    }

    set selCompany(value: CompanyModel) {
        const oldValue = this.selCompany;
        if (value) {
            localStorage.setItem(AppStore.SelCompany, JSON.stringify(value));
        } else {
            localStorage.removeItem(AppStore.SelCompany);
        }

        this.selCompanyChanged.next(value);
    }

    //#region Company Products

    companyProductsChanged = new Subject<Array<CompanyProductModel>>();

    get selCompanyProducts(): Array<CompanyProductModel> {
        const onjStr = localStorage.getItem(AppStore.SelCompanyProducts);
        let obj: Array<CompanyProductModel>;
        if (onjStr) {
            obj = Object.assign(new Array<CompanyProductModel>(), JSON.parse(onjStr));
        }
        return obj;
    }

    set selCompanyProducts(value: Array<CompanyProductModel>) {
        const oldValue = this.selCompany;
        if (value) {
            localStorage.setItem(AppStore.SelCompanyProducts, JSON.stringify(value));
        } else {
            localStorage.removeItem(AppStore.SelCompanyProducts);
        }

        this.companyProductsChanged.next(value);
    }

    //#endregion

    //#endregion

    //#region Project

    get selProject(): SxProjectModel {
        return this.cus.selProject;
    }

    set selProject(value: SxProjectModel) {
        this.cus.selProject = value;
    }

    //#endregion
}

import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { BehaviorSubject, Observable, merge, Subscription } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
// ---
import { PreloaderService } from 'src/app/@core/services/preloader.service';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
import { SxApiCompanyClient } from 'src/app/@core/api/company/api-client';
// ---
import { CompanyModel, CompanyProductModel } from 'src/app/@core/api/company/dto';
import { UserModel } from 'src/app/@core/auth/api/dto';
import { UserRole, UserRoleDisplayPipe } from 'src/app/@core/auth/api/enums';
import { PAGE_SIZE_OPTIONS, COMMON_IMG_AVATAR } from 'src/app/@core/const/app-local-storage.const';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { SxProjectModel } from 'src/app/@core/api/project/dto';
import { SxProjectsService } from '../projects.service';
import { SxProjectEditDialog } from 'src/app/@share/dialogs/project-edit/project-edit.dialog';
import { SxProjectsTableCriteria, SxProjectsTableDataSource } from './projects-list.datasource';

@Component({
    templateUrl: './projects-list.component.html',
})
export class SxProjectsListComponent implements OnInit, OnDestroy, AfterViewInit {
    /**
     * Binding
     */
    // @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatTable, { static: true }) table: MatTable<SxProjectModel>; // initialize
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;


    /**
     * Fields
     */
    company: CompanyModel;
    criteria: SxProjectsTableCriteria;
    criteriaChanged: Subscription;
    imageAvatarUrl = COMMON_IMG_AVATAR;

    pageSizeOpt = PAGE_SIZE_OPTIONS;
    pageIndex = 0;
    pageSize = 12;
    sortActive: string;
    sortDirection: string;

    dataCount = 0;
    dataChanged: Subscription;
    displayedColumns = ['action', 'project_id', 'project_name', 'project_account_id', 'pstatus_id', 'manager_id', 'estimator_id', 'adate', 'notes'];
    //, 'company_id', 'estimator_id', 'estimator_name' , 'udate'
    canEdit: boolean;
    companyChanged: Subscription;

    constructor(
        private router: Router,
        private toastr: ToastrService,
        private preloader: PreloaderService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private pService: SxProjectsService,
        public tableds: SxProjectsTableDataSource,
        private cdref: ChangeDetectorRef) {

    }


    ngOnInit(): void {
        // console.log('user:', this.cus.user);
        if (!this.pService.selCompany) {
            this.pService.selCompany = this.cus.company;
        }

        this.canEdit = true;
        this.dataChanged = this.tableds.listSubject.subscribe((users: Array<SxProjectModel>) => {
            // console.log('MyCompanyUsersComponent:userListSubject()->', users);
            this.dataCount = this.tableds.itemsCount;
            this.initFields();
        });
        this.criteriaChanged = this.tableds.criteriaChanged.subscribe((criteria: SxProjectsTableCriteria) => {
            this.criteria = this.tableds.criteria;
            // console.log('MyCompanyUsersComponent:criteriaChanged()->', criteria);
            // this.paginator._changePageSize(this.paginator.pageSize);
            this.initFields();
            this.loadPage();
        });

        this.companyChanged = this.pService.selCompanyChanged.subscribe((company: CompanyModel) => {
            // console.log('companyProductsChanged ->', companyProducts);
            if (this.pService.selCompany) {
                this.criteria.company_id = this.pService.selCompany.company_id;
                this.tableds.criteria = this.criteria;
            }
        });

        this.initFields();
        this.loadPage();
    }

    ngOnDestroy(): void {
        // console.log('HomeComponent:ngOnDestroy()->');
        if (this.dataChanged) { this.dataChanged.unsubscribe(); }
        if (this.criteriaChanged) { this.criteriaChanged.unsubscribe(); }
    }

    initFields() {
        this.company = this.cus.company;
        this.criteria = this.tableds.criteria;
        this.pageIndex = this.criteria.pageIndex;
        this.pageSize = this.criteria.limit;

        this.sortActive = this.criteria.sortCol;
        this.sortDirection = this.criteria.sortDesc ? 'desc' : 'asc';
        if(this.criteria.company_id !== this.company.company_id){
            this.criteria.company_id = this.company.company_id;
            this.tableds.criteria = this.criteria;
        }
        // console.log('initFields-> criteria:', this.criteria);
        const tq = '?m=' + new Date().getTime();


    }


    ngAfterViewInit() {

        this.sort.sortChange.subscribe(() => {
            this.criteria.pageIndex = 0;
            this.criteria.sortCol = this.sort.active;
            this.criteria.sortDesc = this.sort.direction !== 'asc';
            this.tableds.criteria = this.criteria;
        });
        this.paginator.page
            .pipe(
                tap(() => {
                    this.criteria.pageIndex = this.paginator.pageIndex;
                    this.criteria.limit = this.paginator.pageSize;
                    this.tableds.criteria = this.criteria;
                })
            )
            .subscribe();
    }

    //#region Table events

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.criteria.filter = filterValue.trim();
        this.tableds.criteria = this.criteria;
    }

    loadPage() {
        this.tableds.loadData()
            .subscribe(res => {
                this.initFields();
            });
    }

    //#endregion

    //#region Actions

    editProject(data: SxProjectModel) {
        console.log('editCompany-> company:', data);
        const dialogRef = this.dialogService.open(SxProjectEditDialog, {
            width: '721px',
            height: '520px',
            data: {
                project: data,
                company: this.pService.selCompany
            }
        });

        dialogRef.afterClosed().subscribe(res => {
            console.log('editProject-> res:', res);
            if (res) {
                this.table.renderRows();
            }
            this.initFields();
        });
    }

    gotoProject(data: SxProjectModel) {
        console.log('gotoProject-> project:', data);
        if (data) {
            this.pService.selProject = data;
            this.router.navigate([AppRoutes.Projects, AppRoutes.Project]);
        }
    }

    //#endregion
}


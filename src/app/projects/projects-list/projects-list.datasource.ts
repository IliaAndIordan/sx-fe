import { DataSource } from '@angular/cdk/table';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
// ---Services
import { SxApiCompanyClient } from 'src/app/@core/api/company/api-client';
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
// ---Models
import { UserModel, RespCompanyUsers } from 'src/app/@core/auth/api/dto';
import { CollectionViewer } from '@angular/cdk/collections';
import { CompanyModel, RespCompanyList } from 'src/app/@core/api/company/dto';
import { ResponseSxProjectTable, SxProjectModel } from 'src/app/@core/api/project/dto';
import { SxApiProjectClient } from 'src/app/@core/api/project/api-client';
import { SxProjectsService } from '../projects.service';

export class SxProjectsTableCriteria {
    public company_id: number;
    public limit: number;
    public offset: number;
    public filter?: string;
    public sortCol: string;
    public sortDesc: boolean;

    get pageIndex(): number {
        let rv = 0;
        rv = this.offset ? (Math.floor(this.offset / (this.limit ? this.limit : 25))) : 0;
        return rv;
    }

    set pageIndex(value: number) {
        this.offset = value ? (value) * (this.limit ? this.limit : 25) : 0;
    }

}
export const KEY_CRITERIA = 'projects_list_criteria';
@Injectable()
export class SxProjectsTableDataSource extends DataSource<SxProjectModel> {

    selProject: SxProjectModel;

    private _data: Array<SxProjectModel>;
    private _isLoading = false;

    numberOfPages = 0;
    itemsCount = 0;

    dataChange: BehaviorSubject<SxProjectModel[]> = new BehaviorSubject<SxProjectModel[]>([]);
    listSubject = new BehaviorSubject<SxProjectModel[]>([]);
    private loadingSubject = new BehaviorSubject<boolean>(false);
    loading$ = this.loadingSubject.asObservable();

    get isLoading(): boolean {
        return this._isLoading;
    }

    set isLoading(value: boolean) {
        this._isLoading = value;
        this.loadingSubject.next(this._isLoading);
    }

    get data(): Array<SxProjectModel> {
        return this._data;
    }

    set data(value: Array<SxProjectModel>) {
        this._data = value;
        this.listSubject.next(this._data as SxProjectModel[]);
    }

    constructor(
        private cus: CurrentUserService,
        private client: SxApiProjectClient,
        private pService: SxProjectsService) {
        super();
    }

    //#region criteria

    public criteriaChanged = new Subject<SxProjectsTableCriteria>();

    public get criteria(): SxProjectsTableCriteria {
        const onjStr = localStorage.getItem(KEY_CRITERIA);
        let obj: SxProjectsTableCriteria;
        if (onjStr) {
            obj = Object.assign(new SxProjectsTableCriteria(), JSON.parse(onjStr));
        } else {
            obj = new SxProjectsTableCriteria();
            obj.limit = 25;
            obj.company_id = this.pService.selCompany.company_id;
            obj.offset = 0;
            obj.sortCol = 'project_name';
            obj.sortDesc = false;
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        }
        return obj;
    }

    public set criteria(obj: SxProjectsTableCriteria) {
        if (obj) {
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        } else {
            localStorage.removeItem(KEY_CRITERIA);
        }
        this.criteriaChanged.next(obj);
    }

    //#endregion

    //#region DataSource Interface Methods

    /**
     * Connect this data source to the table. The table will only update when
     * the returned stream emits new items.
     * @returns A stream of the items to be rendered.
     */
    connect(collectionViewer: CollectionViewer): Observable<SxProjectModel[]> {
        // return this.userListSubject.asObservable();
        return this.listSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        // this.userListSubject.complete();
        // this.loadingSubject.complete();
    }


    loadData(): Observable<Array<SxProjectModel>> {
        console.log('loadData->');
        console.log('loadData-> criteria=', this.criteria);

        this.isLoading = true;
        const limit = this.criteria.limit;
        const offset = this.criteria.offset;
        const companyId = this.criteria.company_id ? this.criteria.company_id : this.pService.selCompany.company_id;
        const orderCol = this.criteria.sortCol;
        const isDesc = this.criteria.sortDesc; // sortDirection && sortDirection === 'asc' ? false : true;
        return new Observable<Array<SxProjectModel>>(subscriber => {

            this.client.projectTable(limit, offset, orderCol, isDesc, companyId, this.criteria.filter)
                .subscribe((res: ResponseSxProjectTable) => {
                    const resp = Object.assign(new ResponseSxProjectTable(), res);
                    console.log('loadData-> resp=', resp);
                    this.data = new Array<SxProjectModel>();

                    if (resp && resp.success) {
                        if (resp && resp.data &&
                            resp.data.project_list && resp.data.project_list.length > 0) {
                            resp.data.project_list.forEach(iu => {
                                const obj = SxProjectModel.fromJSON(iu);
                                this.data.push(obj);
                            });
                        }

                        this.itemsCount = resp.data.totals[0].total_rows;
                        this.listSubject.next(this.data);
                        subscriber.next(this.data);
                    }
                    console.log('loadData-> data=', this.data);
                    console.log('loadData-> itemsCount=', this.itemsCount);
                    this.isLoading = false;
                }, msg => {
                    console.log('loadData -> ' + msg);

                    this.itemsCount = 0;
                    this.data = new Array<SxProjectModel>();
                    this.isLoading = false;

                    // component.errorMessage = msg;
                });

        });

    }
    //#endregion
}

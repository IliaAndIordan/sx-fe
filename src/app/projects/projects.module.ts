import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// --- Modules
import { ShareModule } from '../@share/share.module';
import { SxMaterialModule } from '../@share/material.module';
import { routedComponents, SxProjectRoutingModule } from './projects-routing.module';
import { SxProjectsService } from './projects.service';
import { SxProjectUtilityBarComponent } from './utility-bar/utility-bar.component';
import { SxProjectsTableDataSource } from './projects-list/projects-list.datasource';
import { SxProjectsListComponent } from './projects-list/projects-list.component';
import { SxProjectEditDialog } from '../@share/dialogs/project-edit/project-edit.dialog';
import { SxProjectDrawerComponent } from './project/drawer/project-drawer.component';
import { SxProjectService } from './project/project.service';
import { SxProjectHomeTabsComponent } from './project/tabs/project-home-tabs.component';
import { SxProjectHomeTabUseCaseComponent } from './project/tabs/project-home-tab-usecase.component';
import { SxProjectTableUseCaseDataSource } from './project/tables/usecase/project-table-usecase.datasource';
import { SxProjectsTableUseCaseComponent } from './project/tables/usecase/project-table-usecase.component';


@NgModule({

    imports: [
        CommonModule,
        ShareModule,
        SxMaterialModule,
        SxProjectRoutingModule,
    ],
    declarations: [
        routedComponents,
        // --- Components
        SxProjectUtilityBarComponent,
        SxProjectDrawerComponent,
        // ---Tabs
        SxProjectHomeTabsComponent,
        SxProjectHomeTabUseCaseComponent,
        // ---Tables
        SxProjectsTableUseCaseComponent,
        // --- Dialogs
    ],
    entryComponents: [
        SxProjectEditDialog,
    ],
    providers: [
        SxProjectsService,
        SxProjectService,
        // ---Table DS
        SxProjectsTableDataSource,
        SxProjectTableUseCaseDataSource,
    ]
})

export class SxProjectsModule { }

import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
// Component
import { AppRoutes } from '../@core/const/app-routes.const';
import { HomeComponent } from './home.component';
import { AuthGuard } from '../@core/guards/auth.guard';
import { SxProjectsComponent } from './projects.component';
import { SxProjectsDashboardComponent } from './dashboard/projects-dashboard.component';
import { SxProjectsListComponent } from './projects-list/projects-list.component';
import { SxProjectHomeComponent } from './project/project-home.component';

const routes: Routes = [
  {
    path: AppRoutes.Root, component: SxProjectsComponent,
    children: [
      {
        path: AppRoutes.Root, component: HomeComponent,
        children: [
          { path: AppRoutes.Root, pathMatch: 'full', redirectTo: AppRoutes.Dashboard },
          { path: AppRoutes.Dashboard, component: SxProjectsDashboardComponent },
          { path: AppRoutes.ProjectList, component: SxProjectsListComponent, },
          { path: AppRoutes.Project, component: SxProjectHomeComponent },
          /*
          { path: AppRoutes.Root, component: MyCompanyComponent,
          children: [
            { path: AppRoutes.Root,  component: CompanyDashboardComponent },
            { path: AppRoutes.Dashboard, component: CompanyDashboardComponent },
            { path: 'users', component: MyCompanyUsersComponent },
          ]},
          
          { path: 'type', component: CompanyTypeComponent },
          { path: 'create', component: CompanyCreateComponent },
          // { path: AppRoutes.Dashboard, component: DashboardComponent },
          */
        ]
      },
      // { path: 'copper-pricing', component: CopperPricingComponent }
    ],
  },
  // { path: 'companycreate', redirectTo: 'type', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SxProjectRoutingModule { }

export const routedComponents = [
  SxProjectsComponent,
  HomeComponent,
  SxProjectsDashboardComponent,
  SxProjectsListComponent,
  SxProjectHomeComponent,
];

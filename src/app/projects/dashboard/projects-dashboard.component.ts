import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { PreloaderService } from 'src/app/@core/services/preloader.service';
import { MatDialog } from '@angular/material/dialog';
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { CompanyModel } from 'src/app/@core/api/company/dto';
import { COMMON_IMG_AVATAR } from 'src/app/@core/const/app-local-storage.const';
import { SxProjectEditDialog } from 'src/app/@share/dialogs/project-edit/project-edit.dialog';

@Component({
    templateUrl: './projects-dashboard.component.html',
})
export class SxProjectsDashboardComponent implements OnInit, OnDestroy {
    /**
     * Binding
     */


    /**
     * Fields
     */
    companyImage = require('../../../assets/images/common/mfr-graphic-white.png');

    company: CompanyModel;
    title = 'Company';
    companyType: number;
    logoUrl: string;

    companiesData: any;
    companiesCount: number;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastrService,
        private preloader: PreloaderService,
        public dialogService: MatDialog,
        private cus: CurrentUserService) {

    }


    ngOnInit(): void {
        this.preloader.show();
        // console.log('user:', this.cus.user);
        if (!this.cus.user.company_id && !this.cus.isAdmin) {
            // this.router.navigate([AppRoutes.Company, 'create']);
        }
        this.initFields();
    }

    ngOnDestroy(): void {
        // console.log('HomeComponent:ngOnDestroy()->');

    }

    initFields() {
        this.company = this.cus.company;
        // console.log('initFields-> company:', this.company);
        this.companyType = this.company ? this.company.company_type_id : undefined;
        this.title = this.company ? this.company.company_name : 'Company';
        this.logoUrl = this.company && this.company.logo_url ? this.company.logo_url
            : this.company && this.company.sxLogoUrl ? this.company.sxLogoUrl : COMMON_IMG_AVATAR;
        this.preloader.hide();
    }


    //#region Actions

    projectCreate() {
        const dialogRef = this.dialogService.open(SxProjectEditDialog, {
            width: '721px',
            height: '520px',
            data: {
                project: undefined,
                company: this.company
            }
        });

        dialogRef.afterClosed().subscribe(res => {
            console.log('SxProjectEditDialog closed res:', res);
            if (res) {

            }
            this.initFields();
        });
    }

    gotoProjectManagement() {
        if (this.cus.isAdmin) {
            this.router.navigate([AppRoutes.Projects, AppRoutes.ProjectList]);
        }
    }

    gotoUsersManagement() {
        if (this.cus.isAdmin) {
            this.router.navigate([AppRoutes.Admin, AppRoutes.UsersList]);
        }
    }

    //#endregion

}

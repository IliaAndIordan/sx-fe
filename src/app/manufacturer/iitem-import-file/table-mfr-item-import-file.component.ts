import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild, ChangeDetectorRef, Output, EventEmitter } from '@angular/core';
import { BehaviorSubject, Observable, merge, Subscription } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
// ---
import { PreloaderService } from 'src/app/@core/services/preloader.service';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
import { SxApiCompanyClient } from 'src/app/@core/api/company/api-client';
// ---
import { CompanyModel, CompanyProductModel } from 'src/app/@core/api/company/dto';
import { UserModel } from 'src/app/@core/auth/api/dto';
import { UserRole, UserRoleDisplayPipe } from 'src/app/@core/auth/api/enums';
import { PAGE_SIZE_OPTIONS, COMMON_IMG_AVATAR } from 'src/app/@core/const/app-local-storage.const';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { SxProjectModel, SxUseCaseModel } from 'src/app/@core/api/project/dto';
import { SxProjectEditDialog } from 'src/app/@share/dialogs/project-edit/project-edit.dialog';
import { FileImportModel, MfrItemImportFileTableCriteria } from 'src/app/@core/api/mfr/item/dto';
import { MfrItemImportFileTableDataSource } from './mfr-item-import-file-table-usecase.datasource';
import { MfrService } from '../mfr.service';

@Component({
    selector: 'sx-table-mfr-item-import-file-usecase',
    templateUrl: './table-mfr-item-import-file.component.html',
})
export class SxTableMfrItemImportFileComponent implements OnInit, OnDestroy, AfterViewInit {
    /**
     * Binding
     */
    @Output() createUseCase: EventEmitter<any> = new EventEmitter<any>();
    @Output() editUseCase: EventEmitter<FileImportModel> = new EventEmitter<FileImportModel>();
    // @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatTable, { static: true }) table: MatTable<SxProjectModel>; // initialize
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;


    /**
     * Fields
     */
    company: CompanyModel;
    criteria: MfrItemImportFileTableCriteria;
    criteriaChanged: Subscription;
    imageAvatarUrl = COMMON_IMG_AVATAR;

    pageSizeOpt = PAGE_SIZE_OPTIONS;
    pageIndex = 0;
    pageSize = 25;
    sortActive: string;
    sortDirection: string;

    dataCount = 0;
    dataChanged: Subscription;
    displayedColumns = ['action', 'file_import_id', 'file_name', 'adate', 'processing_status_id',
        'file_size', 'file_data_fields', 'file_rows', 'udate'];

    canEdit: boolean;

    constructor(
        private router: Router,
        private toastr: ToastrService,
        private preloader: PreloaderService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private mfrService: MfrService,
        public tableds: MfrItemImportFileTableDataSource,
        private cdref: ChangeDetectorRef) {

    }


    ngOnInit(): void {
        // console.log('user:', this.cus.user);

        this.canEdit = true;
        this.dataChanged = this.tableds.listSubject.subscribe((item: Array<FileImportModel>) => {
            // console.log('MyCompanyUsersComponent:userListSubject()->', users);
            this.dataCount = this.tableds.itemsCount;
            this.initFields();
        });
        this.criteriaChanged = this.tableds.criteriaChanged.subscribe((criteria: MfrItemImportFileTableCriteria) => {
            this.criteria = this.tableds.criteria;
            // console.log('MyCompanyUsersComponent:criteriaChanged()->', criteria);
            // this.paginator._changePageSize(this.paginator.pageSize);
            this.initFields();
            this.loadPage();
        });

        this.initFields();
        this.loadPage();
    }

    ngOnDestroy(): void {
        // console.log('HomeComponent:ngOnDestroy()->');
        if (this.dataChanged) { this.dataChanged.unsubscribe(); }
        if (this.criteriaChanged) { this.criteriaChanged.unsubscribe(); }
    }

    initFields() {
        this.company = this.cus.company;
        this.criteria = this.tableds.criteria;
        this.pageIndex = this.criteria.pageIndex;
        this.pageSize = this.criteria.limit;

        this.sortActive = this.criteria.sortCol;
        this.sortDirection = this.criteria.sortDesc ? 'desc' : 'asc';

        // console.log('initFields-> criteria:', this.criteria);
        const tq = '?m=' + new Date().getTime();


    }


    ngAfterViewInit() {

        this.sort.sortChange.subscribe(() => {
            this.criteria.pageIndex = 0;
            this.criteria.sortCol = this.sort.active;
            this.criteria.sortDesc = this.sort.direction !== 'asc';
            this.tableds.criteria = this.criteria;
        });
        this.paginator.page
            .pipe(
                tap(() => {
                    this.criteria.pageIndex = this.paginator.pageIndex;
                    this.criteria.limit = this.paginator.pageSize;
                    this.tableds.criteria = this.criteria;
                })
            )
            .subscribe();
    }

    //#region Table events

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.criteria.filter = filterValue.trim();
        this.tableds.criteria = this.criteria;
    }

    loadPage() {
        this.tableds.loadData()
            .subscribe(res => {
                this.initFields();
            });
    }

    //#endregion

    //#region Actions

    createUseCaseClick() {
        this.createUseCase.emit('emit!');
    }


    editItemClick(data: FileImportModel) {
        console.log('editItemClick-> data:', data);
        this.editUseCase.emit(data);
    }


    gotoFileHome(data: FileImportModel) {
        console.log('gotoFileHome-> file:', data);
        if (data) {
            this.mfrService.selItemImportFile = data;
            this.router.navigate([AppRoutes.Manufacturer, AppRoutes.ItemImportFile]);
        }
    }

    refreshClick() {
        this.loadPage();
    }

    //#endregion
}


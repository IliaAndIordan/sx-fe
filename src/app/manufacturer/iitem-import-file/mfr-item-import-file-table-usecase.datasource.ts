import { DataSource } from '@angular/cdk/table';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
// ---Services
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
// ---Models
import { CollectionViewer } from '@angular/cdk/collections';
import { FileImportModel, MfrItemImportFileTableCriteria, ResponseMfrItemImportFileTable } from 'src/app/@core/api/mfr/item/dto';
import { MfrService } from '../mfr.service';
import { MfrItemApiClient } from 'src/app/@core/api/mfr/item/api-client';



export const KEY_CRITERIA = 'mfr-item-import-file-table-criteria';
@Injectable()
export class MfrItemImportFileTableDataSource extends DataSource<FileImportModel> {

    private _data: Array<FileImportModel>;
    private _isLoading = false;

    numberOfPages = 0;
    itemsCount = 0;

    dataChange: BehaviorSubject<FileImportModel[]> = new BehaviorSubject<FileImportModel[]>([]);
    listSubject = new BehaviorSubject<FileImportModel[]>([]);
    private loadingSubject = new BehaviorSubject<boolean>(false);
    loading$ = this.loadingSubject.asObservable();

    get isLoading(): boolean {
        return this._isLoading;
    }

    set isLoading(value: boolean) {
        this._isLoading = value;
        this.loadingSubject.next(this._isLoading);
    }

    get data(): Array<FileImportModel> {
        return this._data;
    }

    set data(value: Array<FileImportModel>) {
        this._data = value;
        this.listSubject.next(this._data as FileImportModel[]);
    }

    constructor(
        private cus: CurrentUserService,
        private client: MfrItemApiClient,
        private mfrService: MfrService) {
        super();
    }

    //#region criteria

    public criteriaChanged = new Subject<MfrItemImportFileTableCriteria>();

    public get criteria(): MfrItemImportFileTableCriteria {
        const objStr = localStorage.getItem(KEY_CRITERIA);
        let obj: MfrItemImportFileTableCriteria;
        if (objStr) {
            obj = Object.assign(new MfrItemImportFileTableCriteria(), JSON.parse(objStr));
        } else {
            obj = new MfrItemImportFileTableCriteria();
            obj.limit = 25;
            obj.company_id = this.mfrService.selCompany.company_id;
            obj.offset = 0;
            obj.sortCol = 'file_import_id';
            obj.sortDesc = true;
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        }
        return obj;
    }

    public set criteria(obj: MfrItemImportFileTableCriteria) {
        if (obj) {
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        } else {
            localStorage.removeItem(KEY_CRITERIA);
        }
        this.criteriaChanged.next(obj);
    }

    //#endregion

    //#region DataSource Interface Methods

    /**
     * Connect this data source to the table. The table will only update when
     * the returned stream emits new items.
     * @returns A stream of the items to be rendered.
     */
    connect(collectionViewer: CollectionViewer): Observable<FileImportModel[]> {
        // return this.userListSubject.asObservable();
        return this.listSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        // this.userListSubject.complete();
        // this.loadingSubject.complete();
    }


    loadData(): Observable<Array<FileImportModel>> {
        console.log('loadData->');
        console.log('loadData-> criteria=', this.criteria);

        this.isLoading = true;
        const limit = this.criteria.limit;
        const offset = this.criteria.offset;
        const companyId = this.criteria.company_id ? this.criteria.company_id : this.mfrService.selCompany.company_id;
        const orderCol = this.criteria.sortCol;
        const isDesc = this.criteria.sortDesc; // sortDirection && sortDirection === 'asc' ? false : true;
        return new Observable<Array<FileImportModel>>(subscriber => {

            this.client.mfrItemsImportFileTable(limit, offset, orderCol, isDesc, companyId, this.criteria.filter)
                .subscribe((res: ResponseMfrItemImportFileTable) => {
                    const resp = Object.assign(new ResponseMfrItemImportFileTable(), res);
                    console.log('loadData-> resp=', resp);
                    this.data = new Array<FileImportModel>();

                    if (resp && resp.success) {
                        if (resp && resp.data &&
                            resp.data.file_list && resp.data.file_list.length > 0) {
                            resp.data.file_list.forEach(iu => {
                                const obj = FileImportModel.fromJSON(iu);
                                this.data.push(obj);
                            });
                        }

                        this.itemsCount = resp.data.totals[0].total_rows;
                        this.listSubject.next(this.data);
                        subscriber.next(this.data);
                    }
                    console.log('loadData-> data=', this.data);
                    console.log('loadData-> itemsCount=', this.itemsCount);
                    this.isLoading = false;
                }, msg => {
                    console.log('loadData -> ' + msg);

                    this.itemsCount = 0;
                    this.data = new Array<FileImportModel>();
                    this.isLoading = false;

                    // component.errorMessage = msg;
                });

        });

    }
    //#endregion
}

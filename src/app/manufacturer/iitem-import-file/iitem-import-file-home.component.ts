import { Component, OnInit, OnDestroy, ViewChild, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
// ---Services
import { ToastrService } from 'ngx-toastr';
import { PreloaderService } from 'src/app/@core/services/preloader.service';
import { MatDialog } from '@angular/material/dialog';
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
// ---Models
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { COMMON_IMG_AVATAR } from 'src/app/@core/const/app-local-storage.const';
import { SxProjectEditDialog } from 'src/app/@share/dialogs/project-edit/project-edit.dialog';
import { SxProjectModel, SxUseCaseModel } from 'src/app/@core/api/project/dto';
import { Animate } from 'src/app/@core/const/animation.const';
import { SxSidePanelModalComponent } from 'src/app/@share/layout/admin/side-panel-modal/side-panel-modal';
import { SxUseCaseEditDialog } from 'src/app/@share/dialogs/usecase-edit/usecase-edit.dialog';
import { CompanyModel } from 'src/app/@core/api/company/dto';
import { MfrService } from '../mfr.service';
import { FileImportModel } from 'src/app/@core/api/mfr/item/dto';

const STORE_KEY_LEFT_PANEL_IN = 'mfr-iitem-import-file-home-left-panel-in';

@Component({
    templateUrl: './iitem-import-file-home.component.html',
})
export class MfrItemImportFileHomeComponent implements OnInit, OnDestroy, OnChanges {
    /**
     * Binding
     */
    @ViewChild('spmCompany') spmCompany: SxSidePanelModalComponent;
    @ViewChild('spmProject') spmProject: SxSidePanelModalComponent;


    /**
     * Fields
     */
    company: CompanyModel;
    companyChanged: Subscription;
    file: FileImportModel;
    fileChanged: Subscription;
    title = 'File';
    subtitle: string;
    panelInVar = Animate.in;


    get panelIn(): boolean {
        let rv = true;
        const valStr = localStorage.getItem(STORE_KEY_LEFT_PANEL_IN);
        if (valStr) {
            rv = JSON.parse(valStr) as boolean;
        }
        this.panelInVar = rv ? Animate.out : Animate.in;
        // console.log('panelIn-> hps:', this.hps);
        return rv;
    }

    set panelIn(value: boolean) {
        localStorage.setItem(STORE_KEY_LEFT_PANEL_IN, JSON.stringify(value));
        this.panelInVar = this.panelIn ? Animate.out : Animate.in;
    }

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastrService,
        private preloader: PreloaderService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private mfrService: MfrService) {

    }



    ngOnInit(): void {
        this.preloader.show();
        this.company = this.mfrService.selCompany;
        this.file = this.mfrService.selItemImportFile;

        this.companyChanged = this.cus.companyChanged.subscribe((company: CompanyModel) => {
            this.initFields();
        });

        this.fileChanged = this.mfrService.selItemImportFileChanged.subscribe((file: FileImportModel) => {
            this.initFields();
        });
        this.initFields();
    }

    ngOnDestroy(): void {
        if (this.companyChanged) { this.companyChanged.unsubscribe(); }
        if (this.fileChanged) { this.fileChanged.unsubscribe(); }
    }

    initFields() {
        this.company = this.mfrService.selCompany;
        this.file = this.mfrService.selItemImportFile;
        this.subtitle = this.file ? this.file.file_name : undefined;

        this.preloader.hide();
    }


    ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
        console.log('ngOnChanges-> changes', changes);
        /*
        if (changes['project']) {
            this.project = changes['project'].currentValue;
            this.initFields();
        }
        */
    }


    //#region Tree Panel

    public closePanelLeftClick(): void {
        this.panelIn = false;
    }

    public expandPanelLeftClick(): void {
        // this.treeComponent.openPanelClick();
        this.panelIn = true;
    }

    //#endregion

    //#region Side Info Actions

    // tslint:disable-next-line: member-ordering
    project: SxProjectModel;
    // tslint:disable-next-line: member-ordering
    spmCompanyObj: CompanyModel;

    spmProjectClose(): void {
        this.spmProject.closePanel();
    }

    spmProjectOpen(project: SxProjectModel) {
        this.project = project;
        console.log('spmProjectOpen -> project=', this.project);
        if (this.project) {
            this.spmProject.expandPanel();
        }

    }

    spmCompanyClose(): void {
        this.spmCompany.closePanel();
    }


    spmCompanyOpen(com: CompanyModel) {
        this.spmCompanyObj = com;
        console.log('spmCompanyOpen -> spmCompanyObj=', this.spmCompanyObj);
        if (this.spmCompanyObj) {
            this.spmCompany.expandPanel();
        }
    }

    //#endregion

    //#region Actions


    gotoProjectList() {
        if (this.cus.isAdmin) {
            this.router.navigate([AppRoutes.Projects, AppRoutes.ProjectList]);
        }
    }

    editProject(data: SxProjectModel) {
        console.log('editProject-> data:', data);
        /*
        const dialogRef = this.dialogService.open(SxProjectEditDialog, {
            width: '721px',
            height: '520px',
            data: {
                project: data,
                company: this.aService.company
            }
        });

        dialogRef.afterClosed().subscribe(res => {
            console.log('editProject-> res:', res);
            if (res) {
                this.cus.companyChanged.next(this.aService.company);
            }
            this.initFields();
        });
        */
    }


    //#endregion

}

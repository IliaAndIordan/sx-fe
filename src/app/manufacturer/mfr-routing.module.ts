import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
// Component
import { AppRoutes } from '../@core/const/app-routes.const';
import { HomeComponent } from './home.component';
import { AuthGuard } from '../@core/guards/auth.guard';
import { MfrComponent } from './mfr.component';
import { MfrDashboardComponent } from './dashboard/mfr-dashboard.component';
import { MfrItemImportComponent } from './item-import/item-import.component';
import { MfrItemImportResultComponent } from './item-import/item-import-result.component';
import { SxTableMfrItemImportFileComponent } from './iitem-import-file/table-mfr-item-import-file.component';
import { MfrItemImportFileHomeComponent } from './iitem-import-file/iitem-import-file-home.component';

const routes: Routes = [
  {
    path: AppRoutes.Root, component: MfrComponent,
    children: [
      {
        path: AppRoutes.Root, component: HomeComponent,
        children: [
          { path: AppRoutes.Root, pathMatch: 'full', redirectTo: AppRoutes.Dashboard },
          { path: AppRoutes.Dashboard, component: MfrDashboardComponent },
          { path: AppRoutes.ItemsImport, component: MfrItemImportComponent, },
          { path: AppRoutes.ItemsImportResult, component: MfrItemImportResultComponent },
          { path: AppRoutes.ItemImportFiles, component: SxTableMfrItemImportFileComponent },
          { path: AppRoutes.ItemImportFile, component: MfrItemImportFileHomeComponent },
        /*
          { path: AppRoutes.Root, component: MyCompanyComponent,
          children: [
            { path: AppRoutes.Root,  component: CompanyDashboardComponent },
            { path: AppRoutes.Dashboard, component: CompanyDashboardComponent },
            { path: 'users', component: MyCompanyUsersComponent },
          ]},

          { path: 'type', component: CompanyTypeComponent },
          { path: 'create', component: CompanyCreateComponent },
          // { path: AppRoutes.Dashboard, component: DashboardComponent },
          */
        ]
      },
      // { path: 'copper-pricing', component: CopperPricingComponent }
    ],
  },
  // { path: 'companycreate', redirectTo: 'type', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MfrRoutingModule { }

export const routedComponents = [
  MfrComponent,
  HomeComponent,
  MfrDashboardComponent,
  MfrItemImportComponent,
  MfrItemImportResultComponent,
  SxTableMfrItemImportFileComponent,
  MfrItemImportFileHomeComponent,
];

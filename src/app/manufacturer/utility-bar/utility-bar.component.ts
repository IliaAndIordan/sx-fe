import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { PreloaderService } from 'src/app/@core/services/preloader.service';
import { MatDialog } from '@angular/material/dialog';
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
import { ItemImportFileSelectDialog } from 'src/app/@share/dialogs/item-file-select/item-import-file-select.dialog';
import { MfrItemImportService } from 'src/app/@core/services/mfr-item-import-service';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { CompanyType } from 'src/app/@core/api/company/enums';

@Component({
    selector: 'sx-mfr-utility-bar',
    templateUrl: './utility-bar.component.html',
})
export class MfrUtilityBarComponent implements OnInit, OnDestroy {
    /**
     * Binding
     */


    /**
     * Fields
     */

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastrService,
        private preloader: PreloaderService,
        public dialogService: MatDialog,
        private mfrIService: MfrItemImportService,
        private cus: CurrentUserService) {

    }


    ngOnInit(): void {

        // console.log('user:', this.cus.user);
        if (!this.cus.user.company_id && !this.cus.isAdmin) {
            // this.router.navigate([AppRoutes.Company, 'create']);
        }
        this.initFields();
    }

    ngOnDestroy(): void {
        // console.log('HomeComponent:ngOnDestroy()->');

    }

    initFields() {
    }


    //#region Actions

    ItemImportFileSelect(): void {
        const dialogRef = this.dialogService.open(ItemImportFileSelectDialog, {
            width: '721px',
            height: '320px', // 520px
            data: { company: this.cus.company }
        });

        dialogRef.afterClosed().subscribe((res: any) => {
            console.log('afterClosed -> res:', res);
            if (res) {
                if(this.cus.company && this.cus.company.company_type_id === CompanyType.Manufacturer){
                    this.mfrIService.selCompany = this.cus.company;
                    this.mfrIService.rawItemData = res;
                    this.router.navigate([AppRoutes.Root, AppRoutes.Manufacturer, AppRoutes.ItemsImport]);
                }
            }
            else{
                this.initFields();
            }
        });
    }

    //#endregion

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// --- Modules
import { ShareModule } from '../@share/share.module';
import { SxMaterialModule } from '../@share/material.module';
import { routedComponents, MfrRoutingModule } from './mfr-routing.module';
import { MfrService } from './mfr.service';
import { MfrUtilityBarComponent } from './utility-bar/utility-bar.component';
import { PriceFileUploadDialog } from '../company/dialogs/price-file-upload/price-file-upload.dialog';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MfrItemImportFileTableDataSource } from './iitem-import-file/mfr-item-import-file-table-usecase.datasource';
import { MfrItemImportFileTreeComponent } from './iitem-import-file/tree/iitem-import-file-tree.component';



@NgModule({

    imports: [
        CommonModule,
        DragDropModule,
        ShareModule,
        SxMaterialModule,
        MfrRoutingModule,
    ],
    declarations: [
        // --- Components
        MfrUtilityBarComponent,

        routedComponents,
        // ---Tree
        MfrItemImportFileTreeComponent,
        // ---Tabs

        // ---Tables

        // --- Dialogs
    ],
    exports: [
        // --- Components
        MfrUtilityBarComponent,
    ],
    entryComponents: [
        PriceFileUploadDialog,
    ],
    providers: [
        MfrService,
        MfrItemImportFileTableDataSource,
        // ---Table DS
    ]
})

export class MfrModule { }

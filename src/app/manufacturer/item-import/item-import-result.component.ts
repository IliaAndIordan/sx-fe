import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
// ---Services
import { ToastrService } from 'ngx-toastr';
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
import { PreloaderService } from 'src/app/@core/services/preloader.service';
import { MfrItemImportService } from '../../@core/services/mfr-item-import-service';
// ---Models
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { COMMON_IMG_AVATAR } from 'src/app/@core/const/app-local-storage.const';
import { CompanyModel } from 'src/app/@core/api/company/dto';
import { DataField, ItemImportColumnModel, MfrItemColumnsOpt } from 'src/app/@core/models/common/item-import-column.model';
import { ShowHideTriggerBlock } from 'src/app/@core/const/animations-triggers';
import { FileImportModel, MfrItemImportModel, ResponseMfrItemImport, ResponseMfrItemImportFileCreate } from 'src/app/@core/api/mfr/item/dto';
import { CompanyType } from 'src/app/@core/api/company/enums';

const CHUNK_SIZE = 50;
@Component({
    templateUrl: './item-import-result.component.html',
    animations: [ShowHideTriggerBlock]
})
export class MfrItemImportResultComponent implements OnInit, OnDestroy {
    /**
     * Binding
     */


    /**
     * Fields
     */
    companyImage = require('../../../assets/images/common/mfr-graphic-white.png');

    company: CompanyModel;
    logoUrl: string;
    fileInfo: FileImportModel;
    columnMappings: Array<ItemImportColumnModel>;
    mapped: ItemImportColumnModel[];
    rawItemDataFields: DataField[];
    mfrItemImportData: Array<MfrItemImportModel>;
    affected_row_ids: Array<number>;
    isValid = true;
    totalItems: number;
    importedItems: number;
    itemsCount: number;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastrService,
        private preloader: PreloaderService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private mfrIService: MfrItemImportService) {

    }


    ngOnInit(): void {
        this.preloader.show();
        // console.log('user:', this.cus.user);
        if (!this.cus.user.company_id && !this.cus.isAdmin) {
            // this.router.navigate([AppRoutes.Company, 'create']);
        }
        this.initFields();
    }

    ngOnDestroy(): void {
        // console.log('HomeComponent:ngOnDestroy()->');

    }

    initFields() {
        this.company = this.cus.company;
        // console.log('initFields-> company:', this.company);
        this.logoUrl = this.company && this.company.logo_url ? this.company.logo_url
            : this.company && this.company.sxLogoUrl ? this.company.sxLogoUrl : COMMON_IMG_AVATAR;
        this.fileInfo = this.mfrIService.fileInfo;

        this.columnMappings = this.mfrIService.columnMappings;
        this.mapped = this.columnMappings ? this.columnMappings.filter(col => col.dataField) : undefined;

        this.rawItemDataFields = this.mfrIService.rawItemDataFields();
        this.mfrItemImportData = this.mfrIService.genMfrItemImportArray();
        console.log('initFields-> mfrItemImportData:', this.mfrItemImportData);
        this.totalItems = this.mfrItemImportData ? this.mfrItemImportData.length : 0;
        this.itemsCount = this.mfrItemImportData ? this.mfrItemImportData.length : 0;
        this.importedItems = 0;
        if (this.fileInfo) {
            this.fileInfo.company_id = this.mfrIService.selCompany.company_id;
            this.fileInfo.user_id = this.cus.user.user_id;
            this.fileInfo.file_data_fields = this.mapped ? this.mapped.length : 0;
            this.fileInfo.file_rows = this.itemsCount;
            this.mfrIService.fileInfo = this.fileInfo;
        }

        console.log('initFields-> fileInfo:', this.fileInfo);

        this.preloader.hide();
        this.verifyIsValid();
    }

    get progress(): number {
        let rv = 0;
        if (this.itemsCount) {
            rv = parseInt(((this.importedItems / this.totalItems) * 100).toFixed(), 10);
        }
        return rv;
    }

    verifyIsValid(): void {
        const required: ItemImportColumnModel[] = this.columnMappings.filter(col => col.required && !col.dataField);
        this.isValid = required.length === 0 && this.mapped.length > 0 && this.itemsCount > 0 &&
            (!this.affected_row_ids || this.affected_row_ids.length === 0);
        console.log('verifyIsValid -> isValid: ', this.isValid);
    }
    //#region Actions

    createFnPromise(chunkItems) {

        return () => this.mfrIService.importItemsPromise(chunkItems)
            .then(res => {
                return res;
            });
    }

    promiseArrayimportItems(): Array<any> {
        const rv = new Array<any>();
        if (this.mfrItemImportData && this.mfrItemImportData.length > 0) {
            const totalItems = this.mfrItemImportData.length;
            for (let idx = 0; idx < this.mfrItemImportData.length; idx += CHUNK_SIZE) {
                const chunkItems = this.mfrItemImportData.slice(idx, idx + CHUNK_SIZE);
                rv.push(this.createFnPromise(chunkItems));
            }
        }

        return rv;
    }



    importItemsClick() {
        this.importedItems = 1;
        this.isValid = false;
        this.preloader.show();

        this.mfrIService.importFileSave(this.mfrIService.fileInfo)
            .subscribe((res: ResponseMfrItemImportFileCreate) => {

                const fi: FileImportModel = this.mfrIService.fileInfo;
                console.log('importItemsClick -> fi:', fi);
                if (fi && fi.file_import_id) {
                    this.importItems().then(ires => {
                        this.preloader.hide();
                        if (this.company.company_type_id === CompanyType.Manufacturer) {
                            this.gotoManufacturer();
                        }
                        else {
                            this.gotoCompany();
                        }
                    });
                }
                else {
                    console.log('importItemsClick -> no fi:', fi);
                }

            },
                err => {
                    this.preloader.hide();
                    console.log('importItemsClick -> error: ', err);
                });



    }

    importItems(): Promise<void> {
        this.importedItems = 0;
        const promise = new Promise<void>(async (resolve, reject) => {
            if (this.itemsCount < (CHUNK_SIZE * 2)) {

                // this.preloader.show();
                this.mfrIService.importItems(this.mfrItemImportData)
                    .subscribe((res: ResponseMfrItemImport) => {
                        // this.preloader.hide();
                        if (res && res.success) {
                            this.importedItems = res.data.affected_row_ids ? res.data.affected_row_ids.length : 0;
                            this.mfrItemImportData = new Array<MfrItemImportModel>();
                            this.itemsCount = this.mfrItemImportData ? this.mfrItemImportData.length : 0;
                        }
                        resolve();
                    },
                        err => {
                            this.preloader.hide();
                            console.log('importItemsClick -> error: ', err);
                            reject(err);
                        });
            }
            else {
                console.log('importChunks -> CHUNK_SIZE: ', CHUNK_SIZE);
                this.importChunks().then(res => {
                    resolve();
                });

            }
        });
        return promise;
    }

    importChunks(): Promise<void> {
        console.log('importItems: ->');
        this.affected_row_ids = new Array<number>();
        // this.preloader.show();
        // this.spinnerService.setMessage('Matching items, please wait...');
        const promise = new Promise<void>(async (resolve, reject) => {


            if (this.mfrItemImportData && this.mfrItemImportData.length > 0) {
                this.totalItems = this.mfrItemImportData.length;
                this.importedItems = 0;
                // this.spinnerService.setMessage('Matching items, found ' + this.totalItems + ' items...');
                const promesesArr = this.promiseArrayimportItems();
                console.log('importChunks: -> promesesArr', promesesArr);

                // tslint:disable-next-line: prefer-for-of
                for (let idx=0; idx< promesesArr.length; idx++) {
                    // console.log('forkJoin: idx:', idx);
                    // const chunkRes: ResponseMfrItemImport = await promesesArr[idx](); ResponseMfrItemImport
                    const chunkRes = await promesesArr[idx]();
                    console.log('importChunks: chunkRes:', chunkRes);

                    if (chunkRes && chunkRes.data && chunkRes.data.affected_row_ids) {
                        chunkRes.data.affected_row_ids.forEach(element => {
                            this.affected_row_ids.push(element);
                        });

                    }
                    this.importedItems = this.affected_row_ids ? this.affected_row_ids.length : 0;
                }
                console.log('importChunks-> after for affected_row_ids:', this.affected_row_ids);

                resolve();


            }

        });
        return promise;
    }

    gotoItemManagement() {
        if (this.cus.isAdmin) {
            // this.router.navigate([AppRoutes.Projects, AppRoutes.ProjectList]);
        }
    }

    gotoManufacturer() {
        this.router.navigate([AppRoutes.Root, AppRoutes.Manufacturer, AppRoutes.ItemImportFiles]);
    }

    gotoCompany() {
        this.router.navigate([AppRoutes.Root, AppRoutes.Company]);
    }

    //#endregion

}

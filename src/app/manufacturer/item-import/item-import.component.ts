import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
// ---Services
import { ToastrService } from 'ngx-toastr';
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
import { PreloaderService } from 'src/app/@core/services/preloader.service';
import { MfrItemImportService } from '../../@core/services/mfr-item-import-service';
// ---Models
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { COMMON_IMG_AVATAR } from 'src/app/@core/const/app-local-storage.const';
import { CompanyModel } from 'src/app/@core/api/company/dto';
import { DataField, ItemImportColumnModel, MfrItemColumnsOpt } from 'src/app/@core/models/common/item-import-column.model';
import { CdkDrag, CdkDragDrop, CdkDropList, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { ShowHideTriggerBlock } from 'src/app/@core/const/animations-triggers';


@Component({
    templateUrl: './item-import.component.html',
    animations: [ShowHideTriggerBlock]
})
export class MfrItemImportComponent implements OnInit, OnDestroy {
    /**
     * Binding
     */
    dbColumnsOpt = MfrItemColumnsOpt;

    /**
     * Fields
     */
    companyImage = require('../../../assets/images/common/mfr-graphic-white.png');

    company: CompanyModel;
    logoUrl: string;
    rawItemDataFields: DataField[];
    columsIds: Array<string>;
    isValid = true;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastrService,
        private preloader: PreloaderService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private mfrIService: MfrItemImportService) {

    }


    ngOnInit(): void {
        this.preloader.show();
        // console.log('user:', this.cus.user);
        if (!this.cus.user.company_id && !this.cus.isAdmin) {
            // this.router.navigate([AppRoutes.Company, 'create']);
        }
        this.initFields();
    }

    ngOnDestroy(): void {
        // console.log('HomeComponent:ngOnDestroy()->');

    }

    initFields() {
        this.company = this.cus.company;
        // console.log('initFields-> company:', this.company);
        this.logoUrl = this.company && this.company.logo_url ? this.company.logo_url
            : this.company && this.company.sxLogoUrl ? this.company.sxLogoUrl : COMMON_IMG_AVATAR;
        this.columsIds = new Array<string>();
        this.dbColumnsOpt = new Array<ItemImportColumnModel>();
        MfrItemColumnsOpt.forEach(element => {
            element.dataField = undefined;
            this.dbColumnsOpt.push(element);
            this.columsIds.push(element.dbColumn);
        });
        this.rawItemDataFields = this.mfrIService.rawItemDataFields();
        this.preloader.hide();
        this.verifyIsValid();
    }

    verifyIsValid(): void {
        const required: ItemImportColumnModel[] = this.dbColumnsOpt.filter(col => col.required && !col.dataField);
        this.isValid = required.length === 0;
        console.log('verifyIsValid -> isValid: ', this.isValid);
    }
    //#region Actions

    drop(event: CdkDragDrop<any>) {
        //  console.log('drop -> event:', event);
        const df = event.item.data as DataField;
        const dbColumn = event.container.data ? event.container.data[0] : undefined;
        // console.log('drop -> dbColumn:', dbColumn);
        // console.log('drop -> previousContainer:', event.previousContainer.data);
        // console.log('drop -> container:', event.container.data);
        if (event.previousContainer === event.container) {
            // moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
        } else {
            if (df && dbColumn) {
                const fIdx = this.rawItemDataFields.findIndex(x => x.idx === df.idx);
                this.rawItemDataFields.splice(fIdx, 1);
                if (dbColumn.dataField) {
                    this.rawItemDataFields.push(dbColumn.dataField);
                }
                // this.rawItemDataFields = this.rawItemDataFields.filter(x => x.idx !== df.idx);
                dbColumn.dataField = df;
                console.log('drop -> dbColumn 2:', dbColumn);
            }
        }
        this.verifyIsValid();
    }

    deleteField(field: DataField) {
        // console.log('deleteField-> field:', field);
        if (field) {
            this.rawItemDataFields.splice(field.idx, 1);
        }
        this.verifyIsValid();
    }

    deleteDataField(column: ItemImportColumnModel) {
        console.log('deleteDataField-> column:', column);
        if (column && column.dataField) {
            this.rawItemDataFields.splice(column.dataField.idx, 0, column.dataField);
        }
        column.dataField = undefined;
        this.verifyIsValid();
    }



    /** Predicate function that only allows even numbers to be dropped into a list. */
    evenPredicate(item: CdkDrag<ItemImportColumnModel>, list: CdkDropList) {
        const dataField = list.data[0].dataField;
        const rv = !dataField ? true : false;
        return rv;
    }

    importItemsClick() {
        console.log('importItemsClick-> dbColumnsOpt:', this.dbColumnsOpt);
        this.mfrIService.columnMappings = this.dbColumnsOpt;
        const fi = this.mfrIService.fileInfo;
        fi.company_id = this.mfrIService.selCompany.company_id;
        fi.user_id = this.cus.user.user_id;
        fi.file_data_fields = this.dbColumnsOpt && this.dbColumnsOpt.length > 0 ? this.dbColumnsOpt.length : 0;
        this.router.navigate([AppRoutes.Root, AppRoutes.Manufacturer, AppRoutes.ItemsImportResult]);
    }

    gotoItemManagement() {
        if (this.cus.isAdmin) {
            // this.router.navigate([AppRoutes.Projects, AppRoutes.ProjectList]);
        }
    }

    gotoUsersManagement() {
        if (this.cus.isAdmin) {
            this.router.navigate([AppRoutes.Admin, AppRoutes.UsersList]);
        }
    }

    gotoCommodity() {
        this.router.navigate([AppRoutes.Root, AppRoutes.Commodity]);
    }

    //#endregion

}

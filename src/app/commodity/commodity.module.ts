import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// --- Modules
import { ShareModule } from '../@share/share.module';
import { SxMaterialModule } from '../@share/material.module';
import { CommodityService } from './commodity.service';
import { CommodityRoutingModule, routedComponents } from './commodity-routing.module';
import { CommodityUtilityBarComponent } from './utility-bar/utility-bar.component';



@NgModule({

    imports: [
        CommonModule,
        ShareModule,
        SxMaterialModule,
        CommodityRoutingModule,
    ],
    declarations: [
        // --- Components
        CommodityUtilityBarComponent,
        routedComponents,
        // ---Tabs

        // ---Tables

        // --- Dialogs
    ],
    exports: [
        // --- Components
    ],
    entryComponents: [

    ],
    providers: [
        CommodityService,
        // ---Table DS
    ]
})

export class CommodityModule { }

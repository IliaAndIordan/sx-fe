import { Component, OnInit, OnDestroy, Output } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { PreloaderService } from 'src/app/@core/services/preloader.service';
import { MatDialog } from '@angular/material/dialog';
// ---Services
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
import { CommodityService } from '../commodity.service';
// ---Models
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { CompanyModel } from 'src/app/@core/api/company/dto';
import { COMMON_IMG_AVATAR } from 'src/app/@core/const/app-local-storage.const';
import { Subscription } from 'rxjs';
import { CommodityListCriteria, CommodityModel } from 'src/app/@core/api/mfr/commodity/dto';


@Component({
    templateUrl: './commodity-list.component.html',
})
export class CommodityListComponent implements OnInit, OnDestroy {
    /**
     * Binding
     */


    /**
     * Fields
     */
    companyImage = require('../../../assets/images/common/mfr-graphic-white.png');

    company: CompanyModel;
    title = 'Commodity';
    companyType: number;
    logoUrl: string;

    criteria: CommodityListCriteria;
    criteriaChanged: Subscription;

    data: Array<CommodityModel>;
    dataCount: number;
    pages: number;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastrService,
        private preloader: PreloaderService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private cmdService: CommodityService) {

    }


    ngOnInit(): void {
        // console.log('user:', this.cus.user);
        if (!this.cus.user.company_id && !this.cus.isAdmin) {
            // this.router.navigate([AppRoutes.Company, 'create']);
        }
        this.criteriaChanged = this.cmdService.criteriaChanged.subscribe((criteria: CommodityListCriteria) => {
            this.criteria = this.cmdService.criteria;
            // console.log('MyCompanyUsersComponent:criteriaChanged()->', criteria);
            // this.paginator._changePageSize(this.paginator.pageSize);
            this.initFields();
            this.loadData();
        });

        this.initFields();
        this.loadData();
    }

    ngOnDestroy(): void {
        // console.log('HomeComponent:ngOnDestroy()->');
        if (this.criteriaChanged) { this.criteriaChanged.unsubscribe(); }
    }

    initFields() {
        this.company = this.cus.company;
        this.criteria = this.cmdService.criteria;
        // console.log('initFields-> company:', this.company);
        // this.title = this.company ? this.company.company_name : 'Company';


    }


    //#region Actions


    gotoItemManagement() {
        if (this.cus.isAdmin) {
            // this.router.navigate([AppRoutes.Projects, AppRoutes.ProjectList]);
        }
    }

    gotoUsersManagement() {
        if (this.cus.isAdmin) {
            this.router.navigate([AppRoutes.Admin, AppRoutes.UsersList]);
        }
    }

    //#endregion

    //#region Data

    refreshClick() {
        this.loadData();
    }

    loadChildren(cm: CommodityModel) {
        console.log('loadChildren-> cm:', cm);
        if (cm && cm.commodity_id !== this.criteria.parentId) {

            this.criteria.addPath(cm);
            this.criteria.parentId = cm.commodity_id;
            this.cmdService.criteria = this.criteria;
        }
    }


    loadData() {
        // this.spinerService.display(true);
        this.preloader.show();
        this.cmdService.commodityList()
            .subscribe((resp: Array<CommodityModel>) => {
                // this.spinerService.display(false);
                this.preloader.hide();
                // console.log('loadUserList -> resp:', resp);
                this.data = resp;
                this.iniList();
            });
    }

    iniList(): void {

        let filtered = new Array<CommodityModel>();
        const dataAll = this.cmdService.cmdList;
        this.dataCount = dataAll ? dataAll.length : 0;

        this.pages = Math.ceil(this.dataCount / this.criteria.limit);
        if (this.pages < this.criteria.pageIndex) {
            this.criteria.pageIndex = 1;
        }
        filtered = dataAll;
        // filteredUsers =  this.filterUsers(usersAll);
        let endIdx = this.criteria.offset ? (this.criteria.offset + this.criteria.limit) : this.criteria.limit;
        // console.log('initUsers -> endIdx1', endIdx);
        if (endIdx > dataAll.length) {
            endIdx = dataAll.length;
        }
        // console.log('initUsers -> endIdx', endIdx);
        // console.log('initUsers -> criteria', this.criteria);
        this.data = filtered.slice(this.criteria.offset, endIdx);
        // console.log('initUsers -> users', this.users);
        /*
        if (this.paginationComponent && this.activePage != this.paginationComponent.activePage) {
            this.paginationComponent.handlePageNumberClick(this.activePage);
        }
        */
    }

    //#endregion

}

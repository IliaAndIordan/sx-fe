import { Injectable } from '@angular/core';
import { Observable, Subject, Subscription } from 'rxjs';
// ---Services
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
import { SxApiCompanyClient } from 'src/app/@core/api/company/api-client';
import { SxApiProjectClient } from '../@core/api/project/api-client';
// ---Models
import { CompanyModel, CompanyProductModel } from 'src/app/@core/api/company/dto';
import { AppStore } from '../@core/const/app-local-storage.const';
import { SxProjectModel } from '../@core/api/project/dto';
import { CommodityApiClient } from '../@core/api/mfr/commodity/api-client';
import { CommodityListCriteria, CommodityModel, ResponseCommodityList } from '../@core/api/mfr/commodity/dto';
import { CommodityType } from '../@core/api/mfr/commodity/enums';

export const KEY_CRITERIA = 'commodity_list_criteria';

@Injectable({
    providedIn: 'root',
})
export class CommodityService {

    /**
     *  Fields
     */


    constructor(
        private cus: CurrentUserService,
        private cmdClient: CommodityApiClient) {
    }

    //#region CommodityListCriteria

    cmdList: Array<CommodityModel>;

    criteriaChanged = new Subject<CommodityListCriteria>();

    get criteria(): CommodityListCriteria {
        const onjStr = localStorage.getItem(KEY_CRITERIA);
        let obj: CommodityListCriteria;
        if (onjStr) {
            obj = Object.assign(new CommodityListCriteria(), JSON.parse(onjStr));
        }
        if(!obj){
            obj = new CommodityListCriteria();
            obj.orderCol = 'display_idx';
            obj.isDesc = false;
            obj.offset = 0;
            obj.limit = 35;
            obj.parentId = undefined;
            obj.commodityType = CommodityType.CSI2016;
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        }
        return obj;
    }

    set criteria(value: CommodityListCriteria) {
        const oldValue = this.criteria;
        if (value) {
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(value));
        } else {
            localStorage.removeItem(KEY_CRITERIA);
        }

        this.criteriaChanged.next(value);
    }

    commodityList(): Observable<Array<CommodityModel>> {
        return new Observable<Array<CommodityModel>>(subscriber => {
            this.loadCommodityList()
                .subscribe((resp: ResponseCommodityList) => {
                    const res = Object.assign(new ResponseCommodityList(), resp);
                    // console.log('companyUserList-> res=', res);
                    const rv: Array<CommodityModel> = new Array<CommodityModel>();
                    if (res && res.success) {

                        if (res && res.data &&
                            res.data.commodity_list && res.data.commodity_list.length > 0) {
                            res.data.commodity_list.forEach(iu => {
                                const obj = CommodityModel.fromJSON(iu);
                                rv.push(obj);
                            });
                        }
                    }
                    // console.log('companyUsers-> rv=', rv);
                    this.cmdList = rv;
                    subscriber.next(this.cmdList);
                });
        });

    }

    loadCommodityList(): Observable<ResponseCommodityList> {
        return new Observable<ResponseCommodityList>(subscriber => {
            this.cmdClient.commodityList(this.criteria.orderCol,
                this.criteria.isDesc,
                this.criteria.commodityType,
                this.criteria.parentId)
                .subscribe((resp: ResponseCommodityList) => {
                    // this.spinerService.display(false);
                    // console.log('loadUserList -> resp:', resp);
                    subscriber.next(resp);
                });
        });

    }

    //#endregion

    //#region Project

    get selProject(): SxProjectModel {
        return this.cus.selProject;
    }

    set selProject(value: SxProjectModel) {
        this.cus.selProject = value;
    }

    //#endregion
}

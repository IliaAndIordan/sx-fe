import { Component, OnInit, OnDestroy, Output, EventEmitter, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { PreloaderService } from 'src/app/@core/services/preloader.service';
import { MatDialog } from '@angular/material/dialog';
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
import { CommodityListCriteria, CommodityModel } from 'src/app/@core/api/mfr/commodity/dto';
import { Subscription } from 'rxjs';
import { CommodityService, KEY_CRITERIA } from '../commodity.service';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';


@Component({
    selector: 'sx-cmdty-utility-bar',
    templateUrl: './utility-bar.component.html',
    styleUrls: ['./utility-bar.component.scss']
})
export class CommodityUtilityBarComponent implements OnInit, OnDestroy {
    /**
     * Binding
     */
    @Input() dataCount: number;
    @Output() showParent: EventEmitter<number> = new EventEmitter<number>();

    /**
     * Fields
     */
    criteria: CommodityListCriteria;
    criteriaChanged: Subscription;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastrService,
        private preloader: PreloaderService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private cmdService: CommodityService) {

    }


    ngOnInit(): void {

        // console.log('user:', this.cus.user);
        if (!this.cus.user.company_id && !this.cus.isAdmin) {
            // this.router.navigate([AppRoutes.Company, 'create']);
        }
        this.criteriaChanged = this.cmdService.criteriaChanged.subscribe((criteria: CommodityListCriteria) => {
            this.criteria = this.cmdService.criteria;
            // console.log('MyCompanyUsersComponent:criteriaChanged()->', criteria);
            // this.paginator._changePageSize(this.paginator.pageSize);
            this.initFields();
        });

        this.initFields();
    }

    ngOnDestroy(): void {
        if (this.criteriaChanged) { this.criteriaChanged.unsubscribe(); }
    }

    initFields() {
        this.criteria = this.cmdService.criteria;
    }


    //#region Actions

    gotoRootCommodities() {
        localStorage.removeItem(KEY_CRITERIA);
        const dto = this.cmdService.criteria;
        this.cmdService.criteria = dto;
        this.router.navigate([AppRoutes.Root, AppRoutes.Commodity]);
    }

    gotoCommodity(cm: CommodityModel) {
        if (cm) {
            this.criteria.removePath(cm);

            this.cmdService.criteria = this.criteria;
        }
    }
    //#endregion

}

import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
// Component
import { AppRoutes } from '../@core/const/app-routes.const';
import { HomeComponent } from './home.component';
import { AuthGuard } from '../@core/guards/auth.guard';
import { CommodityComponent } from './commodity.component';
import { CommodityListComponent } from './dashboard/commodity-list.component';


const routes: Routes = [
  {
    path: AppRoutes.Root, component: CommodityComponent,
    children: [
      {
        path: AppRoutes.Root, component: HomeComponent,
        children: [
          { path: AppRoutes.Root, pathMatch: 'full', redirectTo: AppRoutes.Dashboard },
          { path: AppRoutes.Dashboard, component: CommodityListComponent },
          /*{ path: AppRoutes.ProjectList, component: SxProjectsListComponent, },
          { path: AppRoutes.Project, component: SxProjectHomeComponent },

          { path: AppRoutes.Root, component: MyCompanyComponent,
          children: [
            { path: AppRoutes.Root,  component: CompanyDashboardComponent },
            { path: AppRoutes.Dashboard, component: CompanyDashboardComponent },
            { path: 'users', component: MyCompanyUsersComponent },
          ]},

          { path: 'type', component: CompanyTypeComponent },
          { path: 'create', component: CompanyCreateComponent },
          // { path: AppRoutes.Dashboard, component: DashboardComponent },
          */
        ]
      },
      // { path: 'copper-pricing', component: CopperPricingComponent }
    ],
  },
  // { path: 'companycreate', redirectTo: 'type', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CommodityRoutingModule { }

export const routedComponents = [
  CommodityComponent,
  HomeComponent,
  CommodityListComponent,
];

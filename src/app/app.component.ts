import { Component, OnInit, AfterViewInit } from '@angular/core';
import { PreloaderService } from './@core/services/preloader.service';
import { Router, RouteConfigLoadStart, RouteConfigLoadEnd } from '@angular/router';
import { SpinnerService } from './@core/services/spinner.service';

@Component({
  selector: 'sx-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit{
  title = 'sx-fe';

  constructor(
    private preloader: PreloaderService,
    private spinner: SpinnerService,
    private router: Router,) {
      
    }

  ngOnInit() {
    this.router.events.subscribe(event => {
      if (event instanceof RouteConfigLoadStart) {
        this.spinner.display(true);
      } else if (event instanceof RouteConfigLoadEnd) {
        this.spinner.display(false);
      }
    });
  }

  ngAfterViewInit() {
      this.preloader.hide();
    // this.spinner.display(false);
  }
}

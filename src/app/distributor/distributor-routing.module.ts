import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
// Component
import { AppRoutes } from '../@core/const/app-routes.const';
import { HomeComponent } from './home.component';
import { AuthGuard } from '../@core/guards/auth.guard';
import { DistributorComponent } from './distributor.component';
import { DistributorDashboardComponent } from './dashboard/distributor-dashboard.component';
import { SxDistributorItemsComponent } from './items/distr-items.component';

const routes: Routes = [
  {
    path: AppRoutes.Root, component: DistributorComponent,
    children: [
      {
        path: AppRoutes.Root, component: HomeComponent,
        children: [
          { path: AppRoutes.Root, component: DistributorDashboardComponent,},
          { path: AppRoutes.itemas, component: SxDistributorItemsComponent },
            /*
          { path: AppRoutes.Dashboard, component: CompanyDashboardComponent },
          { path: 'type', component: CompanyTypeComponent },
          { path: 'create', component: CompanyCreateComponent },
          // { path: AppRoutes.Dashboard, component: DashboardComponent },
          */
        ]
      },
      // { path: 'copper-pricing', component: CopperPricingComponent }
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DistributorRoutingModule { }

export const routedComponents = [
  DistributorComponent,
  HomeComponent,
  DistributorDashboardComponent,
  SxDistributorItemsComponent,
];

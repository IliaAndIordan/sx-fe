import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// --- Modules
import { ShareModule } from '../@share/share.module';
import { SxMaterialModule } from '../@share/material.module';
// ---Services
import { CompanyService } from '../company/company.service';
import { DistributorService } from './distributor.service';
// ---Components
import { DistributorRoutingModule, routedComponents } from './distributor-routing.module';
import { DistributorUtilityBarComponent } from './utility-bar/utility-bar.component';
import { SxDistributorItemsService } from './items/dist-items.service';



@NgModule({

    imports: [
        CommonModule,
        ShareModule,
        SxMaterialModule,
        DistributorRoutingModule,
    ],
    declarations: [
        DistributorUtilityBarComponent,
        routedComponents,
        // --- Dialogs
        
    ],
    entryComponents: [
    ],
    providers:[
        CompanyService,
        DistributorService,
        SxDistributorItemsService,
    ]
})

export class DistributorModule { }

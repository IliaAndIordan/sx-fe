import { Injectable } from '@angular/core';
import { GravatarService } from '@infinitycube/gravatar';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
// -Services
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
import { SxApiCompanyClient } from 'src/app/@core/api/company/api-client';
import { DistributorService } from '../distributor.service';
// -Models
import { registerLocaleData } from '@angular/common';



export const SR_LEFT_PANEL_IN_KEY = 'sx_distributor_items_tree_panel_in';
export const SR_SEL_TABIDX_KEY = 'sx_distributor_items_tab_idx';

@Injectable({
    providedIn: 'root',
})
export class SxDistributorItemsService {

    /**
     *  Fields
     */

    constructor(
        private cus: CurrentUserService,
        private gravatarService: GravatarService,
        private client: SxApiCompanyClient,
        private distService: DistributorService) {
    }

    /*
    get region(): AmsRegion {
        return this.wadService.region;
    }

    get subregion(): AmsSubregion {
        return this.wadService.subregion;
    }
    */

    //#region subregion tree panel

    panelInChanged = new BehaviorSubject<boolean>(true);

    get panelIn(): boolean {
        let rv = true;
        const valStr = localStorage.getItem(SR_LEFT_PANEL_IN_KEY);
        if (valStr) {
            rv = JSON.parse(valStr) as boolean;
        }
        return rv;
    }

    set panelIn(value: boolean) {
        localStorage.setItem(SR_LEFT_PANEL_IN_KEY, JSON.stringify(value));
        this.panelInChanged.next(value);
    }

    tabIdxChanged = new BehaviorSubject<number>(undefined);

    get tabIdx(): number {
        let rv = 0;
        const dataStr = localStorage.getItem(SR_SEL_TABIDX_KEY);
        //console.log('selFolderId-> dataStr', dataStr);
        if (dataStr) {
            try {
                rv = parseInt(dataStr, 10);
            }
            catch {
                localStorage.removeItem(SR_SEL_TABIDX_KEY);
                rv = 1;
            }

        }
        // console.log('selTabIdx-> rv', rv);
        return rv;
    }

    set tabIdx(value: number) {
        // console.log('selTabIdx->', value);
        const oldValue = this.tabIdx;

        localStorage.setItem(SR_SEL_TABIDX_KEY, JSON.stringify(value));
        if (oldValue !== value) {
            this.tabIdxChanged.next(value);
        }
    }

    //#endregion

    //#region Data
    /*
    public loadAirport(apId: number): Observable<AmsAirport> {

        return new Observable<AmsAirport>(subscriber => {

            this.client.getAirport(apId)
                .subscribe((resp: AmsAirport) => {
                    console.log('loadAirport-> resp', resp);
                    subscriber.next(resp);
                },
                err => {

                    throw err;
                });
    });

    }

    */

    //#endregion


}

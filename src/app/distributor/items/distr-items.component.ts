

import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
// Services
import { SpinnerService } from '../../@core/services/spinner.service';
// -Models
import { environment } from 'src/environments/environment';
import { Animate } from 'src/app/@core/const/animation.const';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
import { SxDistributorItemsService } from './dist-items.service';
import { ExpandTab, PageTransition, ShowHideTriggerBlock, ShowHideTriggerFlex, 
  SpinExpandIconTrigger, TogleBtnTopRev } from 'src/app/@core/const/animations-triggers';



@Component({
  templateUrl: './distr-items.component.html',
  animations: [PageTransition,
    ShowHideTriggerBlock,
    ShowHideTriggerFlex,
    SpinExpandIconTrigger,
    ExpandTab, TogleBtnTopRev]
})
export class SxDistributorItemsComponent implements OnInit, OnDestroy {


  /**
   * Fields
   */
  panelInVar = Animate.in;
  panelInChanged: Subscription;

  //logo = COMMON_IMG_LOGO_RED;
  //amsImgUrl = URL_COMMON_IMAGE_AMS;
  env: string;

  //sxLogo = COMMON_IMG_LOGO_RED;
  sidebarClass = 'sidebar-closed';

  /*
  airport: AmsAirport
  airportChanged: Subscription;
*/
  get panelIn(): boolean {
    let rv = this.itemsService.panelIn;
    this.panelInVar = rv ? Animate.out : Animate.in;
    // console.log('panelIn-> hps:', this.hps);
    return this.itemsService.panelIn;
  }

  set panelIn(value: boolean) {
    this.itemsService.panelIn = value;
    this.panelInVar = this.panelIn ? Animate.out : Animate.in;
  }

  constructor(private router: Router,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private spinerService: SpinnerService,
    private cus: CurrentUserService,
    private itemsService: SxDistributorItemsService) { }


  ngOnInit(): void {
    //this.env = environment.a;
    const tmp = this.panelIn;
    this.panelInChanged = this.itemsService.panelInChanged.subscribe((panelIn: boolean) => {
      const tmp = this.panelIn;
    });

/*
    this.airportChanged = this.wadService.airportChanged.subscribe((airport: AmsAirport) => {
      this.initFields();
    });
*/
    
    this.initFields();
    

  }



  ngOnDestroy(): void {
    // console.log('HomeComponent:ngOnDestroy()->');
   // if (this.airportChanged) { this.airportChanged.unsubscribe(); }
    if (this.panelInChanged) { this.panelInChanged.unsubscribe(); }
  }

  initFields() {
    //this.region = this.wadService.region;
  
  }

  //#region  data

  public loadAirport(): void {
    /*
    this.spinerService.show();
    this.aService.loadAirport(this.airport.apId)
      .subscribe((resp: AmsAirport) => {
        //console.log('gatGvdtTsoSearchHits-> loadLoginsCount', list);
        this.wadService.airport = resp;
        this.spinerService.hide();
        this.initFields();
      },
        err => {
          this.spinerService.hide();
          console.log('loadRegions-> err: ', err);
        });
        */
  }
  //#endregion

  //#region  Actions

  // sidebar-closed
  sidebarToggle() {
    this.sidebarClass = this.sidebarClass === 'sidebar-closed' ? 'sidebar-open' : 'sidebar-closed';
  }

/*
  gotoSubregion(value: AmsSubregion) {
    console.log('gotoSubregion -> value:', value);
    if (value) {
      this.wadService.subregion = value;
      this.router.navigate([AppRoutes.Root, AppRoutes.wad, AppRoutes.subregion]);
    }

  }
*/
  //#endregion

  //#region Tree Panel

  public closePanelLeftClick(): void {
    this.panelIn = false;
  }

  public expandPanelLeftClick(): void {
    // this.treeComponent.openPanelClick();
    this.panelIn = true;
  }

  //#endregion

  //#region SPM Region

  spmRegionClose(): void {
    console.log('spmRegionClose ->');
    //this.wadService.regionPanelOpen.next(false);
  }

  spmRegionOpen() {
    console.log('spmRegionOpen ->');
    /*
    this.region = this.wadService.region;
    if (this.region ) {
      this.wadService.regionPanelOpen.next(true);
    }
    */
  }

  spmSubregionClose(): void {
    //console.log('spmSubregionClose ->');
    //this.wadService.subregionPanelOpen.next(false);
  }

  spmSubregionOpen() {
    //console.log('spmSubregionOpen ->');
    /*
    this.subregion = this.wadService.subregion;
   
    if (this.subregion) {
      this.wadService.subregionPanelOpen.next(true);
    }
    */
  }

  spmAirportClose(): void {
    console.log('spmAirportClose ->');
    //this.wadService.airportPanelOpen.next(this.airport);
  }

  spmAirportOpen() {
    console.log('spmAirportOpen ->');
    /*
    this.airport = this.wadService.airport;
    if (this.airport ) {
      this.wadService.airportPanelOpen.next(this.airport);
    }*/

  }


  //#endregion
}
